
DEF TEMP-TABLE hm-hdr
    FIELD rec-type          AS CHAR FORMAT "x"
    FIELD supplier          AS CHAR FORMAT "x(6)"
    FIELD po-num            AS CHAR FORMAT "x(6)"
    FIELD company           AS CHAR FORMAT "x(3)"
    FIELD chg-type          AS CHAR FORMAT "x(3)"
    FIELD order-date        AS CHAR FORMAT "x(10)"
    FIELD order-type        AS CHAR FORMAT "x(30)"
    FIELD terms             AS CHAR FORMAT "x(30)"
    FIELD curr-code         AS CHAR FORMAT "xxx"
    FIELD whse              AS CHAR FORMAT "xxx"
    FIELD planner           AS CHAR FORMAT "x(30)"
    FIELD phone             AS CHAR FORMAT "x(15)"
    FIELD fax               AS CHAR FORMAT "x(15)"
    FIELD email             AS CHAR FORMAT "x(50)"
    FIELD ship-to-name1     AS CHAR FORMAT "x(35)"
    FIELD ship-to-name2     AS CHAR FORMAT "x(30)"
    FIELD ship-to-addr1     AS CHAR FORMAT "x(30)"
    FIELD ship-to-addr2     AS CHAR FORMAT "x(30)"
    /*     FIELD ship-to-addr3     AS CHAR FORMAT "x(30)"  */
    FIELD ship-to-city      AS CHAR FORMAT "x(30)"
    FIELD ship-to-state     AS CHAR FORMAT "xx"
    FIELD ship-to-zip       AS CHAR FORMAT "x(15)"
    FIELD ship-to-country   AS CHAR FORMAT "x(03)"
    FIELD ref-A             AS CHAR FORMAT "x(30)"
    FIELD ref-B             AS CHAR FORMAT "x(30)"
    field FO_order_type     as char format "x(2)"
    FIELD tp-code           AS CHAR FORMAT "x(09)"
    INDEX rec-type-supplier-po AS PRIMARY rec-type supplier po-num.


DEF TEMP-TABLE hm-line
    FIELD rec-type          AS CHAR FORMAT "x"
    FIELD supplier          AS CHAR FORMAT "x(6)"
    FIELD po-num            AS CHAR FORMAT "x(6)"
    FIELD company           AS CHAR FORMAT "x(3)"
    FIELD chg-type          AS CHAR FORMAT "x(3)"
    FIELD line-num          AS INTE FORMAT ">>9"
    FIELD ITEM              AS CHAR FORMAT "x(16)"
    FIELD price             AS CHAR FORMAT "x(14)"
    FIELD price-unit        AS CHAR FORMAT "xxx"
    FIELD due-date          AS CHAR FORMAT "x(10)"    
    FIELD qty-ordered       AS CHAR FORMAT "x(09)"
    FIELD uom               AS CHAR FORMAT "x(03)"
    FIELD amount            AS CHAR FORMAT "x(12)"
    FIELD COUNT             AS CHAR FORMAT "x(06)"
    FIELD whse              AS CHAR FORMAT "x(03)"
    FIELD revision          AS CHAR FORMAT "x(06)"
    FIELD drawing           AS CHAR FORMAT "x(16)"
    FIELD line-TEXT         AS CHAR FORMAT "x(240)"
    field prod-line         as char format "x(40)"
    field long-prod-num     as char format "x(25)"
    INDEX rec-type-supplier-po-line AS PRIMARY rec-type supplier po-num line-num.

DEF TEMP-TABLE tt-text
    FIELD   t-seq  AS INTE
    FIELD   t-text AS CHAR FORMAT "x(76)"
    INDEX t-seq-text AS PRIMARY t-seq t-text.

DEF VAR t-dir               AS CHAR FORMAT "x(90)"      NO-UNDO.
DEF VAR t-entry             AS INTE                     NO-UNDO.
DEF VAR t-text              AS CHAR FORMAT "x(20)"      NO-UNDO.
DEF VAR Y                   AS INTE                     NO-UNDO.
DEF VAR t-list-file-names   AS CHAR FORMAT "x(60)"      NO-UNDO.
DEF VAR vct                 AS INTE                     NO-UNDO.



t-dir = "M:\HMISiteIdTest\".
t-list-file-names = "M:\test\edi.txt".

OS-COMMAND SILENT DIR /b VALUE(t-dir) > value(t-list-file-names).

vct = 0.
INPUT FROM value(t-list-file-names) NO-ECHO.
REPEAT:
    vct = vct + 1.
    CREATE tt-text.
    IMPORT UNFORMATTED tt-text.t-text.
    tt-text.t-text = TRIM(tt-text.t-text).
    tt-text.t-seq = vct.
END.
INPUT CLOSE. PAUSE(0).


t-dir = "".
FOR EACH tt-text 
    WHERE tt-text.t-text <> "":

    IF SUBSTRING(tt-text.t-text,1,1) = "" THEN NEXT.

    IF LENGTH(t-dir) > 3 THEN
        t-dir = t-dir + "," + tt-text.t-text .
    ELSE
        t-dir = tt-text.t-text.
    
END.

/* DISPLAY                       */
/*     t-dir   SKIP              */
/*     t-entry SKIP (2)          */
/*         WITH 2 COL WIDTH 300. */

/* DISPLAY                                */
/*     ENTRY(1,t-dir) FORMAT "x(12)" SKIP */
/*     ENTRY(2,t-dir) FORMAT "x(12)" SKIP */
/*     ENTRY(3,t-dir) FORMAT "x(12)" SKIP */
/*         WITH 2 COL.                    */

t-entry = NUM-ENTRIES(trim(t-dir)).
ENTRIES-LOOP:
DO Y = 1 TO NUM-ENTRIES(t-dir).
t-text = "M:\HMISiteIdTest\" + ENTRY(Y,t-DIR).


IF t-text BEGINS "M:\HMISiteIdTest\92517discpo" THEN
DO:

  INPUT FROM value(t-text) NO-ECHO.

  REPEAT:

      CREATE hm-hdr.
      IMPORT DELIMITER "|" hm-hdr NO-ERROR.

      IF hm-hdr.rec-type = "L" THEN
      DO:
          DELETE hm-hdr.
      END.
  END.
  INPUT CLOSE. PAUSE(0).

  INPUT FROM VALUE(t-text) NO-ECHO.

      REPEAT:

          CREATE hm-line.
          IMPORT DELIMITER "|" hm-line NO-ERROR.
          IF hm-line.rec-type = "H" THEN
          DO:
              DELETE hm-line.
              NEXT.
          END.
      END.
      INPUT CLOSE. PAUSE(0).

  END.
END.


FOR EACH hm-hdr WHERE 
        hm-hdr.rec-type <> "",
    EACH symix.co NO-LOCK WHERE
        co.cust-po = hm-hdr.po-num,
    FIRST symix.coitem NO-LOCK OF symix.co WHERE
        coitem.stat = "O",
    FIRST symix.ux-co EXCLUSIVE-LOCK OF symix.co WHERE
        ux-co.uf-site-id = "":

        ASSIGN
            ux-co.uf-site-id = hm-hdr.company
            .

        DISPLAY hm-hdr.po-num co.cust-po hm-hdr.company ux-co.uf-site-id WITH 2 COL.
END.

