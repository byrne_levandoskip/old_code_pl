








DEFINE TEMP-TABLE found NO-UNDO
	FIELD cust-num  LIKE symix.customer.cust-num
	FIELD customer  AS CHAR
    FIELD city      AS CHAR
    FIELD state     AS CHAR
    FIELD slsman    LIKE symix.slsman.slsman
    .

RUN import-stuff.
RUN check-stuff.
RUN output-stuff.

PROCEDURE import-stuff:

    INPUT FROM M:\slsman_assignment.csv.  /* ADD NAME */
    REPEAT:
        CREATE found.
        IMPORT DELIMITER "," found NO-ERROR.
        RUN lib/expnd-ky.p (INPUT 7, INPUT-OUTPUT found.cust-num).
    END.
    INPUT CLOSE. PAUSE(0).
END PROCEDURE.

PROCEDURE check-stuff:
    
    OUTPUT TO M:\slsman_update_errlog.csv.
    FOR EACH found EXCLUSIVE-LOCK:

        FIND FIRST symix.slsman NO-LOCK WHERE
            slsman.slsman = found.slsman NO-ERROR.
        IF NOT AVAIL slsman THEN DO:
            EXPORT DELIMITER ","
                found.cust-num
                found.slsman
                "No Slsman Found".
            DELETE found.
            NEXT.
        END.

        FIND FIRST symix.customer NO-LOCK WHERE
            customer.cust-num = found.cust-num NO-ERROR.
        IF NOT AVAIL customer THEN DO:
            EXPORT DELIMITER ","
                found.cust-num
                found.slsman
                "No Customer Found".
            DELETE found.
        END.
    END.
    OUTPUT CLOSE.
END PROCEDURE.

PROCEDURE output-stuff:

    OUTPUT TO M:\slsman_assign_output.csv.
    FOR EACH found NO-LOCK WHERE
        found.cust-num <> "",
        EACH symix.customer NO-LOCK WHERE
        customer.cust-num = found.cust-num:

        IF found.slsman <> customer.slsman THEN DO:
/*             ASSIGN                              */
/*                 customer.slsman = found.slsman. */
            EXPORT DELIMITER ","
                found.cust-num
                customer.cust-seq
                customer.slsman
                found.slsman
                "CHANGE".
        END.
        ELSE DO:
            EXPORT DELIMITER ","
                found.cust-num
                customer.cust-seq
                customer.slsman
                found.slsman
                "NO CHANGE".
        END.
    END.    
END.


/*                                                 */
/* OUTPUT TO M:\loadscan-pre-change.csv.           */
/* FOR EACH symix.be-loadscan NO-LOCK WHERE        */
/*     be-loadscan.extracted = NO AND              */
/*     be-loadscan.scanned = YES AND               */
/*     be-loadscan.trailer = "7051":               */
/*     EXPORT DELIMITER ","                        */
/*         be-loadscan.                            */
/* END.                                            */
/* OUTPUT CLOSE.                                   */
/*                                                 */
/*                                                 */
/*                                                 */
/* OUTPUT TO M:\loadscan-post-change.csv.          */
/* FOR EACH symix.be-loadscan EXCLUSIVE-LOCK WHERE */
/*     be-loadscan.extracted = NO AND              */
/*     be-loadscan.scanned = YES AND               */
/*     be-loadscan.trailer = "7051":               */
/*     ASSIGN                                      */
/*         be-loadscan.trailer = "LIVE".           */
/*     EXPORT DELIMITER ","                        */
/*         be-loadscan.                            */
/* END.                                            */
/* OUTPUT CLOSE.                                   */
/*                                                 */
/*                                                 */
