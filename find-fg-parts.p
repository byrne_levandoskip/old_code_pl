
DEF TEMP-TABLE found-subassm
    FIELD ITEM  LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE found-fg
    FIELD ITEM  LIKE symix.ITEM.ITEM.


FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM BEGINS "03378" OR
    ITEM.ITEM BEGINS "03557" OR
    ITEM.ITEM BEGINS "03558" OR
    ITEM.ITEM BEGINS "03559" OR
    ITEM.ITEM BEGINS "080157" OR
    ITEM.ITEM BEGINS "43560" OR
    ITEM.ITEM BEGINS "03373" OR
    ITEM.ITEM BEGINS "03561" OR
    ITEM.ITEM BEGINS "03562" OR
    ITEM.ITEM BEGINS "03563" OR
    ITEM.ITEM BEGINS "03564" OR
    ITEM.ITEM BEGINS "080158" OR
    ITEM.ITEM BEGINS "03385":

    CREATE found-subassm.
    ASSIGN found-subassm.ITEM = ITEM.ITEM.

END.

OUTPUT TO M:\found-subassm.csv.
FOR EACH found-subassm:
    EXPORT DELIMITER ","
        found-subassm.
END.
OUTPUT CLOSE.


FOR EACH found-subassm,
    EACH symix.jobmatl NO-LOCK WHERE
    jobmatl.ITEM = found-subassm.ITEM AND
    (jobmatl.effect-date  <= TODAY OR 
     jobmatl.effect-date  = ?) AND
    (jobmatl.obs-date     >  TODAY OR 
     jobmatl.obs-date     = ?),
    EACH symix.job NO-LOCK OF symix.jobmatl WHERE
    job.TYPE = "s",
    EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM = job.ITEM:

    CREATE found-fg.
    ASSIGN found-fg.ITEM = ITEM.ITEM.
END.


OUTPUT TO M:\found-fg.csv.
FOR EACH found-fg:
    EXPORT DELIMITER ","
        found-fg.
END.
OUTPUT CLOSE.


