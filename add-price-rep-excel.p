

DEF VAR hExcel         AS COM-HANDLE NO-UNDO.
DEF VAR hWorkBook      AS COM-HANDLE NO-UNDO.
DEF VAR hWorkSheet     AS COM-HANDLE NO-UNDO.
DEF VAR hWindow        AS COM-HANDLE NO-UNDO.
DEF VAR v-row          AS INT INIT 1.
DEF VAR v-lot          as char.
def var v-co-num       like symix.coitem.co-num.
def var v-co-line      like symix.coitem.co-line.
def var v-cr-rep       as char.


CREATE "Excel.Application" hExcel.
ASSIGN
   hWorkBook      = hExcel:WorkBooks:OPEN("M:\Orders on Hand.xlsx")
   hWorkSheet     = hexcel:Sheets:ITEM(1) 
   hWindow        = hExcel:ActiveWindow
   hexcel:VISIBLE = FALSE.  /* skip first row - headers */

/* DO v-row = 2 TO 5670: */
DO v-row = 2 TO 5:

    ASSIGN
        v-lot  = TRIM(STRING(hWorkSheet:Cells(v-row,1):VALUE))
        v-co-num = substring(v-lot, 1, index(v-lot, "-") - 1)
        v-co-line = int(substring(v-lot, index(v-lot, "-") + 1))
        .

    run lib/expnd-ky.p (input 7, input-output v-co-num).

    find first symix.coitem no-lock where
        coitem.co-num = v-co-num and
        coitem.co-line = v-co-line no-error.

    find first symix.co no-lock of symix.coitem no-error.

    find first symix.ux-customer no-lock where
        ux-customer.cust-num = co.cust-num no-error.

    if avail ux-customer then
        assign v-cr-rep = substring(ux-customer.uf-internal-email, 1, index(ux-customer.uf-internal-email, "@") - 1).

    message
        v-lot skip
        v-co-num skip
        v-co-line skip
        v-cr-rep skip
        view-as alert-box.

    if avail coitem then
        assign 
        hWorkSheet:Cells(v-row,25):VALUE = v-cr-rep
        hWorkSheet:Cells(v-row,26):VALUE = coitem.price-conv
        .

END.
OUTPUT CLOSE.

hWorkBook:CLOSE("M:\Orders on Hand.xlsx") NO-ERROR.
hExcel:QUIT() NO-ERROR.
RELEASE OBJECT hExcel NO-ERROR.
RELEASE OBJECT hWorkBook NO-ERROR.
RELEASE OBJECT hWorkSheet NO-ERROR.
RELEASE OBJECT hWindow NO-ERROR.



