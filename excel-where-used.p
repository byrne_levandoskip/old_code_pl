

/****

Qty per U/M needs some work
  Currently assumes all U/M as EA
  
  
*****/


DEFINE TEMP-TABLE search-list NO-UNDO
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD qty           AS INT.

DEFINE TEMP-TABLE indent-bom NO-UNDO
    FIELD parent-item   LIKE symix.ITEM.ITEM
    FIELD level         AS INT
    FIELD cur-item      LIKE symix.ITEM.ITEM
    FIELD DESCRIPTION   LIKE symix.ITEM.DESCRIPTION
    FIELD qty-used      AS INT
    FIELD extra-f1      AS CHAR FORMAT "x(150)"
    FIELD extra-f2      AS CHAR FORMAT "x(150)"
    FIELD extra-f3      AS CHAR FORMAT "x(150)"
    .


/***************/
/**   MAIN    **/
/***************/

RUN import-1.

FOR EACH search-list NO-LOCK WHERE
    search-list.ITEM <> "":

    RUN find-bom (
        INPUT 0,
        INPUT search-list.ITEM,
        INPUT search-list.ITEM,
        INPUT search-list.qty,
        INPUT TODAY,
        INPUT ?
        ).
END.

RUN output-bom.

/*****************/
/**  END MAIN   **/
/*****************/










/* use if search is based on critriea in item.item */
PROCEDURE import-1:
    FOR EACH symix.ITEM NO-LOCK WHERE
        ITEM.ITEM BEGINS "Data Option":
        CREATE search-list.
        ASSIGN
            search-list.ITEM = ITEM.ITEM
            search-list.qty  = 1.
    END.
END PROCEDURE.

/* use if reference is provided */
PROCEDURE import-2:
    INPUT FROM M:\import-me.csv.
    REPEAT:
        CREATE search-list.
        IMPORT DELIMITER "," search-list.
    END.
    INPUT CLOSE.
END PROCEDURE.

PROCEDURE output-bom:
    OUTPUT TO VALUE("M:\indent-bom_" + 
                    STRING(MONTH(TODAY)) + "-" +
                    STRING(DAY(TODAY)) + "-" +
                    STRING(YEAR(TODAY)) + "_" +
                    STRING(TIME) + ".csv").

    EXPORT DELIMITER ","
        "Parent Item"
        "Level"
        "Level Item"
        "Description"
        "Qty per"
        "Extra Field 1"
        "Extra Field 2"
        "Extra Field 3"
        .

    FOR EACH indent-bom NO-LOCK WHERE
        indent-bom.parent-item <> "":
        EXPORT DELIMITER ","
            indent-bom.
    END.
    OUTPUT CLOSE.
END PROCEDURE.



PROCEDURE find-bom:
    DEF INPUT PARAMETER t-level AS INT.
    DEF INPUT PARAMETER t-item  LIKE symix.ITEM.ITEM.
    DEF INPUT PARAMETER p-item  LIKE symix.ITEM.ITEM. /* parent item */
    DEF INPUT PARAMETER t-qty   LIKE symix.jobmatl.matl-qty.
    DEF INPUT PARAMETER t-eff-d LIKE symix.jobmatl.effect-date.
    DEF INPUT PARAMETER jobmatl-recid AS RECID NO-UNDO.
    
    FIND symix.ITEM NO-LOCK WHERE 
        ITEM.ITEM = t-item NO-ERROR.
    
    IF AVAILABLE(ITEM)
    THEN DO:

        FIND FIRST symix.ux-item NO-LOCK OF symix.ITEM NO-ERROR.

        CREATE indent-bom.
        ASSIGN
            indent-bom.parent-item     = p-item
            indent-bom.level           = t-level
            indent-bom.cur-item        = t-item
            indent-bom.DESCRIPTION     = ITEM.DESCRIPTION
            indent-bom.qty-used        = t-qty
            indent-bom.extra-f1        = ux-item.uf-kit-description
            .
    
        FOR EACH symix.jobmatl NO-LOCK WHERE
            jobmatl.job           =  ITEM.job AND
            jobmatl.suffix        =  ITEM.suffix AND
            (jobmatl.effect-date  <= t-eff-d OR jobmatl.effect-date  = ?) AND
            (jobmatl.obs-date     >  t-eff-d OR jobmatl.obs-date     = ?),
          EACH symix.jobroute OF jobmatl NO-LOCK WHERE
            (jobroute.effect-date <= t-eff-d OR jobroute.effect-date = ?) AND
            (jobroute.obs-date    >  t-eff-d OR jobroute.obs-date    = ?),
          EACH symix.job OF jobroute NO-LOCK WHERE job.type = "S"
          BY jobmatl.bom-seq BY jobmatl.oper-num BY jobmatl.sequence:

            RUN find-bom  (
                INPUT t-level + 1,
                INPUT jobmatl.item,
                INPUT p-item,
                INPUT t-qty * jobmatl.matl-qty-conv,
                INPUT t-eff-d,
                INPUT recid(jobmatl)
                ).
    
        END.
    END.
    ELSE IF NOT AVAIL ITEM AND t-level = 0 THEN DO:
        CREATE indent-bom.
        ASSIGN
            indent-bom.parent-item  = t-item
            indent-bom.level        = t-level
            indent-bom.DESCRIPTION  = "NO ITEM FOUND"
            .
    END.
    ELSE DO:
       IF jobmatl-recid <> ? then
          find symix.jobmatl where recid(jobmatl) = jobmatl-recid no-lock.
/*        ASSIGN                                                            */
/*           fnd-item  = no                                                 */
/*           t-desc    = if available jobmatl and jobmatl.description <> "" */
/*                                           then jobmatl.description       */
/*                                           else symtext.sys-text.txt[3]   */
/*           t-job     = ""                                                 */
/*           t-suff    = 0                                                  */
/*           t-display = FALSE                                              */
/*           .                                                              */
    END.
    
    RELEASE ITEM.
    
    DO ON ENDKEY UNDO, LEAVE:
        /* nothing */
    END.
END.




