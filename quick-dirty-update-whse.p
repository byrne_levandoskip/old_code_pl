
/* trig/jobroute_wc.p 
changes job whse AND coitem whse */
/* {lib/trig-def.i &object=job/v-jrtopr} */
/* {lib/std-def.i}                       */
    DEF VAR t-job                   LIKE symix.job.job.
    DEF VAR t-suffix                LIKE symix.job.suffix.
    DEF VAR t-oper-num              LIKE symix.jobmatl.oper-num.
    DEF VAR t-sequence              LIKE symix.jobmatl.sequence.
    DEF VAR t-whse                  LIKE symix.job.whse.
    DEF VAR t-backflush             LIKE symix.jobmatl.backflush.
    DEF VAR t-changed                 AS LOGICAL INIT FALSE.

DEF WORK-TABLE found 
    FIELD wc        LIKE symix.jobroute.wc
    FIELD lot       AS CHAR FORMAT "x(20)"
    FIELD full-job  AS CHAR FORMAT "x(20)"
    FIELD job       LIKE symix.job.job
    FIELD suffix    LIKE symix.job.suffix
    .


INPUT FROM M:\all-open-jobs-p.csv.
REPEAT:
    CREATE found.
    IMPORT DELIMITER ","
        found.
    ASSIGN
        found.job = SUBSTRING(found.full-job, 1, 7)
        found.suffix = INT(SUBSTRING(found.full-job, 9))
        .
END.
INPUT CLOSE.


/* IF SELF:MODIFIED THEN DO: */
/*  can't run setwhse because it uses the orig jobroute wc */
/*        RUN lib/setwhse.p             */
/*            (INPUT jobroute.job,      */
/*             INPUT jobroute.suffix).  */
FOR EACH found NO-LOCK WHERE
    found.job <> "":

    FIND FIRST symix.jobroute NO-LOCK WHERE
        jobroute.job = found.job AND
        jobroute.suffix = found.suffix NO-ERROR.

    FIND symix.job EXCLUSIVE-LOCK WHERE job.job = jobroute.job
        AND job.suffix = jobroute.suffix NO-ERROR.
/*     IF error-status:error THEN DO:                   */
/*       {lib/msg-app.i std-msg std-msg E=ERROR-STATUS} */
/*       {&RETURN-ERROR}                                */
/*     end.                                             */

    IF CAN-FIND(FIRST symix.wc 
                  WHERE wc.wc = jobroute.wc
                  AND wc.charfld1 NE job.whse
                  AND wc.charfld1 NE '')
                  THEN DO:
          FIND FIRST symix.wc
              WHERE wc.wc = jobroute.wc
              NO-LOCK NO-ERROR.
          IF AVAILABLE wc THEN DO:
              IF job.ord-type = "O" THEN DO:
                  FIND FIRST symix.coitem
                      WHERE coitem.co-num  = job.ord-num
                        AND coitem.co-line = job.ord-line           
                      EXCLUSIVE-LOCK NO-ERROR.
                  IF AVAILABLE coitem THEN DO:
                      ASSIGN
                          coitem.whse = wc.charfld1.
                      RELEASE coitem.
                  END. /* IF AVAILABLE coitem THEN DO: */
              END. /* IF job.ord-type = "O" THEN DO: */
/*               MESSAGE jobroute.job jobroute.suffix VIEW-AS ALERT-BOX. */
/*               FIND symix.job WHERE job.job = jobroute.job             */
/*               AND job.suffix = jobroute.suffix                        */
/*                 EXCLUSIVE-LOCK .                                      */
/*                                                                       */
/*               IF error-status:error THEN DO:                          */
/*                 {lib/msg-app.i std-msg std-msg E=ERROR-STATUS}        */
/*                 {&RETURN-ERROR}                                       */
/*               end.                                                    */
            ASSIGN
                job.whse = wc.charfld1
                t-whse = job.whse
                t-changed = TRUE
                t-job = job.job
                t-suffix = job.suffix.
            RELEASE job.
        END. /* IF AVAILABLE wc */
    END. /* IF AVAILABLE job THEN DO: */

    /* BEG INSERTION TO RESET BACKFLUSH FLAG 
    WILL ONLY BE PERFORMED IF whse changed */
    IF t-changed THEN DO:
            FIND FIRST symix.whse
                WHERE whse.whse = t-whse
                NO-LOCK NO-ERROR.
            ASSIGN
                t-backflush = whse.logifld.
/*                 t-job       = job.job  PL - Moved this up */
/*                 t-suffix    = job.suffix. */
            RUN UPDATE_JOBMATL.
    END. /* IF t-changed THEN */
    /* END INSERTION TO RESET BACKFLUSH FLAG */

END.


PROCEDURE UPDATE_JOBMATL.
    FOR EACH symix.jobmatl
        WHERE jobmatl.job       = t-job
          AND jobmatl.suffix    = t-suffix
          AND jobmatl.backflush NE t-backflush  /* only locking if we need to change */
        EXCLUSIVE-LOCK.
        ASSIGN
            jobmatl.backflush = t-backflush.
        /* FOLLOWING ONLY DONE IF backflush FALSE SO THAT
           CERTAIN product-codes CAN STILL BE BACKFLUSHED */
        IF jobmatl.backflush = FALSE THEN
            DO:
            FIND FIRST symix.ITEM
                WHERE ITEM.ITEM = jobmatl.ITEM
                NO-LOCK NO-ERROR.
            IF AVAILABLE ITEM and
                can-find(FIRST symix.prodcode
                    WHERE prodcode.product-code = ITEM.product-code
                    AND SUBSTR(prodcode.DESCRIPTION,30,1) = "*" )
                THEN
                    ASSIGN
                        jobmatl.backflush = TRUE.
        END. /* IF jobmatl.backflush = FALSE THEN */
    END. /* FOR EACH symix.jobmatl */
END PROCEDURE. /* UPDATE_JOBMATL */

