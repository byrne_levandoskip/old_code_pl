/* Finds and reports blank customer items on open Herman orders */

DEF VAR t-found   AS LOG INITIAL FALSE.

DEF VAR mailserv  AS CHAR.
DEF VAR mailto    AS CHAR.
DEF VAR mailfrom  AS CHAR.
DEF VAR mailloc   AS CHAR.
DEF VAR mailsubj  AS CHAR.
DEF VAR mailbody  AS CHAR.
DEF VAR mailmime  AS CHAR.
DEF VAR mailatt   AS CHAR.
DEF VAR mailtype  AS CHAR.
DEF VAR mailcc    AS CHAR.
DEF VAR xstatus   AS LOG.
DEF VAR xmessage  AS CHAR.

OUTPUT TO VALUE("c:\temp\hmi-blank-ci.csv").
EXPORT DELIMITER ","
    "Order"
    "Line"
    "Byrne Item"
    .
    
FOR EACH byrne.coitem NO-LOCK WHERE
    coitem.stat = "o" AND 
    (coitem.co-cust-num = "HERMAN"  OR
    coitem.co-cust-num = "GEIGER"),
    FIRST byrne.co NO-LOCK OF byrne.coitem:

    IF co.taken-by <> "EDI-POC" THEN NEXT.

    IF coitem.cust-item = "" THEN DO:
        t-found = TRUE.
        EXPORT DELIMITER ","
            coitem.co-num
            coitem.co-line
            coitem.ITEM
            .
    END.
END.
OUTPUT CLOSE.


IF t-found = TRUE THEN DO:

    /*         ASSIGN mailto = "custserv@byrne-electrical.com". */
    ASSIGN mailto = "levandoskip@byrne-electrical.com".

    ASSIGN
        mailserv        = "smtp_server.byrne-electrical.com"
        mailfrom        = "supportcenter@byrne-electrical.com"
        mailcc          = ""
        mailatt         = "hmi-blank-ci.csv:type=text/plain"
        mailloc         = "c:\temp\hmi-blank-ci.csv"
        mailsubj        = "Blank Herman Cust Item Found"
        mailbody        = ""
        mailmime        = ""
        mailtype        = "file".

    RUN utilities/smtpmailpub.p (mailserv, mailto, mailfrom, mailcc, mailatt,
        mailloc, mailsubj, mailbody, mailmime,mailtype, 0,NO,"","","",
        OUTPUT xstatus,
        OUTPUT xmessage
        ) NO-ERROR.
END.

OS-COMMAND SILENT DEL VALUE("c:\temp\hmi-blank-ci.csv").

