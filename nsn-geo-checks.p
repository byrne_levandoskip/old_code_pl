


/* DEF BUFFER b-coitem FOR symix.coitem.              */
/*                                                    */
/* DEF VAR count-item AS INT.                         */
/*                                                    */
/* OUTPUT TO M:\per-geo.csv.                          */
/* EXPORT DELIMITER ","                               */
/*     "Order Num"                                    */
/*     "Order Line"                                   */
/*     "Item"                                         */
/*     "NSN ?"                                        */
/*     "NSN Name"                                     */
/*     "Prior Orders Count"                           */
/*     .                                              */
/* FOR EACH symix.coitem NO-LOCK WHERE                */
/*     coitem.co-num = " 851157",                     */
/*     FIRST symix.ux-coitem NO-LOCK OF symix.coitem: */
/*                                                    */
/*     ASSIGN                                         */
/*         count-item = 0.                            */
/*                                                    */
/*     FOR EACH b-coitem NO-LOCK WHERE                */
/*         b-coitem.co-num LT coitem.co-num AND       */
/*         b-coitem.ITEM = coitem.ITEM:               */
/*         ASSIGN                                     */
/*             count-item = count-item + 1.           */
/*     END.                                           */
/*                                                    */
/*     EXPORT DELIMITER ","                           */
/*         coitem.co-num                              */
/*         coitem.co-line                             */
/*         coitem.ITEM                                */
/*         ux-coitem.uf-nsn                           */
/*         ux-coitem.uf-nsn-names                     */
/*         count-item                                 */
/*         .                                          */
/*                                                    */
/* END.                                               */


OUTPUT TO M:\nsn-product-code-check.csv.
EXPORT DELIMITER ","
    "Order"
    "Line"
    "Item"
    "NSN ?"
    "NSN Name"
    "Descrption"
    "Product Code"
    .
FOR EACH symix.co NO-LOCK WHERE
    co.order-date GE 01/01/19,
    EACH symix.coitem NO-LOCK OF symix.co,
    EACH symix.ux-coitem NO-LOCK OF symix.coitem WHERE
    ux-coitem.uf-nsn = YES,
    FIRST symix.ITEM NO-LOCK WHERE
    ITEM.ITEM = coitem.ITEM:

    EXPORT DELIMITER ","
        coitem.co-num
        coitem.co-line
        coitem.ITEM
        ux-coitem.uf-nsn
        ux-coitem.uf-nsn-names
        ITEM.DESCRIPTION
        ITEM.product-code
        .
END.
OUTPUT CLOSE.