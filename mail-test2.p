




DEF VAR v-email-body    AS CHAR FORMAT "x(100)" EXTENT 5           NO-UNDO.

DEF VAR xstatus  AS LOG.
DEF VAR xmessage AS CHAR.

DEF VAR mailserv AS CHAR.
DEF VAR mailfrom AS CHAR.
DEF VAR mailto   AS CHAR.
DEF VAR mailloc  AS CHAR.
DEF VAR mailsubj AS CHAR. 
DEF VAR mailbody AS CHAR.
DEF VAR mailatt  AS CHAR.
DEF VAR mailtype AS CHAR.
DEF VAR mailcc   AS CHAR.
DEF VAR mailmime AS CHAR.

ASSIGN
    mailserv = "smtp_server.byrne-electrical.com"
    mailfrom = "levandoskip@byrne-electrical.com"
    mailto = "levandoskip@byrne-electrical.com"
    mailloc = "c:\temp\test1.txt,c:\temp\test2.txt"
    mailsubj = "This is only a test"
    mailbody = "c:\temp\email-body.txt"
    mailatt = "test1.txt:type=test/plain,test2.txt:type=test/plain"
    mailtype = "file"
    mailcc = "levandoskip@byrne-electrical.com"
    mailmime = ""
    v-email-body[1] = "This is only a test"
    v-email-body[2] = "To see if this text"
    v-email-body[3] = "Shows up in the body"
    v-email-body[4] = "Of this email that"
    v-email-body[5] = "I am sending to myself"
    .

OUTPUT TO c:\temp\email-body.txt.
PUT 
    v-email-body[01]        AT 1
    v-email-body[02]        AT 1
    v-email-body[03]        AT 1
    v-email-body[04]        AT 1  
    v-email-body[05]        AT 1
    .
OUTPUT CLOSE.


RUN utilities/smtpmailpub.p 
    (mailserv, 
     mailto, 
     mailfrom, 
     mailcc, 
     mailatt,
     mailloc, 
     mailsubj,
     mailbody, 
     mailmime,
     mailtype, 
     0,NO,"","","",
     OUTPUT xstatus,
     OUTPUT xmessage) NO-ERROR.

MESSAGE
    "Status: " xstatus SKIP
    "Messaage: " xmessage SKIP
    VIEW-AS ALERT-BOX.

OS-COMMAND SILENT DEL value(mailsubj) /q.

