

/* only attach a file */	
assign 
	unix-cmd = "uuencode /FilePath/FileName.txt FileName.txt | \
	mailx -m -s 'Email Subject' EmailPerson1@byrne.com \ 
	EmailPerson2@byrne.com EmailPerson3@byrne.com EmailPersonX@byrne.com"
	.

/* only have text in the body */
assign 
	unix-cmd = "cat /FilePath/EmailBody.txt | \
	mailx -m -s 'Email Subject' EmailPerson1@byrne.com \ 
	EmailPerson2@byrne.com EmailPerson3@byrne.com EmailPersonX@byrne.com"
	.

/* echo for simple text in email body */
assign 
	unix-cmd = "(echo Text in the body of the email. ; \ 
	uuencode /FilePath/FileName.txt FileName.txt) | \
	mailx -m -s 'Email Subject' EmailPerson1@byrne.com \ 
	EmailPerson2@byrne.com EmailPerson3@byrne.com EmailPersonX@byrne.com"
	.

/* cat for long text in email body */	
assign 
	unix-cmd = "(cat /FilePath/EmailBody.txt ; \ 
	uuencode /FilePath/FileName.txt FileName.txt) | \
	mailx -m -s 'Email Subject' EmailPerson1@byrne.com \ 
	EmailPerson2@byrne.com EmailPerson3@byrne.com EmailPersonX@byrne.com"
	.
	
