

OUTPUT TO M:\loadscan-upship-07-19-17.csv.

FOR EACH symix.be-loadscan EXCLUSIVE-LOCK WHERE
    be-loadscan.barcode = "1PQ3T133K" OR
    be-loadscan.barcode = "1PQ3T134L" OR
    be-loadscan.barcode = "1PQ3T135M" OR
    be-loadscan.barcode = "1PQ3T136N" OR
    be-loadscan.barcode = "1PQ3T137O" OR
    be-loadscan.barcode = "1PQ3T138P" OR
    be-loadscan.barcode = "1PQ3T139Q" OR
    be-loadscan.barcode = "1PQ3T140J" OR
    be-loadscan.barcode = "1PQ3T141K" OR
    be-loadscan.barcode = "1PQ3T142L" OR
    be-loadscan.barcode = "1PQ3T143M" OR
    be-loadscan.barcode = "1PQ3T144N" OR
    be-loadscan.barcode = "1PQ3T145O" OR
    be-loadscan.barcode = "1PQ3T146P" OR
    be-loadscan.barcode = "1PQ3T147Q" OR
    be-loadscan.barcode = "1PQ3T148R" OR
    be-loadscan.barcode = "1PQ3T149S" OR
    be-loadscan.barcode = "1PQ3T150L" OR
    be-loadscan.barcode = "1PQ3T151M" OR
    be-loadscan.barcode = "1PQ3T152N" OR
    be-loadscan.barcode = "1PQ3T153O" OR
    be-loadscan.barcode = "1PQ3T154P" OR
    be-loadscan.barcode = "1PQ3T155Q" OR
    be-loadscan.barcode = "1PQ3T156R" OR
    be-loadscan.barcode = "1PQ3T157S" OR
    be-loadscan.barcode = "1PQ3T158T" OR
    be-loadscan.barcode = "1PQ3T159U":

    EXPORT DELIMITER ","
        be-loadscan.

/*     ASSIGN                         */
/*         be-loadscan.scanned = NO   */
/*         be-loadscan.extracted = NO */
/*         be-loadscan.trailer = ""   */
/*         be-loadscan.trans-date = ? */
/*         be-loadscan.trans-time = 0 */
/*         be-loadscan.ship-type = "" */
/*         be-loadscan.skid = 0.      */

END.

OUTPUT CLOSE.
