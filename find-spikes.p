
DEF VAR myDay           AS DATE INITIAL TODAY.
DEF VAR curWk           AS INT.
DEF VAR ct              AS INT INITIAL 0.


/* table to hold 3 month average of Steelcase
   Parts by item by week. Only populates RW parts */
DEFINE TEMP-TABLE three-month-ave
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD avrg          AS INT.

/* Weekly table to check against 3 month average */
DEFINE TEMP-TABLE cur-week-demand
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD dman          AS INT.

/* Spikes found */
DEFINE TEMP-TABLE found
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD byrne-item    AS CHAR FORMAT "x(120)"
    FIELD avrg          AS INT
    FIELD dman          AS INT EXTENT 7.

/* Populates all sub-assmblies */
DEF TEMP-TABLE comp-item
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD be-item   LIKE symix.ITEM.ITEM
    FIELD qty       AS INT.

RUN write-log("Start Program").
RUN populate-average.
RUN write-log("populate-average finish. Count: " + STRING(ct)).
RUN find-spikes.
RUN write-log("find-spikes finish").
RUN generate-byrne-item.
RUN write-log("generate-byrne-item finish").
RUN out-results.

PROCEDURE populate-average:
    FOR EACH symix.co NO-LOCK WHERE
        co.cust-num = "Steel",
        EACH symix.coitem NO-LOCK OF symix.co WHERE
        coitem.due-date GE myDay - 91 AND
        coitem.due-date LT myDay AND
        coitem.qty-shipped GT 0,
        FIRST symix.ITEM NO-LOCK OF symix.coitem WHERE
        ITEM.product-code = "fg-kt-rw" OR
        ITEM.product-code = "fg-rw":
        
        FIND FIRST three-month-ave EXCLUSIVE-LOCK WHERE
            three-month-ave.ITEM = coitem.ITEM NO-ERROR.

        IF AVAIL three-month-ave THEN DO:
            ASSIGN
                three-month-ave.avrg = three-month-ave.avrg + coitem.qty-shipped.

        END.
        ELSE DO:
            CREATE three-month-ave.
            ASSIGN
                three-month-ave.ITEM = coitem.ITEM
                three-month-ave.avrg = coitem.qty-shipped.
            ct = ct + 1.
        END.

    END.

    OUTPUT TO M:\three-month-ave.csv.
    FOR EACH three-month-ave EXCLUSIVE-LOCK:
        ASSIGN
            three-month-ave.avrg = three-month-ave.avrg / 13.
        EXPORT DELIMITER ","
            three-month-ave.
    END.
    OUTPUT CLOSE.
END. /* procedure populate-average */

PROCEDURE find-spikes:

    REPEAT curWk = 1 TO 7:

        /* build weekly demand */
        FOR EACH symix.co NO-LOCK WHERE
            co.cust-num = "Steel",
            EACH symix.coitem NO-LOCK OF symix.co WHERE
            coitem.due-date GE myDay AND
            coitem.due-date LT myDay + 7,
            FIRST symix.ITEM NO-LOCK OF symix.coitem WHERE
            ITEM.product-code = "fg-kt-rw" OR
            ITEM.product-code = "fg-rw":

            FIND FIRST cur-week-demand EXCLUSIVE-LOCK WHERE
                cur-week-demand.ITEM = coitem.ITEM NO-ERROR.
    
            IF AVAIL cur-week-demand THEN DO:
                ASSIGN
                    cur-week-demand.dman = cur-week-demand.dman + coitem.qty-ordered.
            END.
            ELSE DO:
                CREATE cur-week-demand.
                ASSIGN
                    cur-week-demand.ITEM = coitem.ITEM
                    cur-week-demand.dman = coitem.qty-ordered.
            END. 
        END.

        /* check for spikes */
        OUTPUT TO M:\cur-week-demand.csv APPEND.
        FOR EACH cur-week-demand NO-LOCK:

            EXPORT DELIMITER ","
                cur-week-demand.ITEM
                cur-week-demand.dman
                myDay.

            FIND FIRST three-month-ave NO-LOCK WHERE
                three-month-ave.ITEM = cur-week-demand.ITEM NO-ERROR.

            IF NOT AVAIL three-month-ave THEN DO:

                FIND FIRST found NO-LOCK WHERE
                    found.ITEM = cur-week-demand.ITEM NO-ERROR.

                IF AVAIL found THEN DO:
                    ASSIGN
                        found.dman[curWk]   = cur-week-demand.dman.
                END.
                ELSE DO:
                    CREATE found.
                    ASSIGN
                        found.ITEM          = cur-week-demand.ITEM
                        found.avrg          = 0
                        found.dman[curWk]   = cur-week-demand.dman.
                END.

            END.
            ELSE IF AVAIL three-month-ave AND 
                 cur-week-demand.dman GE (three-month-ave.avrg * 1.1) THEN DO:

                FIND FIRST found NO-LOCK WHERE
                    found.ITEM = cur-week-demand.ITEM NO-ERROR.

                IF AVAIL found THEN DO:
                    ASSIGN
                        found.dman[curWk]   = cur-week-demand.dman.
                END.
                ELSE DO:
                    CREATE found.
                    ASSIGN
                        found.ITEM          = cur-week-demand.ITEM
                        found.avrg          = three-month-ave.avrg
                        found.dman[curWk]   = cur-week-demand.dman.
                END.
            END.
        END.
        OUTPUT CLOSE.


        /* reset values */
        ASSIGN
            myDay = myDay + 7.
        EMPTY TEMP-TABLE cur-week-demand.
        RUN write-log("Loop " + STRING(curWk) + " done. New myDay: " + STRING(myDay)).
    END.

END. /* find-spikes */

PROCEDURE out-results:
    OUTPUT TO M:\Steelcase_Order_Spikes.csv.
    EXPORT DELIMITER ","
        "FG Item"
        "Byrne Item(s)"
        "3-Month Ave"
        "Week1"
        "Week2"
        "Week3"
        "Week4"
        "Week5"
        "Week6"
        "Week7"
        .
    FOR EACH found NO-LOCK WHERE
        found.ITEM <> "":
        EXPORT DELIMITER ","
            found.
    END.
    OUTPUT CLOSE.
END. /* procedure out-results */

/* BDO Style Output */
PROCEDURE generate-byrne-item:

    FOR EACH found EXCLUSIVE-LOCK WHERE
        found.ITEM <> "":
        
        FOR EACH symix.job NO-LOCK WHERE
            job.TYPE = "s" AND
            job.ITEM = found.ITEM,
            EACH symix.jobmatl NO-LOCK OF symix.job,
            FIRST symix.ITEM NO-LOCK WHERE
            jobmatl.ITEM = ITEM.ITEM AND
            (NOT ITEM.ITEM BEGINS "DI-" AND
             NOT ITEM.ITEM BEGINS "BULK" AND
             ITEM.product-code <> "RM-LB" AND
             ITEM.product-code <> "RM-IS" AND
             ITEM.product-code <> "RM-COM"):

            FIND FIRST comp-item NO-LOCK WHERE
                comp-item.ITEM = ITEM.ITEM NO-ERROR.

            IF AVAIL comp-item THEN DO:
                ASSIGN
                    comp-item.qty       = comp-item.qty + jobmatl.matl-qty
                    .
            END.
            ELSE DO:
                CREATE comp-item.
                ASSIGN
                    comp-item.ITEM      = ITEM.ITEM
                    comp-item.be-item   = TRIM("BE" + ITEM.ITEM)
                    comp-item.qty       = jobmatl.matl-qty
                    .
            END.
        END.

        FOR EACH comp-item NO-LOCK WHERE
            comp-item.ITEM <> ""
            BREAK BY comp-item.ITEM:
            ASSIGN
                found.byrne-item = found.byrne-item + "(" + STRING(comp-item.qty) + ")" + comp-item.be-item + ",".
        END.

        ASSIGN
            found.byrne-item = SUBSTRING(found.byrne-item, 1, LENGTH(found.byrne-item) - 1).

        EMPTY TEMP-TABLE comp-item.
    END.

END. /* generate-byrne-item */


PROCEDURE write-log:
    DEFINE INPUT PARAMETER c AS CHAR FORMAT "x(300)".
    OUTPUT TO M:\find-spikes.lg APPEND.
    PUT c.
    PUT " ".
    PUT STRING(TIME, "HH:MM:SS").
    PUT SKIP.
    OUTPUT CLOSE.
END. /* procedure write-log */

