

DEF TEMP-TABLE found
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD sub-item  LIKE symix.ITEM.ITEM
    .


FOR EACH symix.job NO-LOCK WHERE
    job.TYPE = "s",
    EACH symix.jobmatl NO-LOCK OF symix.job WHERE
    jobmatl.ITEM BEGINS "03385" OR
    jobmatl.ITEM BEGINS "03565" OR
    jobmatl.ITEM BEGINS "03566" OR
    jobmatl.ITEM BEGINS "03567":

    FIND FIRST found NO-LOCK WHERE
        found.ITEM = job.ITEM NO-ERROR.

    IF NOT AVAIL found THEN DO:
        CREATE found.
        ASSIGN
            found.ITEM = job.ITEM
            found.sub-item = jobmatl.ITEM.

    END.
END.

/* FOR EACH found:               */
/*     DISPLAY found WITH 2 COL. */
/* END.                          */


OUTPUT TO M:\per-jake.csv.
EXPORT DELIMITER ","
    "Customer"
    "PO"
    "Order"
    "Line"
    "Item"
    "Qty Ordered"
    "Qty Shipped"
    "Due Date"
    "Job"
    "Work Center"
    .


FOR EACH symix.coitem NO-LOCK WHERE
    coitem.stat = "o",
    FIRST found NO-LOCK WHERE
    found.ITEM = coitem.ITEM AND
    found.ITEM <> "",
    FIRST symix.co NO-LOCK OF symix.coitem,
    FIRST symix.jobroute NO-LOCK WHERE
    jobroute.job = coitem.ref-num:

    EXPORT DELIMITER ","
        co.cust-num
        co.cust-po
        coitem.co-num
        coitem.co-line
        coitem.ITEM
        coitem.qty-ordered
        coitem.qty-shipped
        coitem.due-date
        jobroute.job
        jobroute.wc
        .
END.

