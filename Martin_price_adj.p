
DEFINE VARIABLE has-record AS LOGICAL.
DEFINE TEMP-TABLE found
    FIELD ITEM       LIKE symix.ITEM.ITEM.

INPUT FROM m:\martin_request.csv NO-ECHO.

REPEAT:
    CREATE found.
    IMPORT DELIMITER "," found.
END.


OUTPUT TO M:\martin_request_output-2.csv.

FOR EACH found NO-LOCK WHERE
    found.ITEM <> "":

    has-record = FALSE.

/*     FOR EACH symix.itemprice NO-LOCK WHERE */
/*         itemprice.ITEM = found.ITEM:       */
/*                                            */
/*         has-record = TRUE.                 */
/*                                            */
/*         EXPORT DELIMITER ","               */
/*             itemprice.                     */
/*         DELETE itemprice. */
/*     END.                                   */

    FOR EACH symix.be-item-pricecode NO-LOCK WHERE
        be-item-pricecode.ITEM = found.ITEM:

        has-record = TRUE.

        EXPORT DELIMITER ","
            be-item-pricecode.
/*         DELETE be-item-pricecode. */
    END.

    IF NOT has-record THEN
        EXPORT DELIMITER ","
        found.ITEM
        "Had no pricing record".

END.

