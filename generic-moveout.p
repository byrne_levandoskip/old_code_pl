


def temp-table move-ords
/*     field co-num        like symix.co.co-num */
    field cust-po       like symix.co.cust-po
    .

def var v-split-count as int.
def var v-job as char.
def var v-start as date.
def var v-end as date.
def var v-move-to as date.
def var v-cust like symix.customer.cust-num.
def var v-file as char.
def var uom-conv-factor like symix.u-m-conv.conv-factor.
def var std-ch as char.

assign
/*     v-start = 03/23/20 */
/*     v-end = 04/13/20 */
    v-move-to = 04/07/20
    v-cust = "HAWORT"
    v-file = "M:\" + v-cust + "-moveout_" + replace(string(today), "/", "") + "_" + string(time) + ".csv"
    .


input from M:\change-haworth.txt.
repeat:
    create move-ords.
    import delimiter "|" move-ords.
/*     run lib/expnd-ky.p(input 7, input-out move-ords.co-num). */
end.
input close.

output to value(v-file).
export delimiter ","
    "Customer"
    "Cust PO"
    "Order Date"
    "Order"
    "Line"
    "Item"
    "Ord Status"
    "Old Due Date"
    "New Due Date"
    "Job"
    "Split #"
    "Update Status"
    .
for each move-ords no-lock where
    move-ords.cust-po <> "",
    first symix.co no-lock where
/*     move-ords.co-num = co.co-num and */
    move-ords.cust-po = co.cust-po and
    co.cust-num = v-cust,
    each symix.coitem no-lock of symix.co where
    coitem.stat = "O":

/* for each symix.co no-lock where                 */
/*     co.cust-num = v-cust,                       */
/*     each symix.coitem no-lock OF symix.co where */
/*     coitem.stat = "O" and                       */
/*     coitem.due-date lt v-end and                */
/*     coitem.due-date ge v-start:                 */

    run lib/getumcf.p 
        (input coitem.u-m,
         input coitem.item,
         input co.cust-num,
         "C",
         output uom-conv-factor,
         output std-ch).

/*     RUN co/itemlog.p ~              */
/*         (coitem.co-num, ~           */
/*          coitem.co-line, ~          */
/*          coitem.co-release, ~       */
/*          coitem.item, ~             */
/*          coitem.qty-ordered-conv, ~ */
/*          coitem.qty-ordered-conv, ~ */
/*          coitem.price-conv, ~       */
/*          coitem.price-conv, ~       */
/*          coitem.disc, ~             */
/*          coitem.disc, ~             */
/*          co.disc, ~                 */
/*          co.disc, ~                 */
/*          coitem.due-date, ~         */
/*          v-move-to, ~               */
/*          coitem.projected-date, ~   */
/*          coitem.projected-date, ~   */
/*          "U", ~                     */
/*          uom-conv-factor, ~         */
/*          coitem.u-m, ~              */
/*          coitem.u-m ~               */
/*          ) NO-ERROR.                */

    assign 
        v-split-count = 0
        v-job = "N/A"
/*         coitem.due-date = v-move-to */
        .

    if coitem.ref-type = "J" then do:
        assign v-job = coitem.ref-num. 
        for each symix.job-sch no-lock where
            job-sch.job = v-job,
            first symix.jrt-sch no-lock of symix.job-sch:
            assign
/*                 job-sch.start-date = v-move-to */
/*                 job-sch.end-date = v-move-to   */
/*                 jrt-sch.start-date = v-move-to   */
/*                 jrt-sch.end-date = v-move-to   */
                v-split-count = v-split-count + 1
                .
        end.
    end.

    export delimiter ","
        co.cust-num
        co.cust-po
        co.order-date
        coitem.co-num
        coitem.co-line
        coitem.item
        coitem.stat
        coitem.due-date
        v-move-to
        v-job
        v-split-count
        "No Update"
/*         "Date Changed" */
        .
end.
output close.

