
OUTPUT TO M:\covert-to-robot.csv.
EXPORT DELIMITER ","
    "PO"
    "Order"
    "Line"
    "Con Lot"
    "Due Date"
    "Job"
    "Suffix"
    "Qty Released"
    "New WC"
    .

FOR EACH symix.coitem NO-LOCK WHERE
    coitem.ITEM = "842400415+STEEL" AND
    coitem.due-date GE 01/09/18 AND
    coitem.stat = "O",
    FIRST symix.ux-coitem NO-LOCK OF symix.coitem,
    FIRST symix.co NO-LOCK OF symix.coitem,
    EACH symix.job NO-LOCK WHERE
    job.job = coitem.ref-num,
    FIRST symix.jobroute EXCLUSIVE-LOCK OF symix.job:

    ASSIGN
        jobroute.wc = "B_AT01".


    EXPORT DELIMITER ","
        co.cust-po
        coitem.co-num
        coitem.co-line
        ux-coitem.uf-con-lot
        coitem.due-date
        job.job
        job.suffix
        job.qty-released
        jobroute.wc
        .
END.
