

OUTPUT TO M:\effective-date-update-postchange.csv.


FOR EACH symix.be-item-pricecode EXCLUSIVE-LOCK WHERE
    be-item-pricecode.effect-date = 10/01/18 AND
    (be-item-pricecode.pricecode = "OEP" OR
     be-item-pricecode.pricecode = "OES" OR
     be-item-pricecode.pricecode = "DLP" OR
     be-item-pricecode.pricecode = "DLS" OR
     be-item-pricecode.pricecode = "O23" OR
     be-item-pricecode.pricecode = "O24" OR
     be-item-pricecode.pricecode = "O33" OR
     be-item-pricecode.pricecode = "O34" OR
     be-item-pricecode.pricecode = "BS2" OR
     be-item-pricecode.pricecode = "BS3" OR
     be-item-pricecode.pricecode = "BS5" OR
     be-item-pricecode.pricecode = "OA2" OR
     be-item-pricecode.pricecode = "OA3" OR
     be-item-pricecode.pricecode = "OA5" OR
     be-item-pricecode.pricecode = "OB2" OR
     be-item-pricecode.pricecode = "OB3" OR
     be-item-pricecode.pricecode = "OB5" OR
     be-item-pricecode.pricecode = "OC2" OR
     be-item-pricecode.pricecode = "OC3" OR
     be-item-pricecode.pricecode = "OC5" OR
     be-item-pricecode.pricecode = "OD2" OR
     be-item-pricecode.pricecode = "OD3" OR
     be-item-pricecode.pricecode = "OD5" OR
     be-item-pricecode.pricecode = "OE2" OR
     be-item-pricecode.pricecode = "OE3" OR
     be-item-pricecode.pricecode = "OE5" OR
     be-item-pricecode.pricecode = "OF2" OR
     be-item-pricecode.pricecode = "OF3" OR
     be-item-pricecode.pricecode = "OF5" OR
     be-item-pricecode.pricecode = "OG2" OR
     be-item-pricecode.pricecode = "OG3" OR
     be-item-pricecode.pricecode = "OG5" OR
     be-item-pricecode.pricecode = "OH2" OR
     be-item-pricecode.pricecode = "OH3" OR
     be-item-pricecode.pricecode = "OH5" OR
     be-item-pricecode.pricecode = "OI2" OR
     be-item-pricecode.pricecode = "OI3" OR
     be-item-pricecode.pricecode = "OI5"):
    
	ASSIGN be-item-pricecode.effect-date = 09/13/18.
	
    EXPORT DELIMITER ","
        be-item-pricecode.
    
END.

OUTPUT CLOSE.
