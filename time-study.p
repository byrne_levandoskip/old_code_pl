




DEF VAR c-1-start   AS INT.
DEF VAR c-1-records AS INT.
DEF VAR c-1-stop    AS INT.

DEF VAR c-2-start   AS INT.
DEF VAR c-2-records AS INT.
DEF VAR c-2-stop    AS INT.

DEF VAR c-3-start   AS INT.
DEF VAR c-3-records AS INT.
DEF VAR c-3-stop    AS INT.

DEF VAR c-4-start   AS INT.
DEF VAR c-4-records AS INT.
DEF VAR c-4-stop    AS INT.



c-1-start = TIME.
FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM MATCHES "*+ALLMUS*":
    c-1-records = c-1-records + 1.
END.
c-1-stop = TIME.


c-2-start = TIME.
FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM MATCHES "*+ALLMUS":
    c-2-records = c-2-records + 1.
END.
c-2-stop = TIME.


c-3-start = TIME.
FOR EACH symix.ITEM NO-LOCK WHERE
    INDEX(ITEM.ITEM, "+ALLMUS") GT 0:
    c-3-records = c-3-records + 1.
END.
c-3-stop = TIME.


c-4-start = TIME.
FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.product-code BEGINS "FG-" AND
    ITEM.ITEM MATCHES "*+ALLMUS":
    c-4-records = c-4-records + 1.
END.
c-4-stop = TIME.




MESSAGE
    "--- Time report ---" SKIP
    "Pass One Records: " c-1-records SKIP
    "Pass One Run Time: " STRING(c-1-stop - c-1-start, "HH:MM:SS") SKIP(2)
    "Pass Two Records: " c-2-records SKIP
    "Pass Two Run Time: " STRING(c-2-stop - c-2-start, "HH:MM:SS") SKIP(2)
    "Pass Three Records: " c-3-records SKIP
    "Pass Three Run Time: " STRING(c-3-stop - c-3-start, "HH:MM:SS") SKIP(2)
    "Pass Four Records: " c-4-records SKIP
    "Pass Four Run Time: " STRING(c-4-stop - c-4-start, "HH:MM:SS") SKIP(2)
    VIEW-AS ALERT-BOX.