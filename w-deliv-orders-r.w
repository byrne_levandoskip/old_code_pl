&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          symglbl          PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:             

  Description:     co/w-deliv-orders-r.w 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:       Carl Peterson -- Nexus Software, Inc.
  
                07/25/08

  Created:       -- 1-734-453-3101 -- cpeterson@nexussoftware.net
  
  cdp       byr-1001 03/26/09    switched the bill-to and ship-to in the program and the Excel template    
  cdp       byr-1002 09/02/09    added logic for ch robinson and celadon for bill to addresses
  cdp       byr-1003 09/02/09    extended length for po-numbers  
  cdp       byr-1004 09/02/09    added logic for showing pre-paid, collect, and third party freight on bill of lading
  cdp       byr-1005 09/03/09    added logic for collect and pre-paid - ship-to and bill-to name
  cdp       byr-1006 06/23/10    added print toggle var - per Tim Cronk  
  MRP       BYR-1007 11/22/10    added UNIS as a TP carrier for Riviera.  
  MRP       BYR-1008 12/21/10    Default number of labels to number of packages.
  KTC       BYR-1089 01/19/12    Add call to custom program to show Do Sequences
  mrp       BYR-1090 05/16/12    changed bartender to use path variable instead of hardcoded path.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

SESSION:DATA-ENTRY-RETURN = TRUE.
DEFINE STREAM t.
OUTPUT STREAM t TO TERMINAL.
DEFINE BUFFER do-hdr FOR symix.do-hdr.
DEFINE BUFFER bcustaddr         FOR symcust.custaddr.
DEFINE BUFFER bcustomer         FOR symix.customer.
DEFINE VAR    f-origin-copies   AS INTE                 NO-UNDO.
DEFINE VAR    l-copies          AS INTE                 NO-UNDO.

DEFINE VAR    t-copies          AS INTE                 NO-UNDO.
DEFINE VAR    i-pages           AS INTE                 NO-UNDO.
DEFINE VAR    ix                AS INTEGER              NO-UNDO.
DEFINE VAR    f-co-num          LIKE symix.co.co-num    NO-UNDO.
DEFINE VAR    i-weight          AS DECI                 NO-UNDO.
DEFINE VAR    l-clear-billto    AS LOG INITIAL FALSE    NO-UNDO.
DEFINE VAR    z                 AS INTE                 NO-UNDO.
DEFINE VAR    t-ship-type       AS CHAR FORMAT "XX"     NO-UNDO.    

DEFINE VAR    outfile           AS CHAR FORMAT "x(40)"  NO-UNDO.

DEFINE VAR    x-path            AS CHAR                 NO-UNDO.

DEFINE VARIABLE vlist                   AS CHAR         NO-UNDO.
DEFINE VARIABLE vtp-code                AS CHAR         NO-UNDO.
    
DEFINE VARIABLE vprinters       AS CHAR FORMAT "x(60)"  NO-UNDO.  
DEFINE VARIABLE vPrinterCount   AS INTE                 NO-UNDO.
DEFINE VARIABLE vZebraPrinters  AS CHAR FORMAT "x(60)"  NO-UNDO. 
DEFINE VARIABLE vDefaultPrinter AS CHAR FORMAT "x(60)"  NO-UNDO.
DEFINE VARIABLE xx              AS INTE                 NO-UNDO.  
DEFINE VARIABLE ctr             AS INTE                 NO-UNDO.  
DEFINE VARIABLE label-qty       AS INTE FORMAT ">>9"    NO-UNDO. 

DEF VAR t-stamp-bc              AS CHARACTER FORMAT "x(40)"     NO-UNDO.
DEF VAR t-stamp-char            AS CHARACTER FORMAT "x(40)"     NO-UNDO.
    

ON RETURN TAB.


DEF STREAM sDefPrinter.
DEFINE STREAM label-stream. /* byr-1009 */
    
{officevar.i}
DEF TEMP-TABLE tt-do-lines
    FIELD   do-num          LIKE symix.do-line.do-num
    FIELD   do-line         LIKE symix.do-line.do-line
    FIELD   KEY             LIKE symix.DO-line.KEY
    FIELD   nmfc            AS   CHAR FORMAT "x(10)"
    FIELD   package-type    LIKE symix.do-line.package-type
    FIELD   DESCRIPTION     LIKE symix.do-line.DESCRIPTION
    FIELD   qty-packages    LIKE symix.do-line.qty-packages
    FIELD   rate-code       LIKE symix.do-line.rate-code
    FIELD   hazard          LIKE symix.do-line.hazard
    FIELD   class-code      AS   CHAR
    FIELD   weight          LIKE symix.do-line.weight
    FIELD   special-inst    AS   CHAR FORMAT "x(20)"
    INDEX   do-num-line AS PRIMARY do-num do-line.


{lib/curr-def.i}
{lib/cur-def.i}
{lib/std-def.i}

DEFINE VAR c-do-num      LIKE symix.do-hdr.do-num        NO-UNDO.

    DEFINE BUFFER parms FOR symix.parms.

    FIND FIRST symix.parms NO-LOCK NO-ERROR.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-DO-Lines

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-do-lines

/* Definitions for BROWSE BROWSE-DO-Lines                               */
&Scoped-define FIELDS-IN-QUERY-BROWSE-DO-Lines tt-do-lines.do-line tt-do-lines.DESCRIPTION tt-do-lines.qty-packages tt-do-lines.package-type tt-do-lines.nmfc tt-do-lines.class-code tt-do-lines.weight   
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-DO-Lines   
&Scoped-define SELF-NAME BROWSE-DO-Lines
&Scoped-define QUERY-STRING-BROWSE-DO-Lines FOR EACH tt-do-lines NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-DO-Lines OPEN QUERY {&SELF-NAME} FOR EACH tt-do-lines NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-DO-Lines tt-do-lines
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-DO-Lines tt-do-lines


/* Definitions for FRAME FRAME-DO-Lines                                 */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-DO-Lines ~
    ~{&OPEN-QUERY-BROWSE-DO-Lines}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS CmbPrinter tglDefaultPrinter TxtNumLabels ~
t-print-label btn_clear_billto btn-clear cb-carrier f-copies ~
RADIO-SET-plant f-do-num cb-stat f-cust-num f-cust-seq f-do-value ~
f-do-hdr-date f-shipcode btn-do-num f-carrier-contact f-veh-num btn_print ~
f-pro-number f-packages f-pickup-date RADIO-SET-toolbar f-po-num ~
btn-customer btn_do-save btn-cust-seq btn_cancel f-cust-name f-curr-code ~
Btn_Done 
&Scoped-Define DISPLAYED-OBJECTS CmbPrinter tglDefaultPrinter TxtNumLabels ~
t-print-label cb-carrier f-copies RADIO-SET-plant f-do-num cb-stat ~
f-cust-num f-cust-seq f-do-value f-do-hdr-date f-shipcode f-Carrier-num ~
f-carrier-contact f-veh-num f-pro-number f-weight f-packages f-pickup-date ~
RADIO-SET-toolbar f-po-num f-lot-num f-cust-name f-curr-code 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn-clear 
     LABEL "&Clear" 
     SIZE 11 BY .88.

DEFINE BUTTON btn-cust-seq 
     IMAGE-UP FILE "images/lookup-u.bmp":U
     LABEL "" 
     SIZE 5.43 BY .88.

DEFINE BUTTON btn-customer 
     IMAGE-UP FILE "images/lookup-u.bmp":U
     LABEL "" 
     SIZE 5.43 BY .88.

DEFINE BUTTON btn-do-num 
     IMAGE-UP FILE "images/lookup-u.bmp":U
     LABEL "Button 1" 
     SIZE 5.43 BY .88.

DEFINE BUTTON btn_cancel 
     LABEL "&Cancel" 
     SIZE 11 BY .88.

DEFINE BUTTON btn_clear_billto 
     LABEL "Clear Bill-To" 
     SIZE 15 BY 1.12.

DEFINE BUTTON btn_do-save 
     LABEL "&Save" 
     SIZE 11 BY .88.

DEFINE BUTTON Btn_Done AUTO-END-KEY DEFAULT 
     LABEL "&Exit/Save" 
     SIZE 11 BY .88
     BGCOLOR 8 .

DEFINE BUTTON btn_print 
     LABEL "&Print" 
     SIZE 11 BY .88.

DEFINE VARIABLE cb-carrier AS CHARACTER FORMAT "X(256)":U 
     LABEL "Carrier #" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cb-stat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "In Process","Approved" 
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE CmbPrinter AS CHARACTER FORMAT "X(256)":U 
     LABEL "Printer" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 37.72 BY 1 NO-UNDO.

DEFINE VARIABLE f-carrier-contact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .88 NO-UNDO.

DEFINE VARIABLE f-Carrier-num AS CHARACTER FORMAT "X(256)":U 
     LABEL "Carrier #" 
     VIEW-AS FILL-IN 
     SIZE 13 BY .88 NO-UNDO.

DEFINE VARIABLE f-copies AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Copies" 
     VIEW-AS FILL-IN 
     SIZE 10.57 BY .88 NO-UNDO.

DEFINE VARIABLE f-curr-code AS CHARACTER FORMAT "X(4)":U INITIAL "USD" 
     LABEL "Currency" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .88 NO-UNDO.

DEFINE VARIABLE f-cust-name AS CHARACTER FORMAT "X(30)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 34 BY .88 NO-UNDO.

DEFINE VARIABLE f-cust-num AS CHARACTER FORMAT "X(7)":U 
     LABEL "Customer" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .88 NO-UNDO.

DEFINE VARIABLE f-cust-seq AS INTEGER FORMAT ">>>9":U INITIAL 0 
     LABEL "Ship To" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE f-do-hdr-date AS DATE FORMAT "99/99/99":U 
     LABEL "Date" 
     VIEW-AS FILL-IN 
     SIZE 11.29 BY .88 NO-UNDO.

DEFINE VARIABLE f-do-num AS CHARACTER FORMAT "x(30)":U 
     LABEL "DO/BOL" 
     VIEW-AS FILL-IN 
     SIZE 34 BY .88 NO-UNDO.

DEFINE VARIABLE f-do-value AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Value" 
     VIEW-AS FILL-IN 
     SIZE 15.72 BY .88 NO-UNDO.

DEFINE VARIABLE f-lot-num AS CHARACTER FORMAT "X(256)":U 
     LABEL "Lot Num" 
     VIEW-AS FILL-IN 
     SIZE 21.43 BY .88 NO-UNDO.

DEFINE VARIABLE f-packages AS INTEGER FORMAT "->,>>9":U INITIAL 1 
     LABEL "Pkg #" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .88 NO-UNDO.

DEFINE VARIABLE f-pickup-date AS DATE FORMAT "99/99/99":U 
     LABEL "Pickup" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE f-po-num AS CHARACTER FORMAT "X(256)":U 
     LABEL "PO #" 
     VIEW-AS FILL-IN 
     SIZE 82 BY .92 NO-UNDO.

DEFINE VARIABLE f-pro-number AS CHARACTER FORMAT "X(15)":U 
     LABEL "Pro #" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .88 NO-UNDO.

DEFINE VARIABLE f-shipcode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Ship Code" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .88 TOOLTIP "Carrier Number" NO-UNDO.

DEFINE VARIABLE f-veh-num AS CHARACTER FORMAT "X(15)":U 
     LABEL "Vehicle #" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .88 NO-UNDO.

DEFINE VARIABLE f-weight AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Wgt" 
     VIEW-AS FILL-IN 
     SIZE 12 BY .88 NO-UNDO.

DEFINE VARIABLE TxtNumLabels AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Labels" 
     VIEW-AS FILL-IN 
     SIZE 10.57 BY .88 NO-UNDO.

DEFINE VARIABLE TxtQty AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Qty" 
     VIEW-AS FILL-IN 
     SIZE 10.57 BY .88 NO-UNDO.

DEFINE VARIABLE RADIO-SET-plant AS CHARACTER INITIAL "2" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Scan Paperwork", "1",
"Manual", "2"
     SIZE 28.57 BY .81 NO-UNDO.

DEFINE VARIABLE RADIO-SET-toolbar AS CHARACTER INITIAL "T" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Ship To", "T",
"Ship From", "F",
"Bill To", "I",
"DO Lines", "L"
     SIZE 74 BY .81 NO-UNDO.

DEFINE VARIABLE t-print-label AS LOGICAL INITIAL yes 
     LABEL "Print Label?" 
     VIEW-AS TOGGLE-BOX
     SIZE 14.86 BY .77 NO-UNDO.

DEFINE VARIABLE tglDefaultPrinter AS LOGICAL INITIAL no 
     LABEL "Default Printer" 
     VIEW-AS TOGGLE-BOX
     SIZE 22.29 BY .77 NO-UNDO.

DEFINE VARIABLE f-consignee-addr-1 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-1" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-addr-2 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-2" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-addr-3 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-3" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-addr-4 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-4" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-city AS CHARACTER FORMAT "X(20)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-contact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE f-consignee-country AS CHARACTER FORMAT "X(20)":U INITIAL "USA" 
     LABEL "Country" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-county AS CHARACTER FORMAT "X(20)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-fax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE f-consignee-name AS CHARACTER FORMAT "X(25)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-phone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE f-consignee-state AS CHARACTER FORMAT "X(3)":U 
     LABEL "Prov/ST" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignee-zip AS CHARACTER FORMAT "X(20)":U 
     LABEL "Post/Zip" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 105.86 BY 10.5.

DEFINE BUTTON btn_whse 
     IMAGE-UP FILE "images/lookup-u.bmp":U
     LABEL "" 
     SIZE 5 BY .88.

DEFINE VARIABLE f-consignor-addr-1 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-1" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-addr-2 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-2" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-addr-3 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-3" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-addr-4 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-4" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-city AS CHARACTER FORMAT "X(20)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-contact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-contact-id AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact ID" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-country AS CHARACTER FORMAT "X(20)":U 
     LABEL "Country" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-county AS CHARACTER FORMAT "X(20)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-fax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-name AS CHARACTER FORMAT "X(25)":U INITIAL "Byrne Electrical" 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-phone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-state AS CHARACTER FORMAT "X(3)":U 
     LABEL "Prov/ST" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-whse AS CHARACTER FORMAT "X(6)":U 
     LABEL "Whse" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .88 NO-UNDO.

DEFINE VARIABLE f-consignor-zip AS CHARACTER FORMAT "X(20)":U 
     LABEL "Post/Zip" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-whse-name AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 17 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 105 BY 10.15.

DEFINE BUTTON Btn_DoSeq 
     LABEL "&Sequences" 
     SIZE 11 BY .88.

DEFINE BUTTON btn_line-done 
     LABEL "&Done" 
     SIZE 11 BY .88.

DEFINE VARIABLE cs-package-type AS CHARACTER FORMAT "X(256)":U INITIAL "Skids" 
     LABEL "Package Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Boxes","Bundles","Cartons","Skids" 
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE f-class-code AS CHARACTER FORMAT "X(4)":U INITIAL "77.5" 
     LABEL "Class Code" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE f-description AS CHARACTER FORMAT "X(20)":U INITIAL "WIRING COMPONENTS" 
     LABEL "Package Description" 
     VIEW-AS FILL-IN 
     SIZE 22 BY .88 NO-UNDO.

DEFINE VARIABLE f-do-line AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Line" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE f-nmfc AS CHARACTER FORMAT "X(10)":U INITIAL "061130-08" 
     LABEL "NMFC" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE f-qty-packages AS INTEGER FORMAT "->,>>9":U INITIAL 0 
     LABEL "Qty" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE CS-DO-Invoice AS CHARACTER FORMAT "X(256)":U 
     LABEL "DO Invoice" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Single by PO" 
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE cs-inv-freq AS CHARACTER FORMAT "X(256)":U INITIAL "Weekly" 
     LABEL "Invoice Freq" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Weekly" 
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE f-invoice-printed AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Invoice Printed" 
     VIEW-AS FILL-IN 
     SIZE 10.57 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-addr-1 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-1" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-addr-2 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-2" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-addr-3 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-3" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-addr-4 AS CHARACTER FORMAT "X(25)":U 
     LABEL "Address-4" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-city AS CHARACTER FORMAT "X(20)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-contact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-country AS CHARACTER FORMAT "X(20)":U 
     LABEL "Country" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-county AS CHARACTER FORMAT "X(20)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-fax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-name AS CHARACTER FORMAT "X(25)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 27 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-phone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-state AS CHARACTER FORMAT "X(3)":U 
     LABEL "Prov/ST" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE f-invoicee-zip AS CHARACTER FORMAT "X(20)":U 
     LABEL "Post/Zip" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 105.29 BY 9.92.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-DO-Lines FOR 
      tt-do-lines SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-DO-Lines
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-DO-Lines C-Win _FREEFORM
  QUERY BROWSE-DO-Lines NO-LOCK DISPLAY
      tt-do-lines.do-line FORMAT ">>9":U
      tt-do-lines.DESCRIPTION FORMAT "x(30)"
      tt-do-lines.qty-packages FORMAT "-z,zz9":U
      tt-do-lines.package-type FORMAT "x(10)":U
      tt-do-lines.nmfc         FORMAT "x(6)":U
      tt-do-lines.class-code FORMAT "x(8)":U COLUMN-LABEL "Class!Code"
      tt-do-lines.weight FORMAT "zzz,zz9.9999":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 68.86 BY 5.92
         FONT 6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     CmbPrinter AT ROW 24.96 COL 33.29 COLON-ALIGNED
     tglDefaultPrinter AT ROW 24.96 COL 75.29
     TxtNumLabels AT ROW 23.81 COL 33.57 COLON-ALIGNED
     TxtQty AT ROW 23.81 COL 51.29 COLON-ALIGNED
     t-print-label AT ROW 23.81 COL 5.29
     btn_clear_billto AT ROW 9.08 COL 103
     btn-clear AT ROW 1.58 COL 105 NO-TAB-STOP 
     cb-carrier AT ROW 6.85 COL 10.72 COLON-ALIGNED
     f-copies AT ROW 2.62 COL 103 COLON-ALIGNED NO-TAB-STOP 
     RADIO-SET-plant AT ROW 1.58 COL 13.29 NO-LABEL NO-TAB-STOP 
     f-do-num AT ROW 3.65 COL 11 COLON-ALIGNED
     cb-stat AT ROW 3.77 COL 77.57 COLON-ALIGNED NO-TAB-STOP 
     f-cust-num AT ROW 4.5 COL 11 COLON-ALIGNED
     f-cust-seq AT ROW 4.65 COL 31.86 COLON-ALIGNED
     f-do-value AT ROW 4.65 COL 53.86 COLON-ALIGNED
     f-do-hdr-date AT ROW 4.69 COL 77.43 COLON-ALIGNED
     f-shipcode AT ROW 5.69 COL 75.72 COLON-ALIGNED
     btn-do-num AT ROW 3.65 COL 47.57 NO-TAB-STOP 
     f-Carrier-num AT ROW 6.77 COL 50.14 COLON-ALIGNED NO-TAB-STOP 
     f-carrier-contact AT ROW 6.73 COL 76 COLON-ALIGNED
     f-veh-num AT ROW 7.85 COL 11 COLON-ALIGNED
     btn_print AT ROW 4.5 COL 105 NO-TAB-STOP 
     f-pro-number AT ROW 7.81 COL 34.86 COLON-ALIGNED
     f-weight AT ROW 7.85 COL 60.14 COLON-ALIGNED NO-TAB-STOP 
     f-packages AT ROW 7.85 COL 81 COLON-ALIGNED
     f-pickup-date AT ROW 7.85 COL 99 COLON-ALIGNED
     RADIO-SET-toolbar AT ROW 10 COL 20.14 NO-LABEL NO-TAB-STOP 
     f-po-num AT ROW 8.85 COL 11 COLON-ALIGNED
     f-lot-num AT ROW 2.62 COL 11 COLON-ALIGNED NO-TAB-STOP 
     btn-customer AT ROW 4.65 COL 21.86 NO-TAB-STOP 
     btn_do-save AT ROW 3.54 COL 105
     btn-cust-seq AT ROW 4.65 COL 40.43 NO-TAB-STOP 
     btn_cancel AT ROW 5.5 COL 105 NO-TAB-STOP 
     f-cust-name AT ROW 5.69 COL 11 COLON-ALIGNED NO-TAB-STOP 
     f-curr-code AT ROW 5.69 COL 56 COLON-ALIGNED NO-TAB-STOP 
     Btn_Done AT ROW 6.38 COL 105 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 123.72 BY 25.31
         FONT 6.

DEFINE FRAME FRAME-DO-Lines
     f-do-line AT ROW 2.08 COL 15.29 COLON-ALIGNED
     f-qty-packages AT ROW 2.08 COL 31.29 COLON-ALIGNED
     cs-package-type AT ROW 2.08 COL 62.29 COLON-ALIGNED
     f-weight AT ROW 2.08 COL 93 COLON-ALIGNED
          LABEL "Line Weight" FORMAT "->>,>>9.9999":U
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
     f-nmfc AT ROW 3.27 COL 15 COLON-ALIGNED
     f-class-code AT ROW 3.35 COL 40.86 COLON-ALIGNED
     f-description AT ROW 4.38 COL 29.57 COLON-ALIGNED
     Btn_DoSeq AT ROW 4.23 COL 66 NO-TAB-STOP 
     btn_line-done AT ROW 4.23 COL 86 NO-TAB-STOP 
     BROWSE-DO-Lines AT ROW 5.73 COL 18
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3.86 ROW 11.15
         SIZE 112 BY 12.35
         FONT 6
         TITLE "Delivery Order Lines".

DEFINE FRAME FRAME-Consignor
     btn_whse AT ROW 3.27 COL 24 NO-TAB-STOP 
     f-consignor-whse AT ROW 3.31 COL 13 COLON-ALIGNED
     f-whse-name AT ROW 3.27 COL 27.72 COLON-ALIGNED NO-LABEL NO-TAB-STOP 
     f-consignor-contact AT ROW 3.27 COL 59 COLON-ALIGNED
     f-consignor-phone AT ROW 4.23 COL 59 COLON-ALIGNED
     f-consignor-name AT ROW 2.23 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-addr-1 AT ROW 4.23 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-addr-2 AT ROW 5.12 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-addr-3 AT ROW 6.08 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-addr-4 AT ROW 7 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-city AT ROW 7.92 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-state AT ROW 8.04 COL 43.86 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-zip AT ROW 8.92 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-county AT ROW 9.08 COL 44 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-country AT ROW 9.96 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignor-fax AT ROW 5.23 COL 59 COLON-ALIGNED
     f-consignor-contact-id AT ROW 2.31 COL 59 COLON-ALIGNED
     RECT-2 AT ROW 1.69 COL 4
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 4.14 ROW 11.27
         SIZE 111.43 BY 11.88
         FONT 6
         TITLE "Ship From - Consignor".

DEFINE FRAME FRAME-Invoicee
     f-invoice-printed AT ROW 7.38 COL 81 COLON-ALIGNED NO-TAB-STOP 
     CS-DO-Invoice AT ROW 5.38 COL 80.72 COLON-ALIGNED
     cs-inv-freq AT ROW 6.42 COL 80.72 COLON-ALIGNED
     f-invoicee-contact AT ROW 2.27 COL 80.72 COLON-ALIGNED
     f-invoicee-phone AT ROW 3.23 COL 80.72 COLON-ALIGNED
     f-invoicee-name AT ROW 2.23 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-addr-1 AT ROW 3.19 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-addr-2 AT ROW 4.08 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-addr-3 AT ROW 5.04 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-addr-4 AT ROW 5.96 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-city AT ROW 6.88 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-state AT ROW 7 COL 43.86 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-zip AT ROW 7.88 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-county AT ROW 8 COL 43 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-country AT ROW 8.92 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-invoicee-fax AT ROW 4.23 COL 80.72 COLON-ALIGNED
     RECT-3 AT ROW 1.58 COL 4
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 4.14 ROW 11.15
         SIZE 111.43 BY 12.12
         FONT 6
         TITLE "Bill To".

DEFINE FRAME FRAME-consignee
     f-consignee-contact AT ROW 2.08 COL 59 COLON-ALIGNED
     f-consignee-phone AT ROW 3.15 COL 59 COLON-ALIGNED
     f-consignee-name AT ROW 2.23 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-addr-1 AT ROW 3.19 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-addr-2 AT ROW 4.08 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-addr-3 AT ROW 5.04 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-addr-4 AT ROW 5.96 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-city AT ROW 6.88 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-state AT ROW 7 COL 41.57 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-zip AT ROW 7.88 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-county AT ROW 8.04 COL 41.86 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-country AT ROW 8.92 COL 13 COLON-ALIGNED NO-TAB-STOP 
     f-consignee-fax AT ROW 4.23 COL 59 COLON-ALIGNED
     RECT-1 AT ROW 1.46 COL 4.72
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 4.43 ROW 11.15
         SIZE 111.43 BY 12.23
         FONT 6
         TITLE "Ship To - Consignee".


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "< Delivery Orders - NSI - co/w-deliv-orders-r.w >"
         HEIGHT             = 25.35
         WIDTH              = 124.57
         MAX-HEIGHT         = 32.5
         MAX-WIDTH          = 164.57
         VIRTUAL-HEIGHT     = 32.5
         VIRTUAL-WIDTH      = 164.57
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME FRAME-consignee:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME FRAME-Consignor:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME FRAME-DO-Lines:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME FRAME-Invoicee:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   Custom                                                               */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME FRAME-Invoicee:MOVE-AFTER-TAB-ITEM (t-print-label:HANDLE IN FRAME DEFAULT-FRAME)
       XXTABVALXX = FRAME FRAME-Consignor:MOVE-BEFORE-TAB-ITEM (btn_clear_billto:HANDLE IN FRAME DEFAULT-FRAME)
       XXTABVALXX = FRAME FRAME-consignee:MOVE-BEFORE-TAB-ITEM (FRAME FRAME-Consignor:HANDLE)
       XXTABVALXX = FRAME FRAME-DO-Lines:MOVE-BEFORE-TAB-ITEM (FRAME FRAME-consignee:HANDLE)
       XXTABVALXX = FRAME FRAME-Invoicee:MOVE-BEFORE-TAB-ITEM (FRAME FRAME-DO-Lines:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FILL-IN f-Carrier-num IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f-lot-num IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f-weight IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       f-weight:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN TxtQty IN FRAME DEFAULT-FRAME
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       TxtQty:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR FRAME FRAME-consignee
   NOT-VISIBLE Custom                                                   */
/* SETTINGS FOR FRAME FRAME-Consignor
   NOT-VISIBLE Custom                                                   */
ASSIGN 
       FRAME FRAME-Consignor:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME FRAME-DO-Lines
   Custom                                                               */
/* BROWSE-TAB BROWSE-DO-Lines btn_line-done FRAME-DO-Lines */
/* SETTINGS FOR FRAME FRAME-Invoicee
   NOT-VISIBLE Custom                                                   */
ASSIGN 
       FRAME FRAME-Invoicee:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN f-invoice-printed IN FRAME FRAME-Invoicee
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-DO-Lines
/* Query rebuild information for BROWSE BROWSE-DO-Lines
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tt-do-lines NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE BROWSE-DO-Lines */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME FRAME-consignee
/* Query rebuild information for FRAME FRAME-consignee
     _Query            is NOT OPENED
*/  /* FRAME FRAME-consignee */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME FRAME-Consignor
/* Query rebuild information for FRAME FRAME-Consignor
     _Query            is NOT OPENED
*/  /* FRAME FRAME-Consignor */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME FRAME-DO-Lines
/* Query rebuild information for FRAME FRAME-DO-Lines
     _Query            is NOT OPENED
*/  /* FRAME FRAME-DO-Lines */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME FRAME-Invoicee
/* Query rebuild information for FRAME FRAME-Invoicee
     _Query            is NOT OPENED
*/  /* FRAME FRAME-Invoicee */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* < Delivery Orders - NSI - co/w-deliv-orders-r.w > */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON ENTRY OF C-Win /* < Delivery Orders - NSI - co/w-deliv-orders-r.w > */
DO:
    vPrinters = SESSION:GET-PRINTERS( ).
    
      DO vPrinterCount = 1 TO NUM-ENTRIES(vPrinters, ","):
    
          IF ENTRY(vPrinterCount,vPrinters,",") MATCHES "*z140*" OR  
             ENTRY(vPrinterCount,vPrinters,",") MATCHES "*2844z*" THEN DO:
          
              vZebraPrinters = vZebraPrinters + ENTRY(vPrinterCount,vPrinters,",") + ",".
    
          END.
    
      
      END.
    
      cmbPrinter:LIST-ITEMS IN FRAME DEFAULT-frame = vZebraPrinters.

     
      
      IF SEARCH("c:\temp\defaultlabelprinter.txt") <> ? THEN do:
          
          INPUT FROM c:\temp\defaultLabelPrinter.txt.
          IMPORT vDefaultPrinter.
            
          cmbPrinter:SCREEN-VALUE = vDefaultPrinter.
            
/*           cmbPrinter:SCREEN-VALUE = vDefaultPrinter. */
      END.              
  
      
       
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* < Delivery Orders - NSI - co/w-deliv-orders-r.w > */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FRAME-consignee
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-consignee C-Win
ON ENTRY OF FRAME FRAME-consignee /* Ship To - Consignee */
DO:
  DISPLAY f-consignee-contact f-consignee-phone f-consignee-name 
          f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
          f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
          f-consignee-county f-consignee-country f-consignee-fax 
      WITH FRAME FRAME-consignee IN WINDOW C-Win.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-consignee C-Win
ON GO OF FRAME FRAME-consignee /* Ship To - Consignee */
DO:
    ASSIGN f-consignee-contact f-consignee-phone f-consignee-name 
           f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
           f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
           f-consignee-county f-consignee-country f-consignee-fax. 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-consignee C-Win
ON LEAVE OF FRAME FRAME-consignee /* Ship To - Consignee */
DO:
  ASSIGN  f-consignee-contact f-consignee-phone f-consignee-name 
          f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
          f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
          f-consignee-county f-consignee-country f-consignee-fax. 
      
      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FRAME-Consignor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-Consignor C-Win
ON ENTRY OF FRAME FRAME-Consignor /* Ship From - Consignor */
DO:
  DISPLAY f-consignor-whse f-whse-name f-consignor-contact f-consignor-phone 
          f-consignor-name f-consignor-addr-1 f-consignor-addr-2 
          f-consignor-addr-3 f-consignor-addr-4 f-consignor-city 
          f-consignor-state f-consignor-zip f-consignor-county 
          f-consignor-country f-consignor-fax f-consignor-contact-id 
      WITH FRAME FRAME-Consignor IN WINDOW C-Win.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-Consignor C-Win
ON LEAVE OF FRAME FRAME-Consignor /* Ship From - Consignor */
DO:
  
 
 
  ASSIGN  f-consignor-whse f-whse-name f-consignor-contact f-consignor-phone 
          f-consignor-name f-consignor-addr-1 f-consignor-addr-2 
          f-consignor-addr-3 f-consignor-addr-4 f-consignor-city 
          f-consignor-state f-consignor-zip f-consignor-county 
          f-consignor-country f-consignor-fax f-consignor-contact-id. 
      
 
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FRAME-DO-Lines
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-DO-Lines C-Win
ON GO OF FRAME FRAME-DO-Lines /* Delivery Order Lines */
DO:
/*   RUN save-do-line. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-DO-Lines C-Win
ON LEAVE OF FRAME FRAME-DO-Lines /* Delivery Order Lines */
DO:
/*   RUN save-do-line. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FRAME-Invoicee
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-Invoicee C-Win
ON ENTRY OF FRAME FRAME-Invoicee /* Bill To */
DO:
  DISPLAY f-invoice-printed CS-DO-Invoice cs-inv-freq f-invoicee-contact 
          f-invoicee-phone f-invoicee-name f-invoicee-addr-1 f-invoicee-addr-2 
          f-invoicee-addr-3 f-invoicee-addr-4 f-invoicee-city f-invoicee-state 
          f-invoicee-zip f-invoicee-county f-invoicee-country f-invoicee-fax 
      WITH FRAME FRAME-Invoicee IN WINDOW C-Win.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-Invoicee C-Win
ON GO OF FRAME FRAME-Invoicee /* Bill To */
DO:
  ASSIGN  f-invoice-printed CS-DO-Invoice cs-inv-freq f-invoicee-contact
          f-invoicee-phone f-invoicee-name f-invoicee-addr-1 f-invoicee-addr-2
          f-invoicee-addr-3 f-invoicee-addr-4 f-invoicee-city f-invoicee-state
          f-invoicee-zip f-invoicee-county f-invoicee-country f-invoicee-fax.
      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FRAME-Invoicee C-Win
ON LEAVE OF FRAME FRAME-Invoicee /* Bill To */
DO:
  ASSIGN  f-invoice-printed CS-DO-Invoice cs-inv-freq f-invoicee-contact
          f-invoicee-phone f-invoicee-name f-invoicee-addr-1 f-invoicee-addr-2
          f-invoicee-addr-3 f-invoicee-addr-4 f-invoicee-city f-invoicee-state
          f-invoicee-zip f-invoicee-county f-invoicee-country f-invoicee-fax.
      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-DO-Lines
&Scoped-define FRAME-NAME FRAME-DO-Lines
&Scoped-define SELF-NAME BROWSE-DO-Lines
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-DO-Lines C-Win
ON VALUE-CHANGED OF BROWSE-DO-Lines IN FRAME FRAME-DO-Lines
DO:
  IF NOT AVAILABLE tt-do-lines  THEN
  DO:
        FIND FIRST tt-do-lines WHERE tt-do-lines.do-num = f-do-num
             NO-ERROR.
  END.
    
  IF AVAILABLE tt-do-lines THEN
  DO:
      ASSIGN f-qty-packages     =   f-qty-packages + tt-do-lines.qty-packages              
             cs-package-type    =   IF tt-do-lines.package-type BEGINS "Box"
                THEN "Boxes" 
                ELSE IF tt-do-lines.package-type BEGINS "BU"
                THEN "Bundles" 
                ELSE IF tt-do-lines.package-type BEGINS "Co"
                THEN "Cartons" ELSE "Skids"
             f-description      =   tt-do-lines.DESCRIPTION
             
             f-class-code       =   tt-do-lines.class-code

             f-weight           =   tt-do-lines.weight
             f-do-line          =   tt-do-lines.do-line
             f-nmfc             =   tt-do-lines.nmfc.
      DISPLAY f-do-line f-qty-packages cs-package-type f-weight f-nmfc  
          f-class-code f-description 
      
      WITH FRAME FRAME-DO-Lines IN WINDOW C-Win.
       
  END.
  ELSE DO:
      ASSIGN f-do-line = 1.
      DISPLAY f-do-line WITH FRAME frame-do-lines.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME btn-clear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-clear C-Win
ON CHOOSE OF btn-clear IN FRAME DEFAULT-FRAME /* Clear */
DO:
  RUN clear-screen.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-cust-seq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-cust-seq C-Win
ON CHOOSE OF btn-cust-seq IN FRAME DEFAULT-FRAME
DO:
  ON RETURN RETURN.      
  cur-cust-num = f-cust-num.
  RUN BROWSE/custship.w (INPUT cur-cust-num).
  ASSIGN f-cust-seq = cur-cust-seq.
  FIND FIRST symcust.custaddr NO-LOCK WHERE symcust.custaddr.cust-num = cur-cust-num
      AND symcust.custaddr.cust-seq = cur-cust-seq
      NO-ERROR.
  ASSIGN f-cust-seq = cur-cust-seq.
  
  f-cust-name = IF AVAILABLE symcust.custaddr THEN 
        symcust.custaddr.NAME ELSE "".

  DISPLAY f-cust-seq f-cust-name WITH FRAME default-frame.
  APPLY "ENTRY" TO f-cust-seq IN FRAME default-frame.
   ON RETURN TAB. 
  APPLY "TAB" TO f-cust-seq IN FRAME default-frame.
  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-customer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-customer C-Win
ON CHOOSE OF btn-customer IN FRAME DEFAULT-FRAME
DO:
   ON RETURN RETURN.      
  RUN BROWSE/customer.p.
  ASSIGN f-cust-num = cur-cust-num.
  
  ASSIGN f-cust-num = cur-cust-num.
  DISPLAY f-cust-num WITH FRAME default-frame.
   ON RETURN TAB. 
  APPLY "ENTRY" TO f-cust-num IN FRAME default-frame.
  APPLY "TAB" TO f-cust-seq IN FRAME default-frame.
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-do-num
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-do-num C-Win
ON CHOOSE OF btn-do-num IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  ON RETURN RETURN.
     
  
  ASSIGN c-do-num = "" f-do-num = "".
  RUN mods/BROWSE/qy-deliv-orders-r.w (INPUT-OUTPUT c-do-num).
  ASSIGN f-do-num = c-do-num.
  
  FIND FIRST symix.do-hdr NO-LOCK WHERE do-hdr.do-num = c-do-num
      NO-ERROR.
  
  DISPLAY f-do-num WITH FRAME default-frame.
  APPLY "ENTRY" TO f-do-num IN FRAME default-frame.
  APPLY "TAB"   TO f-do-num IN FRAME default-frame.
  ON RETURN TAB.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_clear_billto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_clear_billto C-Win
ON CHOOSE OF btn_clear_billto IN FRAME DEFAULT-FRAME /* Clear Bill-To */
DO:
/*   l-clear-billto = TRUE. */
/*   CLEAR FRAME frame-invoicee ALL NO-PAUSE. */
  ASSIGN
    f-invoicee-addr-1             =   ""   
    f-invoicee-addr-2             =   ""   
    f-invoicee-addr-3             =   ""           
    f-invoicee-addr-4             =   ""   
    f-invoicee-city               =   "Rockford"                    
    f-invoicee-contact            =   ""                
    f-invoicee-country            =   ""                 
    f-invoicee-county             =   ""                  
    f-invoicee-fax                =   ""                     
    f-invoicee-name               =   "Byrne Electrical, Inc."                  
    f-invoicee-phone              =   "1-616-866-3461"                 
    f-invoicee-state              =   "MI"                   
    f-invoicee-zip                =   "49341"
    t-ship-type                   =   "PP".  

  DISPLAY f-invoicee-addr-1
          f-invoicee-addr-2
          f-invoicee-addr-3
          f-invoicee-addr-4
          f-invoicee-contact
          f-invoicee-country
          f-invoicee-county
          f-invoicee-fax
          f-invoicee-phone
          f-invoicee-name 
          f-invoicee-city
          f-invoicee-state
          f-invoicee-zip
          
      WITH FRAME frame-invoicee.

        
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_do-save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_do-save C-Win
ON CHOOSE OF btn_do-save IN FRAME DEFAULT-FRAME /* Save */
DO:   ASSIGN f-do-num.
      IF f-do-num = "" /* OR NOT AVAILABLE do-hdr */ THEN
      DO:
          MESSAGE "You cannot save with a blank delivery order number"
              VIEW-AS ALERT-BOX.
          
          APPLY "ENTRY" TO f-do-num IN FRAME default-frame.
          RETURN.
      END.
      ELSE
      RUN assign-frames.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Done
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Done C-Win
ON CHOOSE OF Btn_Done IN FRAME DEFAULT-FRAME /* Exit/Save */
DO:

  IF AVAILABLE do-hdr THEN
  DO:
       
        RUN assign-frames.
            
  END.
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-DO-Lines
&Scoped-define SELF-NAME Btn_DoSeq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_DoSeq C-Win
ON CHOOSE OF Btn_DoSeq IN FRAME FRAME-DO-Lines /* Sequences */
DO:
  APPLY "choose" TO btn_do-save IN FRAME default-frame.      
  RUN co/DoSeqGrid.p (f-do-num, f-do-line).
    
END.    /* choo/se of Btn_DoSeq */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn_line-done
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_line-done C-Win
ON CHOOSE OF btn_line-done IN FRAME FRAME-DO-Lines /* Done */
DO:
  RUN save-do-line.
  DISABLE ALL WITH FRAME frame-do-lines.
  
  ASSIGN radio-set-toolbar = "T".
  APPLY "VALUE-CHANGED" TO radio-set-toolbar 
      IN FRAME default-frame.
  ASSIGN radio-set-toolbar = "T".  
  DISPLAY radio-set-toolbar WITH FRAME default-frame.  
  APPLY "ENTRY" TO f-do-num IN FRAME default-frame.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME btn_print
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_print C-Win
ON CHOOSE OF btn_print IN FRAME DEFAULT-FRAME /* Print */
DO: ASSIGN f-do-num.

    APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME {&FRAME-NAME}.

    IF f-invoicee-name BEGINS "Byrne" THEN t-ship-type = "PP". /* byr-1004 */
        
    IF t-ship-type = ""  AND LOOKUP(SUBSTRING(f-carrier-num,1,1),"x,u") > 0 /* byr-1004 */
    THEN
    DO:
   
        t-ship-type = IF R-INDEX(f-carrier-num,"B") > 0 THEN "PP"
            ELSE IF R-INDEX(f-carrier-num,"P") > 0 THEN "PP"
            ELSE IF R-INDEX(f-carrier-num,"F") > 0 THEN "C"
            ELSE IF R-INDEX(f-carrier-num,"3") > 0 THEN "TP"
            ELSE "".
    END.
    IF t-ship-type = "" THEN /* byr-1005 */
    DO:
        IF do-hdr.invoicee-name = do-hdr.consignee-name THEN
            ASSIGN t-ship-type = "C".
        ELSE IF do-hdr.invoicee-name <> do-hdr.consignee-name THEN
            ASSIGN t-ship-type = "TP".
    END.
        
    IF f-do-num <> "" AND f-cust-num <> "" THEN
        RUN assign-frames.
    IF NOT AVAILABLE do-hdr THEN
        DO:
            MESSAGE "Delivery Order Header is not available"
                VIEW-AS ALERT-BOX.
            RETURN NO-APPLY.
        END.
    ASSIGN f-copies.
    t-copies = f-copies.
    ON RETURN RETURN.
    UPDATE skip(1) t-copies VALIDATE(t-copies <> 0, "Number of copies must be > 0")  
        COLON 40 LABEL "Number of copies? " 
        
           SKIP(1)
        WITH FRAME f-copies ROW 10 OVERLAY WIDTH 60 SIDE-LABELS CENTERED.

    HIDE FRAME f-copies. PAUSE(0).
        
    ON RETURN TAB.    
        
    
    DISABLE f-do-num WITH FRAME default-frame.
    OUTPUT STREAM t TO TERMINAL.
        ASSIGN t-source     = "r:\601\mods\templates\bill-of-lading.xls".    
               t-target     = "c:\temp\bill-of-lading.xls".
                     OS-COMMAND silent DEL value(t-target).
                     OS-COMMAND SILENT COPY VALUE(t-source) VALUE(t-target).

                     CREATE "Excel.Application" chExcelApplication.

                     /* launch Excel so it is visible to the user */
                     chExcelApplication:Visible = FALSE.

                     /* create a new Workbook */
                    /* chWorkbook = chExcelApplication:Workbooks:Add().*/

                      chexcelApplication:workbooks:open(t-target) no-error.
                      
                      chWorkSheet = chExcelApplication:Sheets:Item(1).
                      

        
        cRange = "I" + "01".
                  chWorkSheet:Range(cRange):Value = TRIM(do-hdr.do-num).
                   

        cRange = "I" + "02".
                            chWorkSheet:Range(cRange):Value = STRING(do-hdr.do-hdr-date,"99/99/99").
        cRange = "I" + "01".
                  chWorkSheet:Range(cRange):Value = TRIM(do-hdr.do-num).

                  

                  cRange = "I" + "01".
                            chWorkSheet:Range(cRange):Value = TRIM(do-hdr.do-num).
                  cRange = "I" + "02".
                                      chWorkSheet:Range(cRange):Value = STRING(do-hdr.do-hdr-date,"99/99/99").
                  cRange = "I" + "03".
                            chWorkSheet:Range(cRange):Value = TRIM(do-hdr.carrier). /* for carrier # */
                  cRange = "I" + "04".
                            chWorkSheet:Range(cRange):Value = TRIM(do-hdr.veh-num).
                  cRange = "I" + "05".
                            chWorkSheet:Range(cRange):Value = STRING(do-hdr.pro-number).
                  cRange = "B" + "06".
                          chWorkSheet:Range(cRange):Value = do-hdr.special-inst[01] + do-hdr.special-inst[02].
                  
                  cRange = "H" + "07".
                          chWorkSheet:Range(cRange):Value = 
                                IF l-clear-billto = TRUE THEN ""
                                        ELSE string(do-hdr.cust-seq). /* was cust-num */
                  cRange = "A" + "14".
/*                   cRange = "F" + "08". */
                          chWorkSheet:Range(cRange):Value = 
                                IF l-clear-billto = TRUE THEN "" 
                                ELSE TRIM(do-hdr.invoicee-name).
                  cRange = "A" + "15".
/*                   cRange = "F" + "09". */
                          chWorkSheet:Range(cRange):Value = 
                                IF l-clear-billto = TRUE THEN ""
                                   ELSE STRING(do-hdr.invoicee-addr[01]).
                  cRange = "A" + "16".
/*                   cRange = "F" + "10". */
                          chWorkSheet:Range(cRange):Value = 
                                IF l-clear-billto = TRUE THEN "" 
                                ELSE 
                                IF do-hdr.invoicee-addr[02] = "" THEN
                              do-hdr.invoicee-city + ", " + do-hdr.invoicee-state + " " + do-hdr.invoicee-zip
                              ELSE do-hdr.invoicee-addr[02]. 
                  cRange = "A" + "17".
/*                   cRange = "F" + "11". */
                               chWorkSheet:Range(cRange):Value = 
                          IF l-clear-billto = TRUE THEN "" 
                          ELSE IF do-hdr.invoicee-addr[02] <> "" AND do-hdr.invoicee-addr[03] = "" THEN
                                   do-hdr.invoicee-city + ", " + do-hdr.invoicee-state + " " + do-hdr.invoicee-zip
                                   ELSE do-hdr.invoicee-addr[03].
                                   

                  cRange = "A" + "18".
/*                   cRange = "F" + "12". */
                             chWorkSheet:Range(cRange):Value = 
                               IF l-clear-billto = TRUE THEN ""
                               ELSE 
                                    IF do-hdr.invoicee-addr[02] <> "" AND do-hdr.invoicee-addr[03] <> "" THEN
                                 do-hdr.invoicee-city + ", " + do-hdr.invoicee-state + " " + do-hdr.invoicee-zip
                                 ELSE "" .
                   cRange = "C" + "13".
                          chWorkSheet:Range(cRange):Value = TRIM(do-hdr.cust-num).
/*                    cRange = "C" + "13".                                                 */
/*                              chWorkSheet:Range(cRange):Value = STRING(do-hdr.cust-seq). */
                   cRange = "F" + "08".
/*                    cRange = "A" + "14". */
                            chWorkSheet:Range(cRange):Value = TRIM(do-hdr.consignee-name).
                    cRange = "F" + "09".
/*                     cRange = "A" + "15". */
                            chWorkSheet:Range(cRange):Value = STRING(do-hdr.consignee-addr[01]).
                    cRange = "F" + "10".
/*                     cRange = "A" + "16". */
                            chWorkSheet:Range(cRange):Value = IF do-hdr.consignee-addr[02] = "" THEN
                                do-hdr.consignee-city + ", " + do-hdr.consignee-state + " " + do-hdr.consignee-zip
                                ELSE do-hdr.consignee-addr[02].
                    cRange = "F" + "11".
/*                     cRange = "A" + "17". */
                                 chWorkSheet:Range(cRange):Value = IF do-hdr.consignee-addr[02] <> "" AND do-hdr.consignee-addr[03] = "" THEN
                                     do-hdr.consignee-city + ", " + do-hdr.consignee-state + " " + do-hdr.consignee-zip
                                     ELSE do-hdr.consignee-addr[03].

                    cRange = "F" + "12".
/*                     cRange = "A" + "18". */

                               chWorkSheet:Range(cRange):Value = IF do-hdr.consignee-addr[02] <> "" AND do-hdr.consignee-addr[03] <> "" THEN
                                   do-hdr.consignee-city + ", " + do-hdr.consignee-state + " " + do-hdr.consignee-zip
                                   ELSE "".
IF t-ship-type <> "" THEN
DO:
        IF t-ship-type = "PP" THEN
        DO:
            cRange = "I" + "17".
            chWorkSheet:Range(cRange):Value = "X".
        END.
        ELSE IF t-ship-type = "TP" THEN
        DO:
            cRange = "I" + "19".
            chWorkSheet:Range(cRange):Value = "X".
        END.
END.
                    irow = 22.
                    FOR EACH symix.do-line NO-LOCK WHERE do-line.do-num = do-hdr.do-num.
                        cRange = STRING(iRow).
                        cRange = "A" + STRING(iRow).
                        chWorkSheet:Range(cRange):Value = do-line.qty-packages.
                        cRange = "B" + STRING(iRow).
                        chWorkSheet:Range(cRange):Value = IF do-line.package-type = "B" THEN "Boxes" 
                            ELSE IF do-line.package-type = "U" THEN "Bundles" 
                            ELSE IF do-line.package-type = "C" THEN "Cartons" 

                            ELSE IF do-line.package-type = "S" THEN "Skids"
                                ELSE "".
                        cRange = "C" + STRING(iRow).
                        chWorkSheet:Range(cRange):Value = do-line.DESCRIPTION.
                        cRange = "I" + STRING(iRow).
                        IF do-line.KEY <> 0 THEN
                        DO:
                            FIND FIRST symix.notes NO-LOCK WHERE notes.KEY = do-line.KEY
                                AND notes.seq = 1
                                NO-ERROR.

                        END.
                        chWorkSheet:Range(cRange):Value = IF AVAILABLE notes AND notes.txt <> ""
                            THEN substring(notes.txt,1,10) 
                            ELSE IF do-line.nmfc <> "" THEN do-line.nmfc
                            ELSE "061130-08".
                        cRange = "J" + STRING(iRow).

                        IF do-line.KEY <> 0 THEN
                        DO:
                            FIND FIRST symix.notes NO-LOCK WHERE notes.KEY = do-line.KEY
                                AND notes.seq = 2
                                NO-ERROR.

                        END.
                        chWorkSheet:Range(cRange):Value = IF AVAILABLE notes AND notes.txt <> ""
                            THEN substring(notes.txt,1,10) 
                            ELSE IF do-line.rate-code <> "" THEN do-line.rate-code
                            ELSE "77.5".
                        cRange = "K" + STRING(iRow).
                        chWorkSheet:Range(cRange):Value = do-line.weight.
                        iRow = iRow + 1.
                        
                    END.

                    PrinterName =   SESSION:PRINTER-NAME.

                    chExcelApplication:DisplayAlerts = FALSE.
                       f-copies = t-copies.
                       ASSIGN f-origin-copies = t-copies i-pages = 1.
/*                        IF t-send-email = FALSE AND t-send-fax = FALSE THEN */
                       DO l-copies = 1 TO f-origin-copies: /* 11/13/07 */
                          chWorksheet:PrintOut(1,i-pages,1,FALSE,printername,FALSE, FALSE).
                       END.

                   
                    chExcelApplication:Visible = FALSE.
                        
                    
                    no-return-value chExcelApplication:CLOSE(t-target) no-error.

                   no-return-value chExcelApplication:QUIT.         
                   IF VALID-HANDLE(chExcelApplication) THEN
                   RELEASE OBJECT chExcelApplication.      

                   /*RELEASE OBJECT chWorkbook.*/
                   IF VALID-HANDLE(chWorksheet) THEN
                   RELEASE OBJECT chWorksheet.

                       PAUSE(2) NO-MESSAGE.

               /*             {mods/closeexcel.i} */

  ASSIGN t-print-label
         txtnumlabels
         txtqty
         cmbprinter
         tgldefaultprinter.

  ENABLE f-do-num WITH FRAME {&FRAME-NAME}.             
  CLEAR FRAME default-frame ALL NO-PAUSE.
  CLEAR FRAME frame-do-lines ALL NO-PAUSE.
  CLEAR FRAME frame-consignee ALL NO-PAUSE.
  CLEAR FRAME frame-consignor ALL NO-PAUSE.
  CLEAR FRAME frame-invoicee  ALL NO-PAUSE.
  APPLY "ENTRY" TO f-consignee-name IN FRAME frame-consignee.
  
  EMPTY TEMP-TABLE tt-do-lines.
  ASSIGN
  f-do-line = 1
  f-description = "WIRING COMPONENTS"
  radio-set-toolbar = "T".

  RUN clear-do-hdr.
  
  f-do-num = "".
  RUN ENABLE_ui.
  
  APPLY "ENTRY" TO f-do-num IN FRAME {&FRAME-NAME}.  
  APPLY "VALUE-CHANGED" TO radio-set-plant   IN FRAME default-frame.
  APPLY "VALUE-CHANGED" TO radio-set-toolbar IN FRAME default-frame.
    
  ASSIGN l-clear-billto = FALSE.
  
/*   ASSIGN t-print-label      */
/*          txtnumlabels       */
/*          txtqty             */
/*          cmbprinter         */
/*          tgldefaultprinter. */
    
  IF t-print-label THEN /* by4-1006 - 06/23/10 */
  DO:
       IF cmbprinter:SCREEN-VALUE IN FRAME default-frame = ? THEN
    DO:
        MESSAGE "Printer selection is blank.  Please select printer from drop down list"
            VIEW-AS ALERT-BOX.
        RETURN NO-APPLY.
    END.

    IF tglDefaultPrinter = TRUE THEN DO:
      OUTPUT STREAM sDefPrinter TO c:\temp\defaultLabelPrinter.txt.
      
      PUT STREAM sDefPrinter
          cmbPrinter.
      
      OUTPUT STREAM sDefPrinter CLOSE.
    END.
        
       OS-COMMAND SILENT DEL C:\TEMP\BOL-LBL.TXT.
    
    outfile = "C:\Temp\bol-lbl.txt".
        
    OUTPUT STREAM label-stream TO VALUE(outfile).
    PUT STREAM label-stream 
        "Name|Addr1|Addr2|Addr3|City|State|PostalCode|Country" SKIP.
    OUTPUT STREAM label-stream CLOSE.  
        
    DO xx = 1 TO txtNumLabels:
          ASSIGN 
              ctr = ctr + 1.

          /* BYR-1000 Created loop to create unique bar-po-id if it already exists*/
          REPEAT:
              ASSIGN
                  t-stamp-bc    = STRING(YEAR (TODAY), "9999")              + 
                                  STRING(MONTH(TODAY), "99")                + 
                                  STRING(DAY  (TODAY), "99")                +
                                  STRING(TIME)                              +
                                  STRING(txtNumLabels, "999")               +           
                                  STRING(ctr, "999")
                  t-stamp-char  = STRING(YEAR (TODAY), "9999")  + "/"       +
                                  STRING(MONTH(TODAY), "99")    + "/"       +
                                  STRING(DAY  (TODAY), "99")    + ":"       +
                                  STRING(TIME, "HH:MM:SS")                  +
                                  STRING(txtNumLabels, "999")               +
                                  STRING(ctr, "999").
    
/*               FIND FIRST symix.bar-po NO-LOCK WHERE             */
/*                   symix.bar-po.bar-po-id = t-stamp-bc NO-ERROR. */
/*               IF AVAIL symix.bar-po THEN                        */
/*                   PAUSE(1).                                     */
/*               ELSE                                              */
/*                   LEAVE.                                        */
              LEAVE.
          END.

         
          
          

    
    
      
      OUTPUT STREAM label-stream TO VALUE(outfile) APPEND.
        PUT STREAM label-stream

            caps(do-hdr.Consignee-name) FORMAT "x(40)"
            "|"                              
            caps(do-hdr.consignee-addr[01]) FORMAT "x(40)" 
            "|" 
            caps(do-hdr.consignee-addr[02]) FORMAT "x(40)" 
            "|"  
            caps(do-hdr.consignee-addr[03]) FORMAT "x(40)" 
            "|"
            caps(do-hdr.consignee-city) FORMAT "x(15)" 
            "|"         
            caps(do-hdr.consignee-state) FORMAT "x(6)"    
            "|" 
             caps(do-hdr.consignee-zip) FORMAT "x(6)"
            "|"
             caps(do-hdr.consignee-country) FORMAT "x(15)"
            SKIP.

        OUTPUT STREAM label-stream CLOSE.

    END. /* numlabels */

    x-path = 'bartend /run /x /p /r=1 /D="C:\Temp\bol-lbl.txt" /f="l:\production 1\bartender labels\address.btw"' + '/c="1"' + '/prn=' + cmbPrinter .
      OS-COMMAND SILENT value(x-path).

/*     OS-COMMAND SILENT (cd /d "c:\program files\seagull\bartender\7.51\" & value(x-path)).  */

            
  END.
    
  APPLY "ENTRY" TO f-do-num IN FRAME default-frame.  

            


END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-Consignor
&Scoped-define SELF-NAME btn_whse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_whse C-Win
ON CHOOSE OF btn_whse IN FRAME FRAME-Consignor
DO:
  RUN BROWSE/whse.p.
  ASSIGN f-consignor-whse = cur-whse.
  FIND FIRST symix.whse NO-LOCK WHERE whse.whse = cur-whse 
      NO-ERROR.
  f-whse-name = IF AVAILABLE whse THEN whse.NAME
      ELSE "".
  DISPLAY f-whse-name f-consignor-whse
      WITH FRAME frame-consignor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME cb-carrier
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cb-carrier C-Win
ON VALUE-CHANGED OF cb-carrier IN FRAME DEFAULT-FRAME /* Carrier # */
DO:
  DEF VAR ic AS INTE            NO-UNDO.
  ASSIGN cb-carrier.
  
  
  FIND FIRST symix.shipcode NO-LOCK WHERE shipcode.DESCRIPTION = trim(cb-carrier)
      NO-ERROR.
  ASSIGN
             f-carrier-num = IF AVAILABLE shipcode THEN shipcode.ship-code
                 ELSE ""
             f-shipcode    = IF AVAILABLE shipcode THEN shipcode.ship-code
                 ELSE "".
                        
  IF LOOKUP(f-carrier-num,"chr,cela,unis") > 0 THEN
  RUN "third-party".

  
   
    
  DISPLAY cb-carrier f-carrier-num f-shipcode WITH FRAME default-frame.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f-Carrier-num
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-Carrier-num C-Win
ON GO OF f-Carrier-num IN FRAME DEFAULT-FRAME /* Carrier # */
OR tab, RETURN OF f-carrier-num IN FRAME default-frame
DO:
    ASSIGN f-carrier-num.
    FIND FIRST symix.shipcode NO-LOCK WHERE shipcode.ship-code = f-carrier-num
        NO-ERROR.
    IF f-carrier-num = "" OR NOT AVAILABLE shipcode THEN
    DO:
        MESSAGE "Invalid carrier code: " f-carrier-num
            VIEW-AS ALERT-BOX.
        RETURN NO-APPLY.

    END.
    ASSIGN cb-carrier   =   shipcode.DESCRIPTION
           f-shipcode   =   shipcode.ship-code.
    DISPLAY cb-carrier f-carrier-num f-shipcode
        WITH FRAME default-frame.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f-copies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-copies C-Win
ON LEAVE OF f-copies IN FRAME DEFAULT-FRAME /* Copies */
DO:
  ASSIGN f-copies.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f-cust-num
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-cust-num C-Win
ON GO OF f-cust-num IN FRAME DEFAULT-FRAME /* Customer */
OR tab, RETURN OF f-cust-num IN FRAME default-frame

DO: 
    ASSIGN f-cust-num.
    FIND FIRST symcust.custaddr NO-LOCK WHERE custaddr.cust-num = f-cust-num
        NO-ERROR.
    IF NOT AVAILABLE custaddr OR f-cust-num = "" THEN
    DO:
        MESSAGE "Invalid customer number"
            VIEW-AS ALERT-BOX.
        RETURN NO-APPLY.
    END.
    FIND FIRST symix.customer WHERE customer.cust-num = f-cust-num
        AND customer.cust-seq   = 0
        NO-ERROR.
    IF AVAILABLE customer THEN
        FIND FIRST symix.shipcode NO-LOCK WHERE shipcode.ship-code =
        customer.ship-code
        NO-ERROR.
    f-cust-num = CAPS(f-cust-num).
    FIND FIRST bcustaddr NO-LOCK WHERE bcustaddr.cust-num = f-cust-num
        AND bcustaddr.cust-seq = 0
        NO-ERROR.
    FIND FIRST bcustomer NO-LOCK WHERE bcustomer.cust-num = f-cust-num
        AND bcustomer.cust-seq = 0
        NO-ERROR.
    ASSIGN f-cust-name = symcust.custaddr.NAME.
    FOR EACH symix.shipcode NO-LOCK WHERE shipcode.ship-code <> "".
        z = z + 1.
        IF shipcode.ship-code = customer.ship-code THEN LEAVE.
    END.
    
               cb-carrier = SUBSTRING(shipcode.DESCRIPTION,1,25).
    FIND FIRST symix.shipcode NO-LOCK WHERE shipcode.DESCRIPTION = cb-carrier
        NO-ERROR.

    ASSIGN
    f-carrier-num     = IF AVAILABLE shipcode THEN caps(shipcode.ship-code) ELSE ""
    f-shipcode        = IF AVAILABLE shipcode THEN caps(shipcode.ship-code) ELSE "".

    

    DISPLAY f-cust-num f-cust-name cb-carrier  f-shipcode
        f-carrier-num WITH FRAME default-frame.

    APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME default-frame.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f-cust-seq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-cust-seq C-Win
ON GO OF f-cust-seq IN FRAME DEFAULT-FRAME /* Ship To */
OR tab, RETURN, leave OF f-cust-seq IN FRAME default-frame
DO:
    ASSIGN f-cust-seq.
    FIND FIRST symix.customer WHERE customer.cust-num = f-cust-num
        AND customer.cust-seq = f-cust-seq
        NO-ERROR.
    IF NOT AVAILABLE customer THEN
    DO:
        MESSAGE "Invalid customer: " f-cust-num f-cust-seq
            VIEW-AS ALERT-BOX.
        RETURN NO-APPLY.
    END.
    FIND FIRST symcust.custaddr WHERE symcust.custaddr.cust-num = f-cust-num
        AND symcust.custaddr.cust-seq = f-cust-seq
        NO-ERROR.
    IF NOT AVAILABLE symcust.custaddr THEN
    DO:
        MESSAGE "Invalid customer: " f-cust-num f-cust-seq
            VIEW-AS ALERT-BOX.
        RETURN NO-APPLY.
    END.
    ASSIGN
        f-consignee-contact =   customer.contact[01]
        f-consignee-phone   =   customer.phone[01]
        f-consignee-name    =   custaddr.NAME
        f-consignee-addr-1  =   custaddr.addr[01]
        f-consignee-addr-2  =   custaddr.addr[02]
        f-consignee-addr-3  =   custaddr.addr[03]
        f-consignee-addr-4  =   custaddr.addr[04].
    ASSIGN
        f-consignee-city    =   custaddr.city
        f-consignee-state   =   custaddr.state
        f-consignee-zip     =   custaddr.zip
        f-consignee-county  =   custaddr.county
        f-consignee-country =   IF custaddr.country <> ""
            THEN custaddr.country ELSE "USA"
        f-consignee-fax     =   custaddr.fax.

    FIND FIRST bcustaddr NO-LOCK WHERE bcustaddr.cust-num =
        f-cust-num 
        AND bcustaddr.cust-seq = 0
        NO-ERROR.

    FIND FIRST bcustomer NO-LOCK WHERE bcustomer.cust-num =
        f-cust-num
        AND bcustomer.cust-seq = 0
        NO-ERROR.
    ASSIGN
      f-consignor-contact =   ""
      f-consignor-phone   =   parms.phone
      f-consignor-name    =   "Byrne Electrical"
      f-consignor-addr-1  =   parms.addr[01]
      f-consignor-addr-2  =   parms.addr[02]
      f-consignor-addr-3  =   parms.addr[03]
      f-consignor-addr-4  =   parms.addr[04].
  ASSIGN
      f-consignor-name    =   "Byrne Electrical"
      f-consignor-city    =   parms.city
      f-consignor-state   =   parms.state
      f-consignor-zip     =   parms.zip
      f-consignor-county  =   "KENT"
      f-consignor-whse    =   "MAIN"
      f-whse-name         =   "MAIN"
      f-consignor-country =   IF parms.country <> ""
        THEN parms.country ELSE "USA"
      f-consignor-fax     =   "1-616-866-3449".

  IF AVAILABLE bcustaddr AND lookup(f-carrier-num,"CHR,CELA") = 0 THEN
  ASSIGN
      f-invoicee-addr-1             =   bcustaddr.addr[01]
      f-invoicee-addr-2             =   bcustaddr.addr[02]
      f-invoicee-addr-3             =   bcustaddr.addr[03]
      f-invoicee-addr-4             =   bcustaddr.addr[04]
      f-invoicee-city               =   bcustaddr.city
      f-invoicee-contact            =   bcustomer.contact[01]
      f-invoicee-country            =   IF bcustaddr.country  <> ""
            THEN bcustaddr.country ELSE "USA"
      f-invoicee-county             =   bcustaddr.county
      f-invoicee-fax                =   bcustaddr.fax-num
      f-invoicee-name               =   bcustaddr.name
      f-invoicee-phone              =   bcustomer.phone[01]
      f-invoicee-state              =   bcustaddr.state
      f-invoicee-zip                =   bcustaddr.zip.

   APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME {&FRAME-NAME}.



    DISPLAY f-consignee-contact f-consignee-phone f-consignee-name 
          f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
          f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
          f-consignee-county f-consignee-country f-consignee-fax 
      WITH FRAME FRAME-consignee IN WINDOW C-Win.
    RUN ENABLE_UI.

    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-DO-Lines
&Scoped-define SELF-NAME f-do-line
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-do-line C-Win
ON GO OF f-do-line IN FRAME FRAME-DO-Lines /* Line */
OR RETURN, TAB OF f-do-line IN FRAME FRAME-do-lines
DO:
  ASSIGN f-do-line.
  IF f-do-line = 0  THEN
  DO:
      MESSAGE "Delivery order line cannot be zero"
          VIEW-AS ALERT-BOX.
      RETURN NO-APPLY.
  END.
  FIND FIRST tt-do-lines
      WHERE tt-do-lines.do-num = f-do-num 
      AND tt-do-lines.do-line  = f-do-line
      NO-ERROR.
  IF AVAILABLE tt-do-lines THEN
  DO:
      ASSIGN f-qty-packages     =   tt-do-lines.qty-packages
             cs-package-type    =   tt-do-lines.package-type
             f-description      =   tt-do-lines.DESCRIPTION
             
             f-class-code       =   tt-do-lines.class-code
             f-weight           =   tt-do-lines.weight
             f-nmfc             =   IF tt-do-lines.nmfc <> ""
                    THEN tt-do-lines.nmfc
                    ELSE "061130-08".
      DISPLAY f-do-line f-qty-packages cs-package-type f-weight f-nmfc  
          f-class-code f-description 
      WITH FRAME FRAME-DO-Lines IN WINDOW C-Win.

  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME f-do-num
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-do-num C-Win
ON ENTRY OF f-do-num IN FRAME DEFAULT-FRAME /* DO/BOL */
DO:
  
  ASSIGN f-do-hdr-date = TODAY
         cb-stat        = "Approved".

  DISPLAY f-do-hdr-date cb-stat WITH FRAME default-frame.
  
  ASSIGN radio-set-toolbar = "L".
  APPLY "VALUE-CHANGE" TO radio-set-toolbar IN FRAME default-frame.
  ASSIGN radio-set-toolbar = "F".
  APPLY "VALUE-CHANGE" TO radio-set-toolbar IN FRAME default-frame.
  ASSIGN radio-set-toolbar = "I".
  APPLY "VALUE-CHANGE" TO radio-set-toolbar IN FRAME default-frame.
/*   VIEW FRAME frame-do-line. */
  ASSIGN radio-set-toolbar = "T".
  APPLY "VALUE-CHANGE" TO radio-set-toolbar IN FRAME default-frame.
  HIDE FRAME frame-do-lines.
    
    /* BYR-1089 - add check for screen value of radio-set-toolbar */
    CASE radio-set-toolbar:SCREEN-VALUE IN FRAME default-frame:

        WHEN "T" THEN DO:

            ENABLE ALL WITH FRAME frame-consignee.
            VIEW FRAME frame-consignee.

        END.    /* Ship To */

        WHEN "F" THEN DO:

            ENABLE ALL WITH FRAME frame-consignor.
            VIEW FRAME frame-consignor.

        END.    /* Ship From */

        WHEN "I" THEN DO:

            ENABLE ALL WITH FRAME frame-invoicee.
            VIEW FRAME frame-invoicee.

        END.    /* */

        WHEN "L" THEN DO:

            ENABLE ALL WITH FRAME frame-do-lines.
            VIEW FRAME frame-do-lines.

        END.    /* */

    END CASE.   /* BYR-1089 - radio-set-toolbar */

  DISPLAY f-consignee-name WITH FRAME frame-consignee.
  f-copies = IF f-copies = 0 THEN 1 ELSE f-copies.
  DISPLAY f-copies WITH FRAME {&FRAME-NAME}.
  
/*   ENABLE f-lot-num WITH FRAME default-frame. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-do-num C-Win
ON GO OF f-do-num IN FRAME DEFAULT-FRAME /* DO/BOL */
OR tab,RETURN OF f-do-num IN FRAME default-frame
DO:
  ASSIGN f-do-num.
    IF f-do-num = "" THEN
    DO:
            MESSAGE "Delivery order number cannot be blank"
                    VIEW-AS ALERT-BOX.
            RETURN NO-APPLY.
    END.
  f-copies = 1.
  DISPLAY f-copies WITH FRAME {&FRAME-NAME}.

  DISABLE f-lot-num WITH FRAME default-frame.
  
  f-do-num = CAPS(f-do-num).
/*    
  RUN lib\expnd-ky.p (INPUT 30, INPUT-OUTPUT f-do-num).
             /* BYR-1089 */
*/  
  DISPLAY f-do-num WITH FRAME default-frame.

  IF f-do-num <> "" THEN
  DO: IF f-lot-num = "" THEN 
      ASSIGN f-cust-num = ""
             f-cust-seq = 0
             f-copies   = 1
             f-cust-name = "".
      FIND FIRST symix.do-hdr WHERE do-hdr.do-num = f-do-num
          NO-ERROR.
      IF AVAILABLE do-hdr THEN
      DO:
      
      FIND FIRST symcust.custaddr NO-LOCK WHERE 
          symcust.custaddr.cust-num = do-hdr.cust-num
          AND symcust.custaddr.cust-seq = do-hdr.cust-seq
          NO-ERROR.
      IF AVAILABLE symcust.custaddr THEN
          ASSIGN f-cust-num     = do-hdr.cust-num
                 f-cust-seq     = do-hdr.cust-seq
                 f-cust-name    = symcust.custaddr.NAME.
      FIND FIRST bcustaddr NO-LOCK WHERE bcustaddr.cust-num = f-cust-num
          AND bcustaddr.cust-seq = 0 
          NO-ERROR.
      FIND FIRST symix.shipcode NO-LOCK WHERE shipcode.ship-code = do-hdr.carrier-num
          NO-ERROR.

      IF AVAILABLE shipcode THEN
      DO:
          
               cb-carrier = SUBSTRING(shipcode.DESCRIPTION,1,25).
/*                cb-carrier = vtp-code + " - " +        */
/*                 SUBSTRING(shipcode.DESCRIPTION,1,25). */

            ASSIGN
            f-carrier-num = IF AVAILABLE shipcode THEN caps(shipcode.ship-code) ELSE ""
            f-shipcode    = IF AVAILABLE shipcode THEN caps(shipcode.ship-code) ELSE "".
            DISPLAY f-cust-num f-cust-name cb-carrier f-shipcode
                f-carrier-num WITH FRAME default-frame.
        
            APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME default-frame.
      END.


      FIND FIRST bcustomer NO-LOCK WHERE bcustomer.cust-num = f-cust-num
          AND bcustomer.cust-seq = 0
          NO-ERROR.
      ASSIGN
/*       cb-stat        = IF do-hdr.stat BEGINS "I" THEN "In Process" */
/*                         ELSE "Approved".                           */
      cb-stat = "approved".      
      IF AVAILABLE do-hdr THEN
          ASSIGN
      f-do-hdr-date = do-hdr.do-hdr-date
      f-do-value    = do-hdr.do-value
      f-curr-code   = IF do-hdr.curr-code <> "" THEN do-hdr.curr-code
          ELSE IF symcust.custaddr.curr-code <> "" THEN symcust.custaddr.curr-code
          ELSE "USE".
      IF AVAILABLE do-hdr THEN
      DO: 

          FIND FIRST symix.shipcode NO-LOCK WHERE shipcode.ship-code = 
              do-hdr.carrier-num 
              NO-ERROR.
         IF AVAILABLE shipcode THEN  
         DO:
         
          vtp-code    =   do-hdr.carrier-num.
               vtp-code + FILL(" ", 7 - LENGTH(vtp-code)).
               cb-carrier = SUBSTRING(shipcode.DESCRIPTION,1,25).

    
             f-carrier-num = IF AVAILABLE shipcode THEN caps(shipcode.ship-code) ELSE "".
             DISPLAY f-cust-num f-cust-name cb-carrier
             f-carrier-num WITH FRAME default-frame.
    
             APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME default-frame.
         END.
          ASSIGN 
              f-carrier-num         = do-hdr.carrier-num
              cb-carrier            = do-hdr.carrier
              f-carrier-contact    = do-hdr.carrier-contact
              f-veh-num            = do-hdr.veh-num
              f-do-value           = do-hdr.do-value
              f-do-hdr-date        = do-hdr.do-hdr-date
              f-pro-number         = do-hdr.pro-number
              f-weight             = do-hdr.weight
              f-packages           = do-hdr.qty-packages
              f-pickup-date        = do-hdr.pickup-date
              RADIO-SET-toolbar.
        
        
                
                
      END.
      
      IF AVAILABLE bcustaddr AND lookup(do-hdr.carrier-num,"chr,cela,UNIS") = 0 THEN
    ASSIGN
        f-invoicee-addr-1             =   bcustaddr.addr[01]
        f-invoicee-addr-2             =   bcustaddr.addr[02]
        f-invoicee-addr-3             =   bcustaddr.addr[03]
        f-invoicee-addr-4             =   bcustaddr.addr[04]
        f-invoicee-city               =   bcustaddr.city
        f-invoicee-contact            =   bcustomer.contact[01]
        f-invoicee-country            =   bcustaddr.country
        f-invoicee-county             =   bcustaddr.county
        f-invoicee-fax                =   bcustaddr.fax-num
        f-invoicee-name               =   bcustaddr.name
        f-invoicee-phone              =   bcustomer.phone[01]
        f-invoicee-state              =   bcustaddr.state
        f-invoicee-zip                =   bcustaddr.zip
        t-ship-type                   =   "".
            
               
        APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME {&FRAME-NAME}.


          
          RUN assign-do-hdr-in.

      END.
      ELSE IF NOT AVAILABLE do-hdr THEN
      DO:
/*           RUN clear-do-hdr.  */
          ASSIGN
        f-do-hdr-date       =   TODAY
        f-pickup-date       =   TODAY
        cb-stat             =   "Approved"
        f-consignor-contact =   ""
        f-consignor-phone   =   parms.phone
        f-consignor-name    =   "Byrne Electrical"
        f-consignor-addr-1  =   parms.addr[01]
        f-consignor-addr-2  =   parms.addr[02]
        f-consignor-addr-3  =   parms.addr[03]
        f-consignor-addr-4  =   parms.addr[04].
    ASSIGN
        f-consignor-city    =   parms.city
        f-consignor-state   =   parms.state
        f-consignor-zip     =   parms.zip
        f-consignor-county  =   "KENT"
        f-consignor-whse    =   "MAIN"
        f-whse-name         =   "MAIN"
        f-consignor-country =   IF parms.country <> ""
            THEN parms.country ELSE "USA"
        f-consignor-fax     =   "1-616-866-3449".

    
        
        ASSIGN
        f-do-line = 1.
         
      END.
      RUN ENABLE_ui.

      DISPLAY f-do-num cb-stat f-cust-num f-cust-seq f-do-value f-do-hdr-date 
          f-cust-name f-curr-code f-shipcode cb-Carrier 
          f-carrier-contact f-veh-num f-pro-number f-weight f-packages 
          f-carrier-num
          f-pickup-date RADIO-SET-toolbar 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
      EMPTY TEMP-TABLE tt-do-lines.
      RUN create-temp-table.
      i-weight = 0.
      FOR EACH tt-do-lines.
          i-weight = i-weight + tt-do-lines.weight.
          f-weight = IF i-weight > 0 THEN i-weight ELSE f-weight.
          DISPLAY f-weight WITH FRAME default-frame.
    
      END.
      
      f-weight = IF i-weight > 0 THEN i-weight ELSE f-weight.
      DISPLAY f-weight WITH FRAME default-frame.
        
      /* BYR-1008 */ 
      IF AVAIL do-hdr THEN DO:
            FIND FIRST symix.do-line OF do-hdr NO-LOCK NO-ERROR.
            IF AVAIL do-line THEN
                    ASSIGN txtnumlabels:SCREEN-VALUE IN FRAME default-frame = STRING(do-line.qty-packages).
      END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f-lot-num
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-lot-num C-Win
ON GO OF f-lot-num IN FRAME DEFAULT-FRAME /* Lot Num */
OR tab, RETURN OF f-lot-num IN FRAME default-frame
DO:
    l-clear-billto = FALSE.

    ASSIGN f-lot-num.
    IF f-lot-num = "" THEN
    DO:
        MESSAGE "Lot number cannot be blank"
            VIEW-AS ALERT-BOX.
        RETURN NO-APPLY.
    END.
    Ix = INDEX(f-lot-num,"-").
    IF ix > 0 THEN
    DO: 
        f-co-num    = SUBSTRING(f-lot-num,1,ix - 1).
        RUN lib/expnd-ky.p (INPUT 7, INPUT-OUTPUT f-co-num).
        
        
        FIND FIRST symix.co NO-LOCK WHERE co.co-num = f-co-num
            NO-ERROR.
        IF NOT AVAILABLE co THEN
        DO:
            MESSAGE "Invalid order number: " f-co-num
                VIEW-AS ALERT-BOX.
            RETURN NO-APPLY.
        END.
        FIND FIRST symcust.custaddr NO-LOCK WHERE symcust.custaddr.cust-num
            = co.cust-num 
            AND symcust.custaddr.cust-seq = 0
            NO-ERROR.
        ASSIGN f-cust-num = co.cust-num
               f-cust-seq = co.cust-seq.
        
        
    END.
    ELSE IF SUBSTRING(f-lot-num,1,1) <> "S" 
        AND ix = 0 AND LENGTH(TRIM(f-lot-num)) = 6 THEN
    DO:
        RUN lib/expnd-ky.p (INPUT 7, INPUT-OUTPUT f-lot-num).
        FIND FIRST symix.co NO-LOCK WHERE co.co-num = f-lot-num
            NO-ERROR.
        IF NOT AVAILABLE co THEN
        DO:
            MESSAGE "Invalid order number: " f-lot-num
                VIEW-AS ALERT-BOX.
            RETURN NO-APPLY.
        END.            
        
        
        
         
         FIND FIRST symcust.custaddr NO-LOCK WHERE symcust.custaddr.cust-num
            = co.cust-num 
            AND symcust.custaddr.cust-seq = 0
            NO-ERROR.

         ASSIGN f-cust-num = co.cust-num
               f-cust-seq = co.cust-seq.
        

    END.
    ELSE IF SUBSTRING(f-lot-num,1,1) = "S" THEN
    DO:

        MESSAGE "Consolidated Lots cannot be Processed"
            VIEW-AS ALERT-BOX.
        RETURN NO-APPLY.
        APPLY "ENTRY" TO f-lot-num IN FRAME default-frame.
    
                
    END.

    DISPLAY f-cust-num f-cust-seq WITH FRAME default-frame.
    APPLY "TAB" TO f-cust-num IN FRAME default-frame.
    APPLY "TAB" TO f-cust-seq IN FRAME default-frame.

    DISABLE f-lot-num WITH FRAME default-frame.
    APPLY "ENTRY" TO f-do-num IN FRAME default-frame.
    PAUSE(1)  NO-MESSAGE.
/*     ENABLE f-lot-num WITH FRAME default-frame. */
    

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f-packages
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-packages C-Win
ON GO OF f-packages IN FRAME DEFAULT-FRAME /* Pkg # */
OR tab, RETURN OF f-packages IN FRAME default-frame
DO:
        ASSIGN f-packages.
        ASSIGN txtnumlabels = f-packages
               txtqty       = f-packages.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-DO-Lines
&Scoped-define SELF-NAME f-qty-packages
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-qty-packages C-Win
ON LEAVE OF f-qty-packages IN FRAME FRAME-DO-Lines /* Qty */
DO:
  ASSIGN f-qty-packages.
  ASSIGN f-packages = f-qty-packages
         txtnumlabels = f-qty-packages.

  DISPLAY f-packages txtnumlabels WITH FRAME DEFAULT-FRAME.     /* BYR-1089 - was {&FRAME-NAME}. */
    
  RUN save-do-line.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f-weight
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f-weight C-Win
ON LEAVE OF f-weight IN FRAME FRAME-DO-Lines /* Line Weight */
DO:
  ASSIGN f-weight.
  RUN save-do-line.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME RADIO-SET-plant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RADIO-SET-plant C-Win
ON VALUE-CHANGED OF RADIO-SET-plant IN FRAME DEFAULT-FRAME
DO:
  ASSIGN radio-set-plant.
  IF radio-set-plant = "2" THEN 
  DO:
      DISABLE f-lot-num WITH FRAME default-frame.
      APPLY "ENTRY" TO f-do-num IN FRAME default-frame.
  END.
  ELSE 
  DO:
      ENABLE f-lot-num WITH FRAME default-frame.
      APPLY "ENTRY" TO f-lot-num IN FRAME default-frame.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RADIO-SET-toolbar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RADIO-SET-toolbar C-Win
ON VALUE-CHANGED OF RADIO-SET-toolbar IN FRAME DEFAULT-FRAME
DO:
  ASSIGN radio-set-toolbar.
  
  IF radio-set-toolbar = "T" THEN
  DO:
      FRAME FRAME-consignee:HIDDEN           = FALSE.
      FRAME FRAME-consignor:HIDDEN           = TRUE.
      FRAME FRAME-invoicee:HIDDEN            = TRUE. PAUSE(0).
      FRAME FRAME-do-lines:HIDDEN            = TRUE. PAUSE(0).
      VIEW FRAME frame-consignee. PAUSE(0).
/*       FRAME FRAME-consignor:HIDDEN           = TRUE. PAUSE(0). */
/*       FRAME FRAME-invoicee:HIDDEN            = TRUE. PAUSE(0). */
/*       FRAME FRAME-do-lines:HIDDEN            = TRUE. PAUSE(0). */
/*                                                                */
      ENABLE ALL WITH FRAME frame-consignee.
      DISPLAY f-consignee-contact f-consignee-phone f-consignee-name 
          f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
          f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
          f-consignee-county f-consignee-country f-consignee-fax 
      WITH FRAME FRAME-consignee IN WINDOW C-Win.

      APPLY "ENTRY" TO f-consignee-name IN  FRAME frame-consignee. 
                           
  END.
  ELSE IF radio-set-toolbar = "F" THEN
  DO:
      FRAME FRAME-consignee:HIDDEN           = TRUE. PAUSE(0).
      FRAME FRAME-invoicee:HIDDEN            = TRUE. PAUSE(0).
      FRAME FRAME-do-lines:HIDDEN            = TRUE. PAUSE(0).
      VIEW FRAME frame-consignor. PAUSE(0).
      ENABLE ALL WITH FRAME frame-consignor.
      DISPLAY f-consignor-whse f-whse-name f-consignor-contact f-consignor-phone 
          f-consignor-name f-consignor-addr-1 f-consignor-addr-2 
          f-consignor-addr-3 f-consignor-addr-4 f-consignor-city 
          f-consignor-state f-consignor-zip f-consignor-county 
          f-consignor-country f-consignor-fax f-consignor-contact-id 
      WITH FRAME FRAME-Consignor IN WINDOW C-Win.


      APPLY "ENTRY" TO f-consignor-name IN FRAME frame-consignor.

  END.
  ELSE IF radio-set-toolbar = "I" THEN
  DO:
      FRAME FRAME-consignee:HIDDEN           = TRUE.  PAUSE(0).
      FRAME FRAME-consignor:HIDDEN           = TRUE.  PAUSE(0).
      FRAME FRAME-invoicee:HIDDEN            = FALSE. PAUSE(0).
      FRAME FRAME-do-lines:HIDDEN            = TRUE.  PAUSE(0).
      VIEW FRAME frame-invoicee. PAUSE(0).
      ENABLE ALL WITH FRAME frame-invoicee.
      APPLY "ENTRY" TO f-invoicee-name IN FRAME frame-invoicee.

  END.
  ELSE IF radio-set-toolbar = "L" THEN
  DO:
      

      FRAME FRAME-consignee:HIDDEN           = TRUE. PAUSE(0).
      FRAME FRAME-consignor:HIDDEN           = TRUE. PAUSE(0).
      FRAME FRAME-invoicee:HIDDEN            = TRUE. PAUSE(0).
      FRAME FRAME-do-lines:HIDDEN            = FALSE. PAUSE(0).


      VIEW FRAME frame-do-lines. PAUSE(0).
      
      ENABLE ALL WITH FRAME frame-do-lines.
      APPLY "ENTRY" TO f-do-line IN FRAME frame-do-lines.
        
      FIND FIRST tt-do-lines WHERE tt-do-lines.do-num = f-do-num
            NO-ERROR.
      {&OPEN-QUERY-browse-do-lines}
            
      
        
      APPLY "VALUE-CHANGED" TO BROWSE browse-do-lines.
  END.
  


END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   vPrinters = SESSION:GET-PRINTERS( ).
    
      DO vPrinterCount = 1 TO NUM-ENTRIES(vPrinters, ","):
    
          IF ENTRY(vPrinterCount,vPrinters,",") MATCHES "*z140*" OR  
             ENTRY(vPrinterCount,vPrinters,",") MATCHES "*2844z*" THEN DO:
          
              vZebraPrinters = vZebraPrinters + ENTRY(vPrinterCount,vPrinters,",") + ",".
    
          END.
    
      
      END.
    
      cmbPrinter:LIST-ITEMS IN FRAME DEFAULT-frame = vZebraPrinters.     
    /*    
    vPrinters = SESSION:GET-PRINTERS().
      IF TODAY < 06/23/10 THEN
            MESSAGE vprinters
            VIEW-AS ALERT-BOX.
      DO vPrinterCount = 1 TO NUM-ENTRIES(vPrinters, ","):
    
          IF ENTRY(vPrinterCount,vPrinters,",") MATCHES "*z140*" OR  
             ENTRY(vPrinterCount,vPrinters,",") MATCHES "*2844z*" THEN DO:
          
              vZebraPrinters = vZebraPrinters + ENTRY(vPrinterCount,vPrinters,",") + ",".
    
          END.
    
      
      END.
    
      cmbPrinter:LIST-ITEMS IN FRAME DEFAULT-frame = vZebraPrinters.
*/
     
      
      IF SEARCH("c:\temp\defaultlabelprinter.txt") <> ? THEN 
      do:
           
          
          INPUT FROM c:\temp\defaultLabelPrinter.txt.
          IMPORT vDefaultPrinter.
          INPUT CLOSE.
         cmbPrinter:SCREEN-VALUE = vDefaultPrinter.
          
      END.        
    
    FOR EACH symix.shipcode NO-LOCK.
             
            
            vList = vList + "," + SUBSTRING(trim(shipcode.DESCRIPTION),1,25).
        
    END.

vList = TRIM(vList,",").

cb-carrier:LIST-ITEMS = vList.

cb-carrier:SCREEN-VALUE = cb-carrier:ENTRY(1).


APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME default-frame.


    ASSIGN f-pickup-date = TODAY.
    
  RUN enable_UI.
  APPLY "ENTRY" TO f-do-num IN FRAME {&FRAME-NAME}.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Assign-Default-Frame C-Win 
PROCEDURE Assign-Default-Frame :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME default-frame.
ASSIGN    f-do-num cb-stat f-cust-num f-cust-seq f-do-value f-do-hdr-date 
          f-cust-name f-curr-code f-shipcode f-Carrier-num f-carrier-contact 
          cb-carrier
          f-veh-num f-pro-number f-weight f-packages f-pickup-date 
          RADIO-SET-toolbar.
END.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Assign-do-hdr-in C-Win 
PROCEDURE Assign-do-hdr-in :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FIND FIRST symix.shipcode NO-LOCK WHERE shipcode.ship-code = do-hdr.carrier-num
    NO-ERROR.
IF AVAILABLE shipcode THEN
DO:

      
               cb-carrier = SUBSTRING(shipcode.DESCRIPTION,1,25).

    
    cb-carrier     = IF AVAILABLE shipcode THEN caps(shipcode.DESCRIPTION) ELSE "".
    f-carrier-num  = IF AVAILABLE shipcode THEN CAPS(shipcode.ship-code)   ELSE "".
    DISPLAY f-cust-num f-cust-name cb-carrier
        f-carrier-num WITH FRAME default-frame.

    APPLY "VALUE-CHANGED" TO cb-carrier IN FRAME default-frame.
END.
ASSIGN
/* f-asn-extracted               =   do-hdr.asn-extracted */
/* f-asn-printed                 =   do-hdr.asn-printed   */
/* f-bol-printed                 =   do-hdr.bol-printed   */
/* f-carrier                     =   do-hdr.carrier */
/* f-carrier-contact             =   do-hdr.carrier-contact  */
f-do-value                    =   do-hdr.do-value
/* f-carrier-num                 =   do-hdr.carrier-num  */
/* f-cod-amt                     =   do-hdr.cod-amt             */
/* f-col-fee                     =   do-hdr.col-fee             */
/* f-col-freight-charges         =   do-hdr.col-freight-charges */
/* f-col-misc-charges            =   do-hdr.col-misc-charges    */
f-consignee-addr-1            =   do-hdr.consignee-addr[01]    
f-consignee-addr-2            =   do-hdr.consignee-addr[02] 
f-consignee-addr-3            =   do-hdr.consignee-addr[03] 
f-consignee-addr-4            =   do-hdr.consignee-addr[04] 
f-consignee-city              =   do-hdr.consignee-city                   
f-consignee-contact           =   do-hdr.consignee-contact                
f-consignee-country           =   IF do-hdr.consignee-country <> ""
    THEN do-hdr.consignee-country ELSE "USA"
f-consignee-county            =   do-hdr.consignee-county                 
f-consignee-fax               =   do-hdr.consignee-fax                    
f-consignee-name              =   do-hdr.consignee-name                   
f-consignee-phone             =   do-hdr.consignee-phone                  
f-consignee-state             =   do-hdr.consignee-state                  
f-consignee-zip               =   do-hdr.consignee-zip                    
f-consignor-addr-1            =   do-hdr.consignor-addr[01]    
f-consignor-addr-2            =   do-hdr.consignor-addr[02]               
f-consignor-addr-3            =   do-hdr.consignor-addr[03]               
f-consignor-addr-4            =   do-hdr.consignor-addr[04]               
f-consignor-city              =   do-hdr.consignor-city                   
f-consignor-contact           =   do-hdr.consignor-contact                
f-consignor-contact-id        =   do-hdr.consignor-contact-id             
f-consignor-country           =   do-hdr.consignor-country                
f-consignor-county            =   do-hdr.consignor-county                 
f-consignor-fax               =   do-hdr.consignor-fax                    
f-consignor-name              =   do-hdr.consignor-name                   
f-consignor-phone             =   do-hdr.consignor-phone                  
f-consignor-state             =   do-hdr.consignor-state                  
f-consignor-whse              =   do-hdr.consignor-whse                   
f-consignor-zip               =   do-hdr.consignor-zip   
f-po-num                      =   do-hdr.special-inst[01] + do-hdr.special-inst[02]
/* f-container                   =   do-hdr.container  */
f-curr-code                   =   IF do-hdr.curr-code <> "" 
        THEN do-hdr.curr-code  ELSE "USD"                      
f-cust-num                    =   do-hdr.cust-num                         
f-cust-seq                    =   do-hdr.cust-seq                         
f-do-hdr-date                 =   do-hdr.do-hdr-date                      
cs-do-invoice                 =   IF do-hdr.do-invoice = "S" THEN "Single" ELSE "Single by PO"                       
f-do-num                      =   do-hdr.do-num                           
f-do-value                    =   do-hdr.do-value                         
cs-inv-freq                   =   IF do-hdr.inv-freq = "W" THEN "Weekly" ELSE ""                         
f-invoice-printed             =   do-hdr.invoice-printed                  
f-invoicee-addr-1             =   do-hdr.invoicee-addr[01]
f-invoicee-addr-2             =   do-hdr.invoicee-addr[02]
f-invoicee-addr-3             =   do-hdr.invoicee-addr[03]
f-invoicee-addr-4             =   do-hdr.invoicee-addr[04]
f-invoicee-city               =   do-hdr.invoicee-city
f-invoicee-contact            =   do-hdr.invoicee-contact
f-invoicee-country            =   do-hdr.invoicee-country
f-invoicee-county             =   do-hdr.invoicee-county
f-invoicee-fax                =   do-hdr.invoicee-fax
f-invoicee-name               =   do-hdr.invoicee-name
f-invoicee-phone              =   do-hdr.invoicee-phone
f-invoicee-state              =   do-hdr.invoicee-state
f-invoicee-zip                =   do-hdr.invoicee-zip
/* f-key                         =   do-hdr.key  */
/* f-pack-slip-printed           =   do-hdr.pack-slip-printed  */
f-pickup-date                 =   do-hdr.pickup-date                      
/* f-ppd-fee                     =   do-hdr.ppd-fee              */
/* f-ppd-freight-charges         =   do-hdr.ppd-freight-charges  */
/* f-ppd-misc-charges            =   do-hdr.ppd-misc-charges     */
f-pro-number                  =   do-hdr.pro-number  
/* f-proforma-printed            =   do-hdr.proforma-printed  */
f-qty-packages                =   do-hdr.qty-packages                     
/* f-route                       =   do-hdr.route         */
/* f-shipped                     =   do-hdr.shipped       */
/* f-shipped-date                =   do-hdr.shipped-date  */
/* f-shipped-time                =   do-hdr.shipped-time  */
/* f-special-inst                =   do-hdr.special-inst  */.


      cb-stat        = IF do-hdr.stat BEGINS "I" THEN "In Process"
                        ELSE "Approved".

ASSIGN 
f-veh-num                     =   do-hdr.veh-num                          
f-weight                      =   do-hdr.weight.                           
/* f-weight-u-m                  =   do-hdr.weight-u-m.  */

FIND FIRST symix.whse NO-LOCK WHERE whse.whse = f-consignor-whse
    NO-ERROR.

  f-whse-name = IF AVAILABLE whse THEN whse.NAME
      ELSE "".
  DISPLAY f-whse-name f-consignor-whse
      WITH FRAME frame-consignor.

  EMPTY TEMP-TABLE tt-do-lines.
  RUN create-temp-table.
  
  {&OPEN-QUERY-browse-do-lines}
          APPLY "VALUE-CHANGED" TO BROWSE browse-do-lines.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Assign-DO-Hdr-out C-Win 
PROCEDURE Assign-DO-Hdr-out :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
     
    
     ASSIGN
/*      do-hdr.asn-extracted                         = f-asn-extracted */
/*      do-hdr.asn-printed                           = f-asn-printed   */
/*      do-hdr.bol-printed                           = f-bol-printed   */
     do-hdr.carrier-num                           = f-carrier-num
     do-hdr.carrier-contact                       = f-carrier-contact        
     do-hdr.carrier                               = cb-carrier  
     do-hdr.do-value                              = f-do-value
     do-hdr.special-inst[01]                      = substring(f-po-num,1,40) 
     do-hdr.special-inst[02]                      = SUBSTRING(f-po-num,41,40)       
/*      do-hdr.cod-amt                               = f-cod-amt */
/*      do-hdr.col-fee                               = f-col-fee */
/*      do-hdr.col-freight-charges                   = f-col-freight-charges */
/*      do-hdr.col-misc-charges                      = f-col-misc-charges    */
     do-hdr.consignee-addr[01]                    = f-consignee-addr-1  
     do-hdr.consignee-addr[02]                    = f-consignee-addr-2  
     do-hdr.consignee-addr[03]                    = f-consignee-addr-3  
     do-hdr.consignee-addr[04]                    = f-consignee-addr-4  
     do-hdr.consignee-city                        = f-consignee-city         
     do-hdr.consignee-contact                     = f-consignee-contact      
     do-hdr.consignee-country                     = f-consignee-country      
     do-hdr.consignee-county                      = f-consignee-county       
     do-hdr.consignee-fax                         = f-consignee-fax          
     do-hdr.consignee-name                        = f-consignee-name         
     do-hdr.consignee-phone                       = f-consignee-phone        
     do-hdr.consignee-state                       = f-consignee-state        
     do-hdr.consignee-zip                         = f-consignee-zip          
     do-hdr.consignor-addr[01]                    = f-consignor-addr-1   
     do-hdr.consignor-addr[02]                    = f-consignor-addr-2   
     do-hdr.consignor-addr[03]                    = f-consignor-addr-3   
     do-hdr.consignor-addr[04]                    = f-consignor-addr-4   
     do-hdr.consignor-city                        = f-consignor-city         
     do-hdr.consignor-contact                     = f-consignor-contact      
     do-hdr.consignor-contact-id                  = f-consignor-contact-id   
     do-hdr.consignor-country                     = IF f-consignor-country   <> ""
         THEN f-consignor-country ELSE "USA"
     do-hdr.consignor-county                      = f-consignor-county       
     do-hdr.consignor-fax                         = f-consignor-fax          
     do-hdr.consignor-name                        = f-consignor-name         
     do-hdr.consignor-phone                       = f-consignor-phone        
     do-hdr.consignor-state                       = f-consignor-state        
     do-hdr.consignor-whse                        = f-consignor-whse         
     do-hdr.consignor-zip                         = f-consignor-zip          
/*      do-hdr.container                             = f-container */
     do-hdr.curr-code                             = IF f-curr-code <> ""
             THEN f-curr-code ELSE "USD"
     do-hdr.cust-num                              = f-cust-num               
     do-hdr.cust-seq                              = f-cust-seq               
     do-hdr.do-hdr-date                           = f-do-hdr-date            
     do-hdr.do-invoice                            = IF cs-do-invoice = "Single" THEN "S" ELSE "P"             
     do-hdr.do-num                                = f-do-num                 
     do-hdr.do-value                              = f-do-value               
     do-hdr.inv-freq                              = IF cs-inv-freq  = "Weekly" THEN "W" ELSE ""             
     do-hdr.invoice-printed                       = f-invoice-printed        
     do-hdr.invoicee-addr[01]                     = f-invoicee-addr-1
     do-hdr.invoicee-addr[02]                     = f-invoicee-addr-2
     do-hdr.invoicee-addr[03]                     = f-invoicee-addr-3
     do-hdr.invoicee-addr[04]                     = f-invoicee-addr-4

     do-hdr.invoicee-city                         = f-invoicee-city
     do-hdr.invoicee-contact                      = f-invoicee-contact
     do-hdr.invoicee-country                      = f-invoicee-country
     do-hdr.invoicee-county                       = f-invoicee-county
     do-hdr.invoicee-fax                          = f-invoicee-fax
     do-hdr.invoicee-name                         = f-invoicee-name
     do-hdr.invoicee-phone                        = f-invoicee-phone
     do-hdr.invoicee-state                        = f-invoicee-state
     do-hdr.invoicee-zip                          = f-invoicee-zip
/*      do-hdr.key                                   = f-key               */
/*      do-hdr.pack-slip-printed                     = f-pack-slip-printed */
     do-hdr.pickup-date                           = f-pickup-date            
/*      do-hdr.ppd-fee                               = f-ppd-fee             */
/*      do-hdr.ppd-freight-charges                   = f-ppd-freight-charges */
/*      do-hdr.ppd-misc-charges                      = f-ppd-misc-charges    */
        do-hdr.pro-number                            = f-pro-number          
/*      do-hdr.proforma-printed                      = f-proforma-printed    */
     do-hdr.qty-packages                          = f-qty-packages           
/*      do-hdr.route                                 = f-route        */
/*      do-hdr.shipped                               = f-shipped      */
/*      do-hdr.shipped-date                          = f-shipped-date */
/*      do-hdr.shipped-time                          = f-shipped-time */
/*      do-hdr.special-inst                          = f-special-inst */
     do-hdr.stat                                  = IF cb-stat BEGINS "I" THEN "I" ELSE "A"                    
     do-hdr.veh-num                               = f-veh-num                
     do-hdr.weight                                = f-weight                 
/*      do-hdr.weight-u-m                            = f-weight-u-m. */.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Assign-Frames C-Win 
PROCEDURE Assign-Frames :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR i-last-key      LIKE symix.notes.KEY    NO-UNDO.
DO WITH FRAME default-frame.
  
  ASSIGN f-do-num cb-stat f-cust-num f-cust-seq f-do-value f-do-hdr-date 
          f-cust-name f-curr-code f-shipcode cb-carrier f-Carrier-num 
          f-do-value f-po-num
          f-carrier-contact f-veh-num f-pro-number f-weight f-packages 
          f-pickup-date RADIO-SET-toolbar.
  END.
  DO WITH FRAME frame-do-lines.
  
   ASSIGN f-do-line f-qty-packages cs-package-type f-weight f-nmfc  
          f-class-code f-description.
  END.
  DO WITH FRAME frame-consignee:
  
  ASSIGN  f-consignee-contact f-consignee-phone f-consignee-name 
          f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
          f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
          f-consignee-county f-consignee-country f-consignee-fax.
  END.
  DO WITH FRAME frame-invoicee:
  
  ASSIGN  f-invoice-printed CS-DO-Invoice cs-inv-freq f-invoicee-contact
          f-invoicee-phone f-invoicee-name f-invoicee-addr-1 f-invoicee-addr-2
          f-invoicee-addr-3 f-invoicee-addr-4 f-invoicee-city f-invoicee-state
          f-invoicee-zip f-invoicee-county f-invoicee-country f-invoicee-fax.
  END.
  DO WITH FRAME frame-consignor.
  
  ASSIGN  f-consignor-whse f-whse-name f-consignor-contact f-consignor-phone 
          f-consignor-name f-consignor-addr-1 f-consignor-addr-2 
          f-consignor-addr-3 f-consignor-addr-4 f-consignor-city 
          f-consignor-state f-consignor-zip f-consignor-county 
          f-consignor-country f-consignor-fax f-consignor-contact-id.

  END.  
  FIND FIRST do-hdr WHERE do-hdr.do-num = f-do-num
      NO-ERROR.
  IF NOT AVAILABLE do-hdr THEN
  DO:
      CREATE do-hdr.
      ASSIGN do-hdr.do-num              =   f-do-num.
      f-copies = 1.
      DISPLAY f-copies WITH FRAME {&FRAME-NAME}.
     

  END.
  IF NOT AVAILABLE symix.shipcode THEN FIND FIRST shipcode WHERE shipcode.DESCRIPTION
      = cb-carrier NO-ERROR.
  ASSIGN    do-hdr.carrier                  =   cb-carrier
            do-hdr.carrier-num              =   IF AVAILABLE shipcode THEN
                shipcode.ship-code ELSE ""
            do-hdr.carrier-contact          =   f-carrier-contact
            do-hdr.special-inst[01]         =   substring(f-po-num,1,40) 
            do-hdr.special-inst[02]         =   substring(f-po-num,41,40)            
                        
            do-hdr.cod-amt                  =   do-hdr.cod-amt
            do-hdr.col-fee                  =   do-hdr.col-fee
            do-hdr.col-freight-charges      =   do-hdr.col-freight-charges
            do-hdr.col-misc-charges         =   do-hdr.col-misc-charges
            do-hdr.consignee-name           =   f-consignee-name
            do-hdr.consignee-addr[01]       =   f-consignee-addr-1
            do-hdr.consignee-addr[02]       =   f-consignee-addr-2
            do-hdr.consignee-addr[03]       =   f-consignee-addr-3
            do-hdr.consignee-addr[04]       =   f-consignee-addr-4
            do-hdr.consignee-city           =   f-consignee-city  
            do-hdr.consignee-state          =   f-consignee-state
            do-hdr.consignee-zip            =   f-consignee-zip
            do-hdr.consignee-country        =   IF f-consignee-country <> ""
                    THEN f-consignee-country ELSE "USA"
            do-hdr.consignee-phone          =   f-consignee-phone 
            do-hdr.consignee-fax            =   f-consignee-fax 
            do-hdr.consignor-name           =   f-consignor-name
            do-hdr.consignor-addr[01]       =   f-consignor-addr-1
            do-hdr.consignor-addr[02]       =   f-consignor-addr-2
            do-hdr.consignor-addr[03]       =   f-consignor-addr-3
            do-hdr.consignor-addr[04]       =   f-consignor-addr-4
            do-hdr.consignor-city           =   f-consignor-city  
            do-hdr.consignor-state          =   f-consignor-state
            do-hdr.consignor-zip            =   f-consignor-zip
            do-hdr.consignor-country        =   f-consignor-country
            do-hdr.consignor-phone          =   f-consignor-phone 
            do-hdr.consignor-fax            =   f-consignor-fax.
        ASSIGN
            do-hdr.cust-num                 =   f-cust-num
            do-hdr.container                =   do-hdr.container
            do-hdr.curr-code                =   IF f-curr-code <> "" THEN
                f-curr-code ELSE "USD"
            do-hdr.cust-seq                 =   f-cust-seq
            do-hdr.do-hdr-date              =   f-do-hdr-date.
            
        
        ASSIGN    
            do-hdr.invoice-printed          =   f-invoice-printed 
            do-hdr.do-value                 =   f-do-value
            do-hdr.do-invoice               =   IF CS-DO-Invoice = "Single" THEN "S" ELSE "P"
            do-hdr.inv-freq                 =   IF cs-inv-freq = "weekly" THEN "W" ELSE ""
            do-hdr.invoicee-contact         =   f-invoicee-contact
            do-hdr.invoicee-phone           =   f-invoicee-phone
            do-hdr.invoicee-name            =   f-invoicee-name
            do-hdr.invoicee-addr[01]        =   f-invoicee-addr-1
            do-hdr.invoicee-addr[02]        =   f-invoicee-addr-2
            do-hdr.invoicee-addr[03]        =   f-invoicee-addr-3
            do-hdr.invoicee-addr[04]        =   f-invoicee-addr-4
            do-hdr.invoicee-city            =   f-invoicee-city
            do-hdr.invoicee-state           =   f-invoicee-state
            do-hdr.invoicee-zip             =   f-invoicee-zip
            do-hdr.invoicee-county          =   f-invoicee-county
            do-hdr.invoicee-country         =   f-invoicee-country
            do-hdr.invoicee-fax             =   f-invoicee-fax
            do-hdr.pack-slip-printed        =   do-hdr.pack-slip-printed
            do-hdr.pickup-date              =   f-pickup-date
            do-hdr.ppd-fee                  =   do-hdr.ppd-fee
            do-hdr.ppd-freight-charges      =   do-hdr.ppd-freight-charges
            do-hdr.ppd-misc-charges         =   do-hdr.ppd-misc-charges
            do-hdr.pro-number               =   f-pro-number
            do-hdr.proforma-printed         =   do-hdr.proforma-printed
            do-hdr.qty-packages             =   f-packages
            do-hdr.route                    =   do-hdr.route
            do-hdr.shipped                  =   do-hdr.shipped
            do-hdr.shipped-date             =   do-hdr.shipped-date
            do-hdr.shipped-time             =   do-hdr.shipped-time
            do-hdr.special-inst[01]         =   do-hdr.special-inst[01]
            do-hdr.special-inst[02]         =   do-hdr.special-inst[02]
            do-hdr.stat                     =   IF cb-stat BEGINS "I" THEN "I" ELSE "A"
            do-hdr.veh-num                  =   f-veh-num
            do-hdr.weight                   =   f-weight
            DO-hdr.weight-u-m               =   do-hdr.weight-u-m.

        FOR EACH tt-do-lines.
            FIND FIRST symix.do-line WHERE do-line.do-num  =   tt-do-lines.do-num
                AND do-line.do-line                        =   tt-do-lines.do-line
                NO-ERROR.
            IF NOT AVAILABLE do-line THEN
            DO:
                CREATE do-line.
                ASSIGN do-line.do-num       =   tt-do-lines.do-num
                       do-line.do-line      =   tt-do-lines.do-line.
                 FIND LAST symix.notes NO-LOCK NO-ERROR.
                  i-last-key        =   notes.KEY + 1.
                  ASSIGN do-line.KEY =   i-last-key.
            END.
            
            ASSIGN do-line.DESCRIPTION      =   tt-do-lines.DESCRIPTION
                   do-line.hazard           =   tt-do-lines.hazard
                   do-line.weight           =   tt-do-lines.weight
                   do-line.marks-except     =   do-line.marks-except
                   do-line.nmfc             =   tt-do-lines.nmfc
                   do-line.package-type     =   IF tt-do-lines.package-type = "Boxes"
                       THEN "B" 
                       ELSE IF tt-do-lines.package-type = "Bundles" THEN "U" 
                       ELSE IF tt-do-lines.package-type = "Cartons" THEN "C" 
                       ELSE "S"
                   do-line.qty-packages     =   tt-do-lines.qty-packages
                   do-line.rate-code        =   tt-do-lines.class-code.
            
            FIND FIRST notes WHERE notes.KEY = do-line.KEY
                AND notes.seq = 1 NO-ERROR.
            IF NOT AVAILABLE notes THEN
            DO:
                CREATE notes.
                ASSIGN notes.KEY = do-line.KEY
                       notes.seq = 1.
            END.
            ASSIGN notes.txt    =   f-nmfc.
            FIND FIRST notes WHERE notes.KEY = do-line.KEY
                AND notes.seq = 2 NO-ERROR.
            IF NOT AVAILABLE notes THEN
            DO:
                CREATE notes.
                ASSIGN notes.KEY = do-line.KEY
                       notes.seq = 2.
            END.
            ASSIGN notes.txt    =   f-class-code.
        END.




END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CLEAR-DO-HDR C-Win 
PROCEDURE CLEAR-DO-HDR :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

ASSIGN
/* f-asn-extracted               =   do-hdr.asn-extracted */
/* f-asn-printed                 =   do-hdr.asn-printed   */
/* f-bol-printed                 =   do-hdr.bol-printed   */
f-carrier-num                 =   "" 
cb-carrier:SCREEN-VALUE IN FRAME default-frame = cb-carrier:ENTRY(1).
ASSIGN
f-shipcode                    =   ""
f-cust-name                   =   ""
f-carrier-contact             =   ""  
cb-carrier                    =   ""
f-po-num                      =   ""
f-lot-num                     =   ""        .

ASSIGN
/* f-cod-amt                     =   do-hdr.cod-amt             */
/* f-col-fee                     =   do-hdr.col-fee             */
/* f-col-freight-charges         =   do-hdr.col-freight-charges */
/* f-col-misc-charges            =   do-hdr.col-misc-charges    */
f-consignee-addr-1            =  ""    
f-consignee-addr-2            =  "" 
f-consignee-addr-3            =  "" 
f-consignee-addr-4            =   ""
f-consignee-city              =   ""                   
f-consignee-contact           =   ""                
f-consignee-country           =   ""                
f-consignee-county            =   ""                 
f-consignee-fax               =   ""                    
f-consignee-name              =   ""
f-consignee-phone             =   ""               
f-consignee-state             =   ""            
f-consignee-zip               =   ""                 
f-consignor-addr-1            =   ""   
f-consignor-addr-2            =   ""              
f-consignor-addr-3            =   ""               
f-consignor-addr-4            =   ""              
f-consignor-city              =   "".
ASSIGN 
f-consignor-contact           =   ""           
f-consignor-contact-id        =   ""          
f-consignor-country           = ""     
f-consignor-county            =   ""                 
f-consignor-fax               =   ""                    
f-consignor-name              =   ""                  
f-consignor-phone             =   ""                  
f-consignor-state             =   ""                  
f-consignor-whse              =   ""                 
f-consignor-zip               =   "".
ASSIGN 
/* f-container                   =   do-hdr.container  */
f-curr-code                   =   "USD"                       
f-cust-num                    =   ""                      
f-cust-seq                    =   0                       
f-do-hdr-date                 =   ?                     
cs-do-invoice                 =   "Single by PO"                       
/* f-do-num                      =   do-hdr.do-num */
f-do-value                    =   0                         
cs-inv-freq                    =  "Weekly"        
f-do-hdr-date                 =   TODAY
cb-stat                       =   "In Process" 
f-invoice-printed             =   FALSE.
ASSIGN
f-invoicee-addr-1             =   ""   
f-invoicee-addr-2             =   ""   
f-invoicee-addr-3             =   ""           
f-invoicee-addr-4             =   ""   
f-invoicee-city               =   ""                    
f-invoicee-contact            =   ""                
f-invoicee-country            =   ""                 
f-invoicee-county             =   ""                  
f-invoicee-fax                =   ""                     
f-invoicee-name               =   ""                  
f-invoicee-phone              =   ""                 
f-invoicee-state              =   ""                   
f-invoicee-zip                =   ""                    
/* f-key                         =   do-hdr.key                              */
/* f-pack-slip-printed           =   do-hdr.pack-slip-printed                */
f-pickup-date                 =   ?                      
/* f-ppd-fee                     =   do-hdr.ppd-fee              */
/* f-ppd-freight-charges         =   do-hdr.ppd-freight-charges  */
/* f-ppd-misc-charges            =   do-hdr.ppd-misc-charges     */
/* f-pro-number                  =   do-hdr.pro-number  */
/* f-proforma-printed            =   do-hdr.proforma-printed  */
f-qty-packages                =   0                    
/* f-route                       =   do-hdr.route         */
/* f-shipped                     =   do-hdr.shipped       */
/* f-shipped-date                =   do-hdr.shipped-date  */
/* f-shipped-time                =   do-hdr.shipped-time  */
/* f-special-inst                =   do-hdr.special-inst  */
cb-stat                        =   ""                             
f-veh-num                     =   ""                       
f-weight                      =   0.                           
/* f-weight-u-m                  =   do-hdr.weight-u-m.  */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Clear-Screen C-Win 
PROCEDURE Clear-Screen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ENABLE f-do-num WITH FRAME {&FRAME-NAME}.             
  CLEAR FRAME default-frame ALL NO-PAUSE.
  CLEAR FRAME frame-do-lines ALL NO-PAUSE.
  CLEAR FRAME frame-consignee ALL NO-PAUSE.
  CLEAR FRAME frame-consignor ALL NO-PAUSE.
  CLEAR FRAME frame-invoicee  ALL NO-PAUSE.
  APPLY "ENTRY" TO f-consignee-name IN FRAME frame-consignee.
  
  EMPTY TEMP-TABLE tt-do-lines.
  ASSIGN
  f-do-line = 1
  f-description = "WIRING COMPONENTS"
  radio-set-toolbar = "T".

  RUN clear-do-hdr.
  
  f-do-num = "".
  RUN ENABLE_ui.
  
  APPLY "ENTRY" TO f-do-num IN FRAME {&FRAME-NAME}.  
  APPLY "VALUE-CHANGED" TO radio-set-plant   IN FRAME default-frame.
  APPLY "VALUE-CHANGED" TO radio-set-toolbar IN FRAME default-frame.
  APPLY "ENTRY" TO f-do-num IN FRAME default-frame.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Create-Temp-Table C-Win 
PROCEDURE Create-Temp-Table :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FOR EACH symix.do-line WHERE do-line.do-num   =   do-hdr.do-num.
            FIND FIRST tt-do-lines WHERE tt-do-lines.do-num  =   do-line.do-num
                AND    tt-do-lines.do-line                   =   do-line.do-line
                NO-ERROR.
            IF NOT AVAILABLE tt-do-lines THEN
            DO:
                CREATE tt-do-lines.
                ASSIGN tt-do-lines.do-num       =   do-line.do-num
                       tt-do-lines.do-line      =   do-line.do-line.
            END.
            ASSIGN tt-do-lines.DESCRIPTION      =   do-line.DESCRIPTION
                   tt-do-lines.hazard           =   do-line.hazard
/*                    tt-do-lines.marks-except     =   do-line.marks-except */
                   tt-do-lines.nmfc             =   IF do-line.nmfc <> ""
                            THEN do-line.nmfc ELSE "061130-08"
                   tt-do-lines.weight           =   do-line.weight
                   tt-do-lines.package-type     =   IF do-line.package-type = "B"
                        THEN "Boxes" 
                         ELSE IF do-line.package-type = "C" THEN "Cartons" 
                         ELSE IF do-line.package-type = "U" THEN "Bundles" 
                         ELSE "Skids"
                   tt-do-lines.qty-packages     =   do-line.qty-packages
                   tt-do-lines.class-code       =   IF do-line.rate-code <>
                             "" THEN do-line.rate-code ELSE "77.5".
                   FIND FIRST symix.notes NO-LOCK WHERE notes.KEY = do-line.KEY
                       AND notes.seq = 1 NO-ERROR.
                   IF AVAILABLE notes THEN ASSIGN tt-do-lines.nmfc = 
                       substring(notes.txt,1,10).
                   IF AVAILABLE notes AND 
                       notes.txt = "" THEN notes.txt = "061130-08".
                   FIND FIRST notes NO-LOCK WHERE notes.KEY = do-line.KEY
                       AND notes.seq = 2 NO-ERROR.
                   IF AVAILABLE notes THEN ASSIGN tt-do-lines.class-code = 
                       substring(notes.txt,1,10).
                   IF AVAILABLE notes AND 
                       notes.txt = "" THEN notes.txt = "77.5".

        END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY CmbPrinter tglDefaultPrinter TxtNumLabels t-print-label cb-carrier 
          f-copies RADIO-SET-plant f-do-num cb-stat f-cust-num f-cust-seq 
          f-do-value f-do-hdr-date f-shipcode f-Carrier-num f-carrier-contact 
          f-veh-num f-pro-number f-weight f-packages f-pickup-date 
          RADIO-SET-toolbar f-po-num f-lot-num f-cust-name f-curr-code 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE CmbPrinter tglDefaultPrinter TxtNumLabels t-print-label 
         btn_clear_billto btn-clear cb-carrier f-copies RADIO-SET-plant 
         f-do-num cb-stat f-cust-num f-cust-seq f-do-value f-do-hdr-date 
         f-shipcode btn-do-num f-carrier-contact f-veh-num btn_print 
         f-pro-number f-packages f-pickup-date RADIO-SET-toolbar f-po-num 
         btn-customer btn_do-save btn-cust-seq btn_cancel f-cust-name 
         f-curr-code Btn_Done 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  DISPLAY f-do-line f-qty-packages cs-package-type f-weight f-nmfc f-class-code 
          f-description 
      WITH FRAME FRAME-DO-Lines IN WINDOW C-Win.
  ENABLE f-do-line f-qty-packages cs-package-type f-weight f-nmfc f-class-code 
         f-description Btn_DoSeq btn_line-done BROWSE-DO-Lines 
      WITH FRAME FRAME-DO-Lines IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-DO-Lines}
  DISPLAY f-invoice-printed CS-DO-Invoice cs-inv-freq f-invoicee-contact 
          f-invoicee-phone f-invoicee-name f-invoicee-addr-1 f-invoicee-addr-2 
          f-invoicee-addr-3 f-invoicee-addr-4 f-invoicee-city f-invoicee-state 
          f-invoicee-zip f-invoicee-county f-invoicee-country f-invoicee-fax 
      WITH FRAME FRAME-Invoicee IN WINDOW C-Win.
  ENABLE CS-DO-Invoice cs-inv-freq f-invoicee-contact f-invoicee-phone 
         f-invoicee-name f-invoicee-addr-1 f-invoicee-addr-2 f-invoicee-addr-3 
         f-invoicee-addr-4 f-invoicee-city f-invoicee-state f-invoicee-zip 
         f-invoicee-county f-invoicee-country f-invoicee-fax RECT-3 
      WITH FRAME FRAME-Invoicee IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-Invoicee}
  DISPLAY f-consignee-contact f-consignee-phone f-consignee-name 
          f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
          f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
          f-consignee-county f-consignee-country f-consignee-fax 
      WITH FRAME FRAME-consignee IN WINDOW C-Win.
  ENABLE f-consignee-contact f-consignee-phone f-consignee-name 
         f-consignee-addr-1 f-consignee-addr-2 f-consignee-addr-3 
         f-consignee-addr-4 f-consignee-city f-consignee-state f-consignee-zip 
         f-consignee-county f-consignee-country f-consignee-fax RECT-1 
      WITH FRAME FRAME-consignee IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-consignee}
  DISPLAY f-consignor-whse f-whse-name f-consignor-contact f-consignor-phone 
          f-consignor-name f-consignor-addr-1 f-consignor-addr-2 
          f-consignor-addr-3 f-consignor-addr-4 f-consignor-city 
          f-consignor-state f-consignor-zip f-consignor-county 
          f-consignor-country f-consignor-fax f-consignor-contact-id 
      WITH FRAME FRAME-Consignor IN WINDOW C-Win.
  ENABLE btn_whse f-consignor-whse f-whse-name f-consignor-contact 
         f-consignor-phone f-consignor-name f-consignor-addr-1 
         f-consignor-addr-2 f-consignor-addr-3 f-consignor-addr-4 
         f-consignor-city f-consignor-state f-consignor-zip f-consignor-county 
         f-consignor-country f-consignor-fax f-consignor-contact-id RECT-2 
      WITH FRAME FRAME-Consignor IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-Consignor}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE find-date C-Win 
PROCEDURE find-date :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT-OUTPUT PARAMETER pio-week-date     AS DATE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Process-Report C-Win 
PROCEDURE Process-Report :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Save-DO-Line C-Win 
PROCEDURE Save-DO-Line :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME frame-do-lines:

ASSIGN 
          f-do-line f-qty-packages cs-package-type f-weight f-nmfc 
          f-class-code f-description.   

  FIND FIRST tt-do-lines WHERE tt-do-lines.do-num = f-do-num 
      AND tt-do-lines.do-line = f-do-line 
      NO-ERROR.
  IF NOT AVAILABLE tt-do-lines THEN
  DO:
      CREATE tt-do-lines.
      ASSIGN tt-do-lines.do-num         =   f-do-num
             tt-do-lines.do-line        =   f-do-line.
  END.
  ASSIGN tt-do-lines.qty-packages       =   f-qty-packages
         tt-do-lines.package-type       =   cs-package-type
         tt-do-lines.DESCRIPTION        =   f-description
         
         tt-do-lines.class-code         =   caps(f-class-code)
         tt-do-lines.weight             =   f-weight
         tt-do-lines.nmfc               =   caps(f-nmfc).
    
    
  {&OPEN-QUERY-browse-do-lines}

  i-weight = 0.
  FOR EACH tt-do-lines.
      i-weight = i-weight + tt-do-lines.weight.
      f-weight = IF i-weight > 0 THEN i-weight ELSE f-weight.
      DISPLAY f-weight WITH FRAME default-frame.

  END.
  
      f-weight = IF i-weight > 0 THEN i-weight ELSE f-weight.
      DISPLAY f-weight WITH FRAME default-frame.
        
  RETURN.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Save-DO-Record C-Win 
PROCEDURE Save-DO-Record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE THIRD-PARTY C-Win 
PROCEDURE THIRD-PARTY :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
IF LOOKUP(f-carrier-num,"CHR,CELA,UNIS") > 0 THEN
DO: t-ship-type = "TP". /* byr-1004 */
     IF LOOKUP(f-carrier-num,"chr") > 0 THEN
    ASSIGN   
        f-invoicee-addr-1             =   "14800 CHARLSON"
        f-invoicee-addr-2             =   "SUITE 2100"
        f-invoicee-addr-3             =   ""
        f-invoicee-addr-4             =   ""
        f-invoicee-city               =   "EDEN PRAIRIE"
        f-invoicee-contact            =   "DMIR"
        f-invoicee-country            =   "USA"
        f-invoicee-county             =   ""
        f-invoicee-fax                =   ""
        f-invoicee-name               =   "CH ROBINSON"
        f-invoicee-phone              =   "1-800-733-2122-117"
        f-invoicee-state              =   "MN"
        f-invoicee-zip                =   "55347"
        t-ship-type                   =   "TP".  

    IF LOOKUP(f-carrier-num,"CELA") > 0 THEN
    ASSIGN   
        f-invoicee-addr-1             =   "1001 HURRICAN STREET"
        f-invoicee-addr-2             =   ""
        f-invoicee-addr-3             =   ""
        f-invoicee-addr-4             =   ""
        f-invoicee-city               =   "FRANKLIN"
        f-invoicee-contact            =   "DAN GIFFIN"
        f-invoicee-country            =   "USA"
        f-invoicee-county             =   ""
        f-invoicee-fax                =   ""
        f-invoicee-name               =   "CONSOLIDATED GROUP"
        f-invoicee-phone              =   "1-888-736-1934 x19"
        f-invoicee-state              =   "IN"
        f-invoicee-zip                =   "46131"
        t-ship-type                   =   "TP".     
        
    IF LOOKUP(f-carrier-num,"UNIS") > 0 THEN
    ASSIGN   
        f-invoicee-addr-1             =   "PO BOX 6047"
        f-invoicee-addr-2             =   ""
        f-invoicee-addr-3             =   ""
        f-invoicee-addr-4             =   ""
        f-invoicee-city               =   "KENNEWICK"
        f-invoicee-contact            =   ""
        f-invoicee-country            =   "USA"
        f-invoicee-county             =   ""
        f-invoicee-fax                =   ""
        f-invoicee-name               =   "UNISHIPPERS"
        f-invoicee-phone              =   "1-956-717-3494"
        f-invoicee-state              =   "WA"
        f-invoicee-zip                =   "99336"
        t-ship-type                   =   "TP".     

END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

