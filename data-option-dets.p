OUTPUT TO M:\per-michael.txt.
EXPORT DELIMITER "|"
    "Data Option"
    "Component"
    "Component Prod Code"
    "Component Usage"
    "Kit Description"
    .

FOR EACH symix.job NO-LOCK WHERE
    job.TYPE = "S" AND
    job.ITEM BEGINS "Data Option",
    EACH symix.jobmatl NO-LOCK WHERE
    jobmatl.job           =  job.job AND
    jobmatl.suffix        =  job.suffix AND
    (jobmatl.effect-date  <= TODAY OR jobmatl.effect-date  = ?) AND
    (jobmatl.obs-date     >  TODAY OR jobmatl.obs-date     = ?),
    EACH symix.jobroute OF jobmatl NO-LOCK WHERE
    (jobroute.effect-date <= TODAY OR jobroute.effect-date = ?) AND
    (jobroute.obs-date    >  TODAY OR jobroute.obs-date    = ?),
    FIRST symix.ux-item NO-LOCK WHERE
    ux-item.ITEM = jobmatl.ITEM,
    FIRST symix.ITEM NO-LOCK WHERE
    ITEM.ITEM = jobmatl.ITEM:

    EXPORT DELIMITER "|"
        job.ITEM
        ITEM.ITEM
        ITEM.product-code
        jobmatl.matl-qty-conv
        REPLACE(ux-item.uf-kit-description, CHR(10), CHR(32))
        .
END.

OUTPUT CLOSE.
