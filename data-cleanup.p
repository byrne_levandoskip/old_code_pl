

DEF BUFFER b-customer FOR symix.customer.
DEF BUFFER b-custaddr FOR symglbl.custaddr.

OUTPUT TO M:\All-Inbound-Customers-scrub.txt.
EXPORT DELIMITER "|"
    "Customer Num"
    "Customer Seq"
    "Customer Name"
    "Customer Type"
    "Created Date"
    "Last Inv Date"
    "Price Code"
    "Country"
    "State"
    "Terms Code"
    "Terms Description"
    "PayMethod"
    .

FOR EACH symix.customer NO-LOCK WHERE
    customer.cust-seq = 0 AND
    customer.cust-type = "INBD":

    FOR EACH b-customer NO-LOCK WHERE
        b-customer.cust-num = customer.cust-num,
        FIRST b-custaddr NO-LOCK OF b-customer,
        FIRST symix.terms NO-LOCK WHERE
        terms.terms-code = b-customer.terms-code:

        IF b-custaddr.country = "0" OR
            b-custaddr.country = "1" OR
            b-custaddr.country = "5" OR
            b-custaddr.country = "7" OR
            b-custaddr.country = "11" OR
            b-custaddr.country = "18" OR
            b-custaddr.country = "25" OR
            b-custaddr.country = "32" OR
            b-custaddr.country = "51" OR
            b-custaddr.country = "63" OR
            b-custaddr.country = "88" OR
            b-custaddr.country = "UNITED STATES" OR
            b-custaddr.country = "USA" THEN
            ASSIGN b-custaddr.country = "US".

        IF b-custaddr.country = "CANADA" THEN
            ASSIGN b-custaddr.country = "CA".

        IF b-custaddr.country = "CA" AND
            b-custaddr.state = "ALB" THEN
            ASSIGN b-custaddr.state = "AB".

        IF b-custaddr.country = "MEXICO" THEN
            ASSIGN b-custaddr.country = "MX".

        IF b-customer.cust-seq GE 1 AND
            b-customer.cust-type <> "" THEN
            ASSIGN b-customer.cust-type = "".


        EXPORT DELIMITER "|"
            b-customer.cust-num
            b-customer.cust-seq
            b-custaddr.NAME
            b-customer.cust-type
            b-customer.datefld
            b-customer.last-inv
            b-customer.pricecode
            b-custaddr.country
            b-custaddr.state
            b-customer.terms-code
            terms.DESCRIPTION
            b-customer.pay-type
            .
    END.
END.
OUTPUT CLOSE.