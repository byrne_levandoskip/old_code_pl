

OUTPUT TO M:\cr-update_11-26-18.txt.
EXPORT DELIMITER "|"
    "Customer"
    "Name"
    "Type"
    "State"
    "Country"
    "Old CR Rep"
    "New CR Rep"
    .

FOR EACH symix.customer NO-LOCK WHERE
    customer.cust-type = "INBD" AND
    customer.cust-seq = 0,
    FIRST symglbl.custaddr NO-LOCK OF symix.customer WHERE
    custaddr.state = "OH" OR
    custaddr.state = "WV" OR
    custaddr.state = "WVA" OR
    custaddr.state = "VA" OR
    custaddr.state = "FL",
    FIRST symix.ux-customer EXCLUSIVE-LOCK OF symix.customer WHERE
    ux-customer.uf-internal-email <> "wegmant@byrne.com":

    
    EXPORT DELIMITER "|"
        customer.cust-num
        custaddr.NAME
        customer.cust-type
        custaddr.state
        custaddr.country
        ux-customer.uf-internal-email
        "reedl@byrne.com"
        .
    ASSIGN
        ux-customer.uf-internal-email = "reedl@byrne.com"
        .
END.
OUTPUT CLOSE.




