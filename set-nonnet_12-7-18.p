



output to M:\setting-nonnet-flag_12-7-2018.csv.
export delimiter ","
    "Warehouse"
    "Location"
    "Loc Rank"
    "Item"
    "Description"
    "Old NonNet Flag"
    "New NonNet Flag"
    "Qty On Hand"
    "UM"
    "Unit Cost"
    .
for each symix.itemloc exclusive-lock where
    itemloc.loc = "CSM",
    first symix.item no-lock where
    item.item = itemloc.item:

    export delimiter ","
        itemloc.whse
        itemloc.loc
        itemloc.rank
        itemloc.item
        item.description
        itemloc.mrb-flag
        "Yes"
        itemloc.qty-on-hand
        item.u-m
        item.unit-cost
        .

/*     assign                     */
/*         itemloc.mrb-flag = yes */
/*         .                      */


end.
output close.

