
DEF TEMP-TABLE sa-items
    FIELD sa-item       LIKE symix.ITEM.ITEM
    .


DEF TEMP-TABLE fg-items
    FIELD fg-item       LIKE symix.ITEM.ITEM
    FIELD qty-in-bom    AS INT
    FIELD orig-item     LIKE symix.ITEM.ITEM
    FIELD product-code  LIKE symix.ITEM.product-code
    .


DEF TEMP-TABLE sales-data
    FIELD fg-item       LIKE symix.ITEM.ITEM
    FIELD orig-item     LIKE symix.ITEM.ITEM
    FIELD cust-num      LIKE symix.co.cust-num
    FIELD cust-item     LIKE symix.itemcust.cust-item
    FIELD sales-ytd     LIKE symix.coitem.qty-invoiced
    FIELD sales-17      LIKE symix.coitem.qty-invoiced
    FIELD sales-16      LIKE symix.coitem.qty-invoiced
    .

CREATE sa-items.
ASSIGN sa-items.sa-item = "CD04291-24-5.5-2".
CREATE sa-items.
ASSIGN sa-items.sa-item = "CD04291-6-18-6".
CREATE sa-items.
ASSIGN sa-items.sa-item = "MC04518-EQL".
CREATE sa-items.
ASSIGN sa-items.sa-item = "MC04519-EQL".
CREATE sa-items.
ASSIGN sa-items.sa-item = "MC04537-EQL".
CREATE sa-items.
ASSIGN sa-items.sa-item = "MC06589-EQL".
CREATE sa-items.
ASSIGN sa-items.sa-item = "MC07051".
CREATE sa-items.
ASSIGN sa-items.sa-item = "MC07052".
CREATE sa-items.
ASSIGN sa-items.sa-item = "MC06587-EQL".
CREATE sa-items.
ASSIGN sa-items.sa-item = "SA06146".
CREATE sa-items.
ASSIGN sa-items.sa-item = "SA06959-AV".
CREATE sa-items.
ASSIGN sa-items.sa-item = "SA06959-C".



FOR EACH sa-items NO-LOCK WHERE
    sa-items.sa-item <> "":

    RUN find-fg(INPUT sa-items.sa-item, INPUT 1, INPUT sa-items.sa-item).  
END.

/* RUN find-fg(INPUT "LYD0504000B", INPUT 1, INPUT "LYD0504000B").         */
/* RUN find-fg(INPUT "LYD0504000B-2FT", INPUT 1, INPUT "LYD0504000B-2FT"). */
/* RUN find-fg(INPUT "LYD0504000B-4FT", INPUT 1, INPUT "LYD0504000B-4FT"). */

OUTPUT TO M:\fg-items_sanity-check.csv.
FOR EACH fg-items NO-LOCK WHERE
    fg-items.fg-item <> "":
    EXPORT DELIMITER ","
        fg-items.
END.
OUTPUT CLOSE.

OUTPUT TO M:\sales-by-component.csv.
EXPORT DELIMITER ","
    "FG Item"
    "Component"
    "Product Code"
    "Customer"
    "Customer PO"
    "Customer Item"
    "Order"
    "Line"
    "Order Line Price"
    "Qty Ordered"
    "Qty Invoiced"
    "Due Date"
    "Lot"
    "Has Bad PO"
    "POs Found"
    .
FOR EACH symix.co NO-LOCK WHERE
    co.order-date GE 05/06/19 and
    co.order-date LE 05/12/19,
    EACH symix.coitem NO-LOCK OF symix.co,
    FIRST fg-items NO-LOCK WHERE
    fg-items.fg-item = coitem.ITEM:


    EXPORT DELIMITER ","
        fg-items.fg-item
        fg-items.orig-item
        fg-items.product-code
        co.cust-num
        co.cust-po
        coitem.cust-item
        coitem.co-num
        coitem.co-line
        coitem.price
        coitem.qty-ordered
        coitem.qty-invoiced
        coitem.due-date
        .
END.




PROCEDURE find-fg:
    DEF INPUT PARAMETER p-item  LIKE symix.ITEM.ITEM.
    DEF INPUT PARAMETER p-qty   like symix.jobmatl.matl-qty-conv.
    DEF INPUT PARAMETER orig-item  LIKE symix.ITEM.ITEM.

    OUTPUT TO M:\find-fg-log.csv APPEND.
    EXPORT DELIMITER ","
        p-item
        p-qty.
    OUTPUT CLOSE.

    FOR EACH symix.jobmatl NO-LOCK WHERE
        jobmatl.ITEM = p-item AND
        (jobmatl.effect-date <= TODAY OR jobmatl.effect-date  = ?) AND
        (jobmatl.obs-date    >  TODAY OR jobmatl.obs-date     = ?),
        EACH symix.jobroute OF symix.jobmatl NO-LOCK WHERE
        (jobroute.effect-date <= TODAY OR jobroute.effect-date = ?) AND
        (jobroute.obs-date    >  TODAY OR jobroute.obs-date    = ?),
        FIRST symix.job NO-LOCK OF symix.jobroute WHERE
        job.suffix = 000 and
        job.TYPE = "s",
        FIRST symix.ITEM NO-LOCK WHERE
        job.ITEM = ITEM.ITEM:

        IF ITEM.product-code BEGINS "FG" THEN DO:

            FIND FIRST fg-items NO-LOCK WHERE
                fg-items.fg-item = ITEM.ITEM NO-ERROR.
            IF ITEM.stat = "A" AND 
                NOT AVAIL fg-items THEN DO:
                CREATE fg-items.
                ASSIGN 
                    fg-items.fg-item = ITEM.ITEM
                    fg-items.product-code = ITEM.product-code
                    fg-items.qty-in-bom = (p-qty * jobmatl.matl-qty-conv)
                    fg-items.orig-item = orig-item
                    .
            END.
            ELSE IF AVAIL fg-items AND 
                ITEM.stat = "A" THEN DO:
                OUTPUT TO M:\dup-item.csv APPEND.
                EXPORT DELIMITER "," 
                    fg-items.fg-item
                    fg-items.qty-in-bom
                    p-item
                    p-qty.
                OUTPUT CLOSE.
            END.
        END.
        ELSE DO:
            RUN find-fg(INPUT ITEM.ITEM,
                        INPUT (p-qty * jobmatl.matl-qty-conv),
                        INPUT orig-item).
        END.
    END.
END PROCEDURE.



