

DEF VAR hExcel         AS COM-HANDLE NO-UNDO.
DEF VAR hWorkBook      AS COM-HANDLE NO-UNDO.
DEF VAR hWorkSheet     AS COM-HANDLE NO-UNDO.
DEF VAR hWindow        AS COM-HANDLE NO-UNDO.
DEF VAR v-row          AS INT INIT 1.
DEF VAR v-po           AS CHAR.
DEF VAR v-rel          AS CHAR.
DEF VAR v-cust-po      LIKE symix.co.cust-po.
DEF VAR v-line-count   AS INT.


DEF TEMP-TABLE found
    FIELD cust-po       LIKE symix.co.cust-po
    FIELD num-of-lines  AS INT.



CREATE "Excel.Application" hExcel.
ASSIGN
   hWorkBook      = hExcel:WorkBooks:OPEN("M:\FNDWRR.xlsx")
   hWorkSheet     = hexcel:Sheets:ITEM(1) 
   hWindow        = hExcel:ActiveWindow
   hexcel:VISIBLE = FALSE.  /* skip first row - headers */




/* DO v-row = 2 TO 2172: */
DO v-row = 2161 TO 2166:
    ASSIGN
        v-po    = TRIM(STRING(hWorkSheet:Cells(v-row,12):VALUE))
        v-rel   = TRIM(STRING(hWorkSheet:Cells(v-row,13):VALUE))
        v-po    = REPLACE(v-po, "?", "")
        v-rel   = REPLACE(v-rel, "?", "")
        .

    IF v-rel <> "" THEN
        v-cust-po = v-po + "-" + v-rel.
    ELSE 
        v-cust-po = v-po.

    MESSAGE 
        v-po SKIP
        v-rel SKIP
        v-cust-po SKIP
        VIEW-AS ALERT-BOX.

    FIND FIRST found EXCLUSIVE-LOCK WHERE
        found.cust-po = v-cust-po NO-ERROR.

    IF NOT AVAIL found THEN DO:
        CREATE found.
        ASSIGN
            found.cust-po = v-cust-po
            found.num-of-lines = 1.
    END.
    ELSE DO:
        ASSIGN
            found.num-of-lines = found.num-of-lines + 1.
    END.
END.

hWorkBook:CLOSE("M:\FNDWRR.xlsx") NO-ERROR.
hExcel:QUIT() NO-ERROR.
RELEASE OBJECT hExcel NO-ERROR.
RELEASE OBJECT hWorkBook NO-ERROR.
RELEASE OBJECT hWorkSheet NO-ERROR.
RELEASE OBJECT hWindow NO-ERROR.



OUTPUT TO M:\allmus-po-check.csv.
EXPORT DELIMITER ","
    "PO"
    "Found In SL"
    "Customer"
    "Number of Lines Excel"
    "Number of Lines SL"
    "Order Date SL"
    .
FOR EACH found NO-LOCK WHERE
    found.cust-po <> "":

    ASSIGN
        v-line-count = 0.

    FIND LAST symix.co NO-LOCK WHERE
        co.cust-po = found.cust-po NO-ERROR.

    IF AVAIL co THEN DO:
        FOR EACH symix.coitem NO-LOCK OF symix.co:
            ASSIGN v-line-count = v-line-count + 1.
        END.
    END.

    EXPORT DELIMITER ","
        found.cust-po
        IF AVAIL co THEN "Yes" ELSE "No"
        IF AVAIL co THEN co.cust-num ELSE ""
        found.num-of-lines
        v-line-count
        IF AVAIL co THEN co.order-date ELSE ?
        .
END.


