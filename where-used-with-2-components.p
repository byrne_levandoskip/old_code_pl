


DEF VAR MC01799 LIKE symix.ITEM.ITEM.
DEF VAR MC01800 LIKE symix.ITEM.ITEM.




OUTPUT TO M:\per-brandon.csv.
EXPORT DELIMITER ","
    "Sub Assm"
    "Sub Assm Description"
    "MC01799 Part"
    "MC01800 Part"
    .

FOR EACH symix.jobmatl NO-LOCK WHERE
    (jobmatl.effect-date <= TODAY OR jobmatl.effect-date  = ?) AND
    (jobmatl.obs-date    >  TODAY OR jobmatl.obs-date     = ?),
    EACH symix.jobroute OF symix.jobmatl NO-LOCK WHERE
    (jobroute.effect-date <= TODAY OR jobroute.effect-date = ?) AND
    (jobroute.obs-date    >  TODAY OR jobroute.obs-date    = ?),
    FIRST symix.job NO-LOCK OF symix.jobroute WHERE
    job.TYPE = "s",
    FIRST symix.ITEM NO-LOCK WHERE
    job.ITEM = ITEM.ITEM
    BREAK BY jobmatl.job:

    IF FIRST-OF(jobmatl.job) THEN
        ASSIGN
        MC01799 = ""
        MC01800 = ""
        .

    IF jobmatl.ITEM BEGINS "MC01799" AND 
        MC01799 = "" THEN
        ASSIGN MC01799 = jobmatl.ITEM.
    ELSE IF jobmatl.ITEM BEGINS "MC01799" AND 
        MC01799 <> "" THEN
        MESSAGE
        "More then one MC01799"
        VIEW-AS ALERT-BOX.



    IF jobmatl.ITEM BEGINS "MC01800" AND 
        MC01800 = "" THEN
        ASSIGN MC01800 = jobmatl.ITEM.
    ELSE IF jobmatl.ITEM BEGINS "MC01800" AND 
        MC01800 <> "" THEN
        MESSAGE
        "More then one MC01800"
        VIEW-AS ALERT-BOX.




    IF LAST-OF(jobmatl.job) AND
        MC01799 <> "" AND
        MC01800 <> "" THEN DO:

        EXPORT DELIMITER ","
            ITEM.ITEM
            ITEM.DESCRIPTION
            MC01799
            MC01800
            .
    END.
END.

OUTPUT CLOSE.
