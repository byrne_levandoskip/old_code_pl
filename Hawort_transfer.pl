## Nightly FTP transfer to Steelcase
## David Z. Kil
## Modified extensively by Mike Padget
## BYR-1065 10/02/12 pl Change location of scripts


my $logfile="//byrne-electrical.com/network/public/customer service/electronic_invoices/hawort/log.txt";
my $xferdir="//byrne-electrical.com/network/public/customer service/electronic_invoices/hawort";
my $postdir="//byrne-electrical.com/network/public/customer service/electronic_invoices/hawort";

my $ftphost=		"ftp.haworth.com";
my $ftpuser=		"gll5";
my $ftppass=		"gr82day";
#my $port=		"5678";
my $ftpuploaddir =	"/company194/Company194ext/in";
my $file=               "810_DTL.edi";
my $mailhost=		"bet003.byrne-electrical.com";

my $logentry = "";
my $status;

use strict;

use File::Copy;
use Net::FTP;
use Net::SMTP;

## Open Log file
open(LOG, ">>$logfile") or die "Can't open log file";


&timestamp;

## Log that the transfer has started
print LOG "***Transfer initiated*** \n";


## ********************************
## Initiate FTP transfer
## ********************************

#my $ftp = Net::FTP->new($ftphost, Port => $port) or writelog("Error connecting to $ftphost \n", $logfile);
my $ftp = Net::FTP->new($ftphost) or writelog("Error connecting to $ftphost \n", $logfile);
$ftp->login($ftpuser, $ftppass) or writelog("Couldn't connect to $ftphost \n", $logfile);
$ftp->cwd($ftpuploaddir) or writelog("Couldn't cwd to $ftpuploaddir \n", $logfile);
print LOG "Connected to $ftphost \n", $logfile;
#$ftp->binary();
foreach my $file (@ARGV) {
	$ftp->append("$xferdir/$file") or writelog("Error transfering $file \n");
	print LOG "$file has been appended \n";
      }
$ftp->quit();


## ********************************
## File Cleanup
## ********************************

## Delete file
foreach my $file (@ARGV)
{
unlink("$xferdir/$file") or writelog("$file could not be deleted", $logfile);
}


## ********************************
## Sucess email
## ********************************

my $smtp = Net::SMTP->new("$mailhost",
				Hello => 'byrne-electrical.com',
				Timeout => 60);
	$smtp->mail("supportcenter\@byrne-electrical.com\n");
	$smtp->recipient("supportcenter\@byrne-electrical.com\n");
	$smtp->data;
	$smtp->datasend("From:  Syteline\@byrne-electrical.com\n");
	$smtp->datasend("To: supportcenter\@byrne-electrical.com\n");
	$smtp->datasend("Subject:  FTP transfer to $ftphost Successful\n");
	$smtp->datasend("\n");
	$smtp->dataend;
	$smtp->quit;

print LOG "Success email sent \n";

close (LOG);
exit 0;
## ********************************
## Function to create log entries
## ********************************
sub writelog {

	## Set the logentry variable
#	my $logentry = sprintf("%04d:%02d:%02d %02d:%02d:%02d $_[0]", $Year,$Month,$Day,$Hour,$Minute,$Second);

	## Write logentry to the log file
#	print LOG "$logentry\n";
	&timestamp;

#	print "$logentry\n";
#	close LOG;

	my $smtp = Net::SMTP->new("$mailhost",
				Hello => 'byrne-electrical.com',
				Timeout => 60);
	$smtp->mail("supportcenter\@byrne-electrical.com\n");
	$smtp->recipient("supportcenter\@byrne-electrical.com\n");
	$smtp->data;
	$smtp->datasend("From:  Syteline\@byrne-electrical.com\n");
	$smtp->datasend("To: supportcenter\@byrne-electrical.com\n");
	$smtp->datasend("Subject:  FTP transfer Error\n");
	$smtp->datasend("\n");
	$smtp->datasend("FTP Transfer had errors.  Below is the error message.\n");
	$smtp->datasend("$_[0]\n");
	$smtp->dataend;
	$smtp->quit;

	exit -1;
}

## ********************************
## Get Timestamp
##*********************************
sub timestamp {

	## Get the all the values for current time
	(my $Second, my $Minute, my $Hour, my $Day, my $Month, my $Year, my $WeekDay, my $DayOfYear, my $IsDST) =
        	localtime(time); $Year += 1900; $Month += 1;

	my $logentry = sprintf("%04d:%02d:%02d %02d:%02d:%02d $_[0]", $Year,$Month,$Day,$Hour,$Minute,$Second);
	print LOG "$logentry \n"; 
}
