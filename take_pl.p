/*
    This code is adapted from custom/where-sold.p
    
*/

DEFINE TEMP-TABLE tt-results
    FIELD ITEM AS CHAR FORMAT "x(30)" LABEL "Item"
    FIELD sold-qty LIKE symix.matltran.qty LABEL "Sold Qty"
    FIELD sold-item LIKE tt-results.ITEM LABEL "Sold Item"
    FIELD qty-complete LIKE symix.job.qty-complete
    FIELD unit-price LIKE symix.itemprice.unit-price1
    FIELD inv-price LIKE symix.inv-item.price LABEL "Inv Price"
    FIELD cust-num LIKE symix.customer.cust-num
    FIELD rawMatPerFG AS INT
    FIELD first-co LIKE byrne.co.co-num
    .

DEF TEMP-TABLE tt-in
    FIELD ITEM AS CHAR FORMAT "x(30)" LABEL "Item"
    .

DEFINE VAR t-ditem AS CHAR FORMAT "x(33)" LABEL "Item" NO-UNDO.
DEFINE VAR t-dprodcode AS CHAR FORMAT "x(13)" LABEL "Prod Code" NO-UNDO.
DEFINE VAR show-price LIKE symex.ex-optsz.show-cost INIT YES.

DEFINE VAR v-raw-matl LIKE symix.ITEM.ITEM NO-UNDO.
DEFINE VAR v-raw-qty LIKE symix.matltran.qty NO-UNDO.
DEFINE VAR v-inv-price LIKE symix.inv-item.price NO-UNDO.

/* DEF VAR startDate AS DATE INIT 10/01/17. */
DEF VAR startDate AS DATE INIT 9/20/18.
DEF VAR endDate AS DATE INIT 09/30/18.

RUN mainBlock.


PROCEDURE mainBlock:
    /*input from file*/
/*     INPUT FROM "M:\Projects\Tickets\489056 - China Source Parts Report\china-sourced-parts.csv". */
    INPUT FROM "M:\china-sourced-parts.csv".

    DEF VAR firstt AS LOGICAL INIT YES.
    REPEAT:
        CREATE tt-in.
        IMPORT DELIMITER "," tt-in.ITEM.
        
        IF firstt THEN DO:
            /* Remove header */
            DELETE tt-in.
            ASSIGN firstt = NO.
        END.
    END.

    DELETE tt-in.

    INPUT CLOSE.


    DEF VAR cont AS INT INIT 1.
/*     OUTPUT TO "M:\Projects\Tickets\489056 - China Source Parts Report\progress.txt". */
    OUTPUT TO "M:\progress.txt".


    run findRawMats.
/*     FOR EACH tt-in:                        */
/*         RUN findRawMats(INPUT tt-in.ITEM). */
/*         EXPORT "findRawMats : " cont.      */
/*         ASSIGN cont = cont + 1.            */
/*     END.                                   */

    ASSIGN cont = 1.

    /*get raw mat per fg qty*/
    FOR EACH tt-results:
        RUN getQtyRawMat(INPUT tt-results.ITEM,
                         INPUT tt-results.sold-item).
        EXPORT "getQtyRawMat :" cont.
        ASSIGN cont = cont + 1.
    END.

    OUTPUT CLOSE.

    /*output*/
    RUN displayResults.

END PROCEDURE.

PROCEDURE findRawMats:
/*     DEF INPUT PARAMETER p-item AS CHAR FORMAT "x(30)". */


/*     /* get sales of raw matl */                                                                            */
/*     FOR /* EACH byrne.ITEM NO-LOCK WHERE ITEM.ITEM EQ p-item, */                                           */
/*         EACH byrne.matltran NO-LOCK WHERE /* matltran.ITEM = ITEM.ITEM */                                  */
/*         can-find(first tt-in where tt-in.item = matltran.item)                                             */
/*         AND matltran.trans-date GE startDate                                                               */
/*         AND matltran.trans-date LE endDate                                                                 */
/*         AND matltran.trans-type = "S"                                                                      */
/*         AND matltran.ref-type = "O",                                                                       */
/*         EACH byrne.co NO-LOCK WHERE co.co-num EQ matltran.ref-num:                                         */
/*                                                                                                            */
/*         IF show-price THEN DO:                                                                             */
/*             FIND LAST byrne.inv-item WHERE                                                                 */
/*                 inv-item.co-num EQ matltran.ref-num AND                                                    */
/*                 inv-item.co-line EQ matltran.ref-line AND                                                  */
/*                 inv-item.co-release = matltran.ref-release NO-LOCK NO-ERROR.                               */
/*                                                                                                            */
/*             IF AVAIL(inv-item) THEN                                                                        */
/*                 FIND FIRST tt-results WHERE tt-results.ITEM = matltran.ITEM                                */
/*                 AND tt-results.cust-num EQ co.cust-num                                                     */
/*                 AND tt-results.inv-price EQ inv-item.price NO-ERROR.                                       */
/*         END.                                                                                               */
/*                                                                                                            */
/*         IF NOT show-price OR NOT AVAIL(inv-item) THEN                                                      */
/*             FIND FIRST tt-results WHERE tt-results.ITEM = matltran.ITEM                                    */
/*             AND tt-results.cust-num = co.cust-num NO-ERROR.                                                */
/*         IF NOT AVAIL(tt-results) THEN DO:                                                                  */
/*             FIND LAST byrne.itemprice /* OF ITEM */ where itemprice.item = matltran.item NO-LOCK NO-ERROR. */
/*             CREATE tt-results.                                                                             */
/*             ASSIGN                                                                                         */
/*                 tt-results.ITEM = matltran.ITEM                                                            */
/*                 tt-results.cust-num = co.cust-num                                                          */
/*                 tt-results.sold-qty = matltran.qty * -1                                                    */
/*                 tt-results.sold-item = tt-results.ITEM                                                     */
/*                 tt-results.qty-complete = tt-results.sold-qty                                              */
/*                 tt-results.unit-price = IF AVAIL(itemprice) THEN itemprice.unit-price1 ELSE 0              */
/*                 tt-results.inv-price = IF AVAIL(inv-item) THEN inv-item.price ELSE 0                       */
/*                 tt-results.first-co = co.co-num                                                            */
/*                 .                                                                                          */
/*         END.                                                                                               */
/*         ELSE                                                                                               */
/*             ASSIGN                                                                                         */
/*                 tt-results.sold-qty = tt-results.sold-qty + (matltran.qty * -1)                            */
/*                 tt-results.qty-complete = tt-results.sold-qty                                              */
/*                 .                                                                                          */
/*     END. /*END giant For each loop */                                                                      */

    FOR /* EACH byrne.ITEM NO-LOCK WHERE
        ITEM.ITEM EQ p-item, */
        EACH byrne.matltran NO-LOCK WHERE /* matltran.ITEM EQ ITEM.ITEM */
        can-find(first tt-in where tt-in.item = matltran.item)
        AND matltran.trans-date GE startDate
        AND matltran.trans-date LE endDate
        AND matltran.trans-type EQ "I",
/*         AND matltran.ref-type = "J", */
        first byrne.job NO-LOCK WHERE job.job EQ matltran.ref-num
        AND job.suffix = matltran.ref-line-suf:
/*         BREAK BY job.job BY job.suffix: */

        export delimiter "|" 
            "matl-loop" 
            matltran.item
            matltran.trans-date 
            matltran.ref-num
            matltran.ref-line-suf
            matltran.trans-type
            matltran.ref-type
            .

        /*accumulate qty by job*/
        ASSIGN
            v-raw-matl = matltran.ITEM /* ITEM.ITEM */
            v-raw-qty = v-raw-qty + (matltran.qty * -1)
            v-inv-price = 0.

/*         IF LAST-OF(job.suffix) THEN DO: */
            IF show-price THEN DO:
                FIND LAST byrne.inv-item WHERE inv-item.co-num EQ job.ord-num
                    AND inv-item.co-line EQ job.ord-line
                    AND inv-item.co-release EQ job.ord-release NO-LOCK NO-ERROR.
                IF AVAIL(inv-item) THEN
                    ASSIGN v-inv-price = inv-item.price.
            END.

            IF job.ord-num NE "" THEN
                RUN PROCESS_CO(INPUT job.ITEM,
                               INPUT job.cust-num,
                               INPUT job.qty-complete,
                               INPUT job.ord-num).
            ELSE DO:
                RUN PROCESS_JOB(INPUT job.ITEM,
                                INPUT job.qty-complete,
                                INPUT job.ord-num).
            END.
            ASSIGN v-raw-qty = 0.
/*         END. */
    END. /*end second giant for each loop */


END PROCEDURE.


PROCEDURE PROCESS_CO:
    DEFINE INPUT PARAMETER t-item LIKE symix.coitem.ITEM.
    DEFINE INPUT PARAMETER t-cust-num LIKE symix.coitem.cust-num.
    DEFINE INPUT PARAMETER t-qty-complete LIKE symix.job.qty-complete.
    DEF INPUT PARAM coNum LIKE byrne.job.ord-num.

    export delimiter "|" 
        "PROCESS_CO" 
        v-raw-matl
        t-item
        t-cust-num 
        t-qty-complete
        coNum
        .

    FIND FIRST tt-results WHERE 
        tt-results.ITEM = v-raw-matl
        AND tt-results.sold-item = t-item
        AND tt-results.cust-num = t-cust-num 
        AND tt-results.inv-price = v-inv-price NO-ERROR.
    IF NOT AVAILABLE(tt-results) THEN DO:
        FIND LAST symix.itemprice WHERE ITEMprice.ITEM = t-item NO-LOCK NO-ERROR.
        CREATE tt-results.
        ASSIGN tt-results.ITEM = v-raw-matl
               tt-results.cust-num = t-cust-num
               tt-results.sold-qty = v-raw-qty
               tt-results.sold-item = t-item
               tt-results.qty-complete = t-qty-complete
               tt-results.unit-price = IF AVAILABLE(itemprice) THEN itemprice.unit-price1 ELSE 0
               tt-results.inv-price = v-inv-price
               tt-results.first-co = coNum.
    END.
    ELSE 
        ASSIGN 
            tt-results.sold-qty = tt-results.sold-qty + v-raw-qty
            tt-results.qty-complete = tt-results.qty-complete + t-qty-complete.

END PROCEDURE.  /* Process_CO */

PROCEDURE PROCESS_JOB:
    DEFINE INPUT PARAMETER t-item LIKE symix.ITEM.ITEM.
    DEFINE INPUT PARAMETER t-qty-complete LIKE symix.job.qty-complete.
    DEF INPUT PARAM coNum LIKE byrne.job.ord-num.

    export delimiter "|" 
        "PROCESS_JOB" 
        v-raw-matl
        t-item
        t-qty-complete
        coNum
        .

    FIND FIRST tt-results WHERE 
        tt-results.ITEM = v-raw-matl
        AND tt-results.sold-item = t-item
        AND tt-results.cust-num = 'INV' NO-ERROR.
    IF NOT AVAILABLE(tt-results) THEN DO:

        FIND LAST symix.itemprice WHERE ITEMprice.ITEM = t-item NO-LOCK NO-ERROR.
        CREATE tt-results.
        ASSIGN tt-results.ITEM = v-raw-matl
               tt-results.cust-num = 'INV'
               tt-results.sold-qty = v-raw-qty
               tt-results.sold-item = t-item
               tt-results.qty-complete = t-qty-complete
               tt-results.unit-price = IF AVAILABLE(itemprice) THEN itemprice.unit-price1 ELSE 0
               tt-results.first-co = coNum.
    END.
    ELSE 
        ASSIGN 
            tt-results.sold-qty = tt-results.sold-qty + v-raw-qty
            tt-results.qty-complete = tt-results.qty-complete + t-qty-complete.

END PROCEDURE. /* Process_Job */

PROCEDURE displayResults:

    DEF VAR cust1 LIKE byrne.customer.cust-num INIT "STEEL".
    DEF VAR cust2 LIKE byrne.customer.cust-num INIT "HERMAN".

    /*OUTPUT TO "C:\temp\china-parts.csv".*/
/*     OUTPUT TO "M:\Projects\Tickets\489056 - China Source Parts Report\output.csv". */
    OUTPUT TO "M:\output.csv".

    EXPORT DELIMITER ","
                "SOLD ITEM (FG)"
                "RAW MATL"
                "QTY OF RAW MAT PER FG"
                "FG QTY"
                "FG TOTAL $ SOLD"
                "CUSTOMER"
                "FIRST CO"
                .

    FOR EACH tt-results:
        IF tt-results.cust-num EQ cust1 OR tt-results.cust-num EQ cust2 THEN DO:
            EXPORT DELIMITER ","
                tt-results.sold-item 
                tt-results.ITEM 
                tt-results.rawMatPerFG
                tt-results.sold-qty
                (tt-results.inv-price * tt-results.sold-qty)
                tt-results.cust-num
                tt-results.first-co
                .
        END.
    END.

    OUTPUT CLOSE.
END PROCEDURE.

PROCEDURE getQtyRawMat:
    DEF INPUT PARAM rawItem LIKE byrne.ITEM.ITEM.
    DEF INPUT PARAM fgItem LIKE byrne.ITEM.ITEM.


    FOR EACH byrne.job WHERE job.TYPE = "S"
        AND job.suffix = 0
        AND job.ITEM = fgItem NO-LOCK,
        EACH byrne.jobmatl NO-LOCK WHERE jobmatl.job EQ job.job
        AND jobmatl.ITEM EQ rawItem
        AND jobmatl.matl-qty <> 0:

        /*
        DISPLAY job.ITEM jobmatl.ITEM jobmatl.matl-qty.
        */

        FOR EACH tt-results WHERE tt-results.sold-item = fgItem
            AND tt-results.ITEM EQ rawItem:
            ASSIGN
                tt-results.rawMatPerFG = jobmatl.matl-qty.
        END.
    END.
END PROCEDURE.
