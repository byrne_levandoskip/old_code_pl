


def shared var tester AS LOG.

tester = FALSE.

IF tester = TRUE THEN
    MESSAGE "Something truthful" VIEW-AS ALERT-BOX.

IF tester = FALSE THEN
    MESSAGE "Something non-truthful" VIEW-AS ALERT-BOX.
