


DEFINE TEMP-TABLE all-jobs
    FIELD job   LIKE symix.job.job.

RUN get-jobs.
RUN find-orphan-trans.


PROCEDURE get-jobs.

    INPUT FROM m:\jobs_7920-6703+HAWORT.csv NO-ECHO.

    REPEAT:
        CREATE all-jobs.
        IMPORT DELIMITER "," all-jobs.
    END.

    INPUT CLOSE.

END PROCEDURE.


PROCEDURE find-orphan-trans:

    DEF VAR job-found AS LOG.

    FOR EACH symix.matltrans NO-LOCK WHERE
        matltrans.trans-type = "F" AND
        matltrans.trans-date GE 07/01/14 AND
        matltrans.ITEM = "7920-6703+HAWORT":

        job-found = FALSE.

        FOR EACH all-jobs NO-LOCK:
            IF all-jobs.ref-num = matltrans.job THEN DO:
                job-found = TRUE.
                LEAVE.
            END.
        END.

        IF NOT(job-found) THEN DO:
            OUTPUT TO M:\orphan-trans.csv APPEND. 
            EXPORT DELIMITER ","
                matltran.
            OUTPUT CLOSE.
        END.

    END.

END PROCEDURE.
