


DEF TEMP-TABLE found
    FIELD ITEM  LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE found-out
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD DESCRIPTION   LIKE symix.ITEM.DESCRIPTION
    FIELD cur-wc        LIKE symix.jobroute.wc
    FIELD qty           AS INT INITIAL 0
    FIELD has-mex       AS LOGICAL INITIAL FALSE.

DEF VAR mex-item AS CHAR FORMAT "x(30)".


INPUT FROM M:\paul.txt.
REPEAT:
    CREATE found.
    IMPORT DELIMITER "," found.
    ASSIGN found.ITEM = TRIM(found.ITEM).
END.
INPUT CLOSE.


FOR EACH found NO-LOCK WHERE
    found.ITEM <> "",
    EACH symix.jobmatl NO-LOCK WHERE
    jobmatl.ITEM = found.ITEM,
    FIRST symix.job NO-LOCK OF symix.jobmatl WHERE
    job.TYPE = "S",
    FIRST symix.jobroute NO-LOCK OF symix.job,
    FIRST symix.ITEM NO-LOCK WHERE
    ITEM.ITEM = job.ITEM:

    CREATE found-out.
    ASSIGN
        found-out.ITEM = ITEM.ITEM
        found-out.DESCRIPTION = ITEM.DESCRIPTION
        found-out.cur-wc = jobroute.wc
        .
END.



FOR EACH found-out EXCLUSIVE-LOCK WHERE
    found-out.ITEM <> "":


    FOR EACH symix.coitem NO-LOCK WHERE
        coitem.ITEM = found.ITEM AND
        coitem.due-date GE 05/01/14 AND
        coitem.qty-shipped GT 0:

        ASSIGN
            found-out.qty = found-out.qty + coitem.qty-shipped.
    END.

    ASSIGN
        mex-item = "M" + found-out.ITEM.

    FIND FIRST symix.job NO-LOCK WHERE
        job.ITEM = found-out.ITEM AND
        job.TYPE = "S" NO-ERROR.

    FIND FIRST symix.jobmatl NO-LOCK OF symix.job WHERE
        jobmatl.ITEM = mex-item NO-ERROR.

    IF AVAIL jobmatl THEN
        ASSIGN found-out.has-mex = TRUE.

END.



OUTPUT TO M:\legacy-parts-check.csv.
EXPORT DELIMITER ","
    "Item"
    "Description"
    "Current WC"
    "Past 3 Yrs Shipped"
    "BOM has been Updated"
    .

FOR EACH found-out NO-LOCK WHERE
    found-out.ITEM <> "":
    EXPORT DELIMITER ","
        found-out.
END.
OUTPUT CLOSE.







