



def buffer b-coitem-log for byrne.coitem-log.

output to M:\yesterday-due-date-changes.csv.
export delimiter ","
    "User Initials"
    "Customer"
    "Order"
    "Line"
    "Item"
    "Activity Date"
    "Activity Time"
    "Activity Seq"
    "Prev Due Date"
    "New Due Date"
    "Date Change"
    .

for each byrne.coitem-log no-lock where
    coitem-log.activity-date = today - 1 and
    coitem-log.activity-seq gt 1 and
    coitem-log.due-date <> ?,
    first byrne.co no-lock of byrne.coitem-log,
    first byrne.ux-coitem-log no-lock of byrne.coitem-log:

    find last b-coitem-log no-lock where
        b-coitem-log.co-num = coitem-log.co-num and
        b-coitem-log.co-line = coitem-log.co-line and
        b-coitem-log.activity-seq lt coitem-log.activity-seq and
        b-coitem-log.due-date <> ? no-error.

    if b-coitem-log.due-date <> coitem-log.due-date then
        export delimiter ","
        ux-coitem-log.uf-user-code
        co.cust-num
        coitem-log.co-num
        coitem-log.co-line
        coitem-log.item
        coitem-log.activity-date
        string(coitem-log.activity-time, "HH:MM:SS")
        coitem-log.activity-seq
        b-coitem-log.due-date
        coitem-log.due-date
        coitem-log.due-date - b-coitem-log.due-date
/*         if b-coitem-log.activity-seq <> coitem-log.activity-seq - 1 then "FollowUp" else "Expected" */
        .
end.
output close.


unix silent "uuencode /tmp/yesterday-due-date-changes.csv yesterday-due-date-changes.csv | \ 
    mailx -m -s 'Yesterdays Due Date Changes' \ 
    levandoskip@byrne.com wilsons@byrne.com". 

unix silent "rm /tmp/yesterday-due-date-changes.csv".
