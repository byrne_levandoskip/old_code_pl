
def temp-table found
    field family-code   like symix.famcode.family-code
    field c-item        as int
    field c-prod-code   as char format "x(300)"
    .

for each symix.famcode no-lock:
    create found.
    assign 
        found.family-code = famcode.family-code
        .
end.

create found.



for each symix.item no-lock,
    first found exclusive-lock where
    found.family-code = item.family-code:
    assign
        found.c-item = found.c-item + 1
        found.c-prod-code = if index(found.c-prod-code, item.product-code) = 0 then
            found.c-prod-code + item.product-code + " | " else
            found.c-prod-code
            .
end.


output to M:\Family-Code-Usage.csv.
export delimiter ","
    "Family Code"
    "Item Ref Count"
    "Prod Code Refs"
    .
for each found no-lock:
    export delimiter ","
        found.
end.
output close.
