

/* 
paper work printed but 
    -open orders
    -due date not in the next X days 
    -due date has changed in the past Y days 
*/

def var f-beyond-due-date like symix.coitem.due-date.
def var f-date-chg-range as date.
def var f-file-name as char format "x(100)".
def var f-file-path as char format "x(100)".
def buffer b-coitem-log for symix.coitem-log.

assign
    f-beyond-due-date = today - 7 /* Y */
    f-date-chg-range = today + 7 /* X */
    f-file-path = "C:\temp\"
    f-file-name = f-file-path + "wasis-orders-outside-lead-time.csv"
    .

output to value(f-file-name).
export delimiter ","
    "Customer"
    "Order"
    "Line"
    "Item"
    "Activity Date"
    "Old Due Date"
    "Current Due Date"
    "Con Lot"
    "Lotsheet Print Date"
    .

for each symix.coitem no-lock where
    coitem.stat = "O" and
    coitem.due-date gt f-date-chg-range,
    each symix.ux-coitem no-lock of symix.coitem where
    ux-coitem.uf-date-lotsht <> ?,
    last symix.coitem-log no-lock where
    coitem-log.co-num = coitem.co-num and
    coitem-log.co-line = coitem.co-line and
    coitem-log.activity-date ge f-beyond-due-date and
    coitem-log.activity-seq gt 1 and
    coitem-log.due-date <> ?,
    first symix.co no-lock of symix.coitem:

    find last b-coitem-log no-lock where
        b-coitem-log.co-num = coitem.co-num and
        b-coitem-log.co-line = coitem.co-line and
        b-coitem-log.activity-seq lt coitem-log.activity-seq and
        b-coitem-log.due-date <> ? no-error.

    export delimiter ","
        co.cust-num
        coitem.co-num
        coitem.co-line
        coitem.item
        coitem-log.activity-date
        b-coitem-log.due-date
        coitem.due-date
        ux-coitem.uf-con-lot
        ux-coitem.uf-date-lotsht
        .
end.
output close.

