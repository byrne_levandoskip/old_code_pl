

def var file-suffix     as char.
def var emp-num         as char format "x(07)".
def var times-exported  as int initial 1.

def temp-table found
    field item          like symix.item.item
    field whse          like symix.itemloc.whse
    field start-qty     like symix.itemloc.qty-on-hand
    field pend-qty      like symix.itemloc.qty-on-hand
    field recv-qty      like symix.itemloc.qty-on-hand
    field skip-qty      like symix.itemloc.qty-on-hand
    field prop-qty      like symix.itemloc.qty-on-hand
    field loc           like symix.itemloc.loc
    field unit-cost     like symix.item.unit-cost
    field cur-u-cost    like symix.item.cur-u-cost
    field keep-recv     as log
    field has-skip      as log
    .



assign
    file-suffix = replace(string(today), "/", "") + "_" + string(time)
    emp-num = "  32001"
    .

run find-trans-loc.
run export-found-temp-table.
run back-fill-transfers.
run export-found-temp-table.


procedure back-fill-transfers:

    output to value("M:\back-fill-transfers_" + file-suffix + ".csv").
    export delimiter ","
        "Found an Item with Qty in 999"
        "Transfer Num"
        "Transfer Line"
        "Item"
        "Qty Shipped"
        "Qty Received"
        "Qty Loss"
        .

    RECV-LOOP:
    for each symix.trnitem no-lock where
        trnitem.qty-shipped <> (trnitem.qty-received + trnitem.qty-loss),
        first symix.transfer no-lock of symix.trnitem
        break by trnitem.sch-ship-date desc:


        find first found exclusive-lock where
            found.whse = transfer.to-whse and
            found.item = trnitem.item no-error.


        if trnitem.sch-ship-date ge today - 7 then do:

            assign
                found.pend-qty = found.pend-qty + (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss))
                found.skip-qty = found.skip-qty + (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss))
                found.has-skip = true
                .

            export delimiter ","
                "Skip Recent"
                trnitem.trn-num
                trnitem.trn-line
                trnitem.item
                trnitem.qty-shipped 
                trnitem.qty-received
                trnitem.qty-loss
                .
        end.
        else if avail found and found.keep-recv then do:

            if found.prop-qty - (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss)) lt 0 then do:
                assign
                    found.pend-qty = found.pend-qty + (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss))
                    found.keep-recv = false
                    .

                export delimiter ","
                    "Stopped Recv"
                    trnitem.trn-num
                    trnitem.trn-line
                    trnitem.item
                    trnitem.qty-shipped 
                    trnitem.qty-received
                    trnitem.qty-loss
                    .
                next RECV-LOOP.
            end.


            assign 
                found.prop-qty = found.prop-qty - (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss))
                found.recv-qty = found.recv-qty + (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss))
                found.pend-qty = found.pend-qty + (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss))
                .

            run create-itemloc
                (input trnitem.item,
                 input transfer.to-whse).
            
            run create-dc-records
                (input trnitem.trn-num,
                 input trnitem.trn-line,
                 input (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss)),
                 input trnitem.u-m,
                 input "2",
                 input "clean-trans").

            export delimiter ","
                "Found"
                trnitem.trn-num
                trnitem.trn-line
                trnitem.item
                trnitem.qty-shipped 
                trnitem.qty-received
                trnitem.qty-loss
                .
        end.
        else if avail found and found.keep-recv = false then do:

            assign
                found.pend-qty = found.pend-qty + (trnitem.qty-shipped - (trnitem.qty-received + trnitem.qty-loss))
                .

            export delimiter ","
                "Stopped Recv"
                trnitem.trn-num
                trnitem.trn-line
                trnitem.item
                trnitem.qty-shipped 
                trnitem.qty-received
                trnitem.qty-loss
                .
        end.
        else do:
            export delimiter ","
                "Not Found"
                trnitem.trn-num
                trnitem.trn-line
                trnitem.item
                trnitem.qty-shipped 
                trnitem.qty-received
                trnitem.qty-loss
                .
        end.     
    end.
    output close.
    
end procedure.


procedure export-found-temp-table:
    output to value("M:\export-transit-locations-rd" + string(times-exported) + "_" + file-suffix + ".csv").
    export delimiter ","
        "Item"
        "Whse"
        "Start Qty"
        "Pending Qty"
        "Received Qty"
        "Skipped Qty"
        "Proposed Qty"
        "Location"
        "Unit Cost"
        "Cur Unit Cost"
        "Received All Pending?"
        "Has Recent Transfers?"
        .
    for each found no-lock:
        export delimiter ","
            found.
    end.
    output close.

    assign times-exported = times-exported + 1.

end procedure.


procedure find-trans-loc:
    for each symix.location no-lock where
        location.loc-type = "T",
        each symix.itemloc no-lock where
    	itemloc.loc = location.loc and
        itemloc.qty-on-hand <> 0,
        first symix.item no-lock where
        item.item = itemloc.item:

        create found.
        assign
            found.item = itemloc.item
            found.whse = itemloc.whse
            found.start-qty = itemloc.qty-on-hand
            found.prop-qty = itemloc.qty-on-hand
            found.loc = itemloc.loc
            found.unit-cost = item.unit-cost
            found.cur-u-cost = item.cur-u-cost
            found.keep-recv = true
            found.has-skip = false
            .
    end.
end procedure. /* find-trans-loc */




procedure create-dc-records:

    DEF INPUT  PARAMETER    p-trn-num       LIKE symix.transfer.trn-num.
    DEF INPUT  PARAMETER    p-trn-line      LIKE symix.transfer.trn-line.
    DEF INPUT  PARAMETER    p-qty           LIKE symix.trnitem.qty-req.
    DEF INPUT  PARAMETER    p-u-m           LIKE symix.trnitem.u-m.
    DEF INPUT  PARAMETER    p-trans-type    LIKE symix.transfer.trn-num. /* 2 to receive */
    DEF INPUT  PARAMETER    p-loc           LIKE symix.location.loc.
    DEF VAR                 v-last-trans    LIKE symix.dctrans.trans-num.
    
    FIND LAST symix.dctrans NO-LOCK NO-ERROR.

    IF AVAIL dctrans THEN
        v-last-trans = dctrans.trans-num + 1.
    ELSE
        v-last-trans = 1.

    CREATE symix.dctrans.
    ASSIGN
        dctrans.trans-num = v-last-trans
    /*     dctrans.termid = "" */
        dctrans.trans-date = TODAY
        dctrans.trans-time = TIME
        dctrans.emp-num = emp-num
        dctrans.trans-type = p-trans-type
        dctrans.trn-num = p-trn-num
        dctrans.trn-line = p-trn-line
        dctrans.qty = p-qty
        dctrans.stat = "U"
        dctrans.u-m = p-u-m
        dctrans.loc = p-loc
        dctrans.OVERRIDE = TRUE
    /*     dctrans.lot = "" */
    /*     dctrans.trn-lot = "" */
        .

    output to value("M:\create-dc-records_" + file-suffix + ".csv") append.
    export delimiter ","
        dctrans.
    output close.

end procedure.




procedure create-itemloc:

    def input  parameter    p-item          like symix.item.item.
    def input  parameter    p-whse          like symix.whse.whse.
    def var                 t-iloc-recid    as int.
    def var                 std-msg         as char.
    def var                 v-trn-line      like symix.trnitem.trn-line.
    
    find first symix.item no-lock where
        item.item = p-item no-error.

    find first symix.itemloc no-lock where
        itemloc.whse = p-whse and
        itemloc.item = p-item and 
        itemloc.loc = "clean-trans" no-error.

    if not avail itemloc then do:
          /* Create the itemloc record. */
        assign
            std-msg = ""
            t-iloc-recid = 0.
        run item/iloc-a.p (
            INPUT p-whse,
            INPUT item.item,
            INPUT "clean-trans",
            INPUT FALSE,
            INPUT 0,
            INPUT 0,
            INPUT 0,
            INPUT 0,
            INPUT 0,
            INPUT 0,
            OUTPUT std-msg,
            OUTPUT t-iloc-recid
            ) NO-ERROR.

        find first symix.itemloc where
            recid(itemloc) = t-iloc-recid no-error.
        
        output to value("M:\create-itemloc_" + file-suffix + ".csv") append.
        export delimiter ","
            itemloc.
        output close.

        release itemloc.
    end.
end procedure. /* create-transfer-item */



