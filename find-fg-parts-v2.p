
DEF TEMP-TABLE found-subassm
    FIELD ITEM  LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE found-fg
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD sub-assm  LIKE symix.ITEM.ITEM.


/* DEF TEMP-TABLE all-orders                              */
/*     FIELD ITEM          LIKE symix.ITEM.ITEM           */
/*     FIELD DESCRIPTION   LIKE symix.ITEM.DESCRIPTION    */
/*     FIELD sub-assm      LIKE symix.ITEM.ITEM           */
/*     FIELD co-num        LIKE symix.coitem.co-num       */
/*     FIELD co-line       LIKE symix.coitem.co-line      */
/*     FIELD qty-invoiced  LIKE symix.coitem.qty-invoiced */
/*     FIELD unit-price    LIKE symix.coitem.price-conv   */
/*     FIELD net-price     AS DEC                         */
/*     FIELD due-date      LIKE symix.coitem.due-date     */
/*     FIELD cust-num      LIKE symix.co.cust-num.        */


FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM BEGINS "02884" OR
    ITEM.ITEM BEGINS "02885" OR
    ITEM.ITEM BEGINS "02886" OR
    ITEM.ITEM BEGINS "02887" OR
    ITEM.ITEM BEGINS "03020" OR
    ITEM.ITEM BEGINS "03022" OR
    ITEM.ITEM BEGINS "03023" OR
    ITEM.ITEM BEGINS "03049" OR
    ITEM.ITEM BEGINS "03050" OR
    ITEM.ITEM BEGINS "03051" OR
    ITEM.ITEM BEGINS "03134" OR
    ITEM.ITEM BEGINS "03135" OR
    ITEM.ITEM BEGINS "03216":

    CREATE found-subassm.
    ASSIGN found-subassm.ITEM = ITEM.ITEM.

END.

OUTPUT TO M:\found-subassm.csv.
FOR EACH found-subassm:
    EXPORT DELIMITER ","
        found-subassm.
END.
OUTPUT CLOSE.


FOR EACH found-subassm,
    EACH symix.jobmatl NO-LOCK WHERE
    jobmatl.ITEM = found-subassm.ITEM AND
    (jobmatl.effect-date  <= TODAY OR 
     jobmatl.effect-date  = ?) AND
    (jobmatl.obs-date     >  TODAY OR 
     jobmatl.obs-date     = ?),
    EACH symix.job NO-LOCK OF symix.jobmatl WHERE
    job.TYPE = "s",
    EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM = job.ITEM:

    CREATE found-fg.
    ASSIGN 
        found-fg.ITEM       = ITEM.ITEM
        found-fg.sub-assm   = found-subassm.ITEM.
END.


/* OUTPUT TO M:\found-fg.csv. */
/* FOR EACH found-fg:         */
/*     EXPORT DELIMITER ","   */
/*         found-fg.          */
/* END.                       */
/* OUTPUT CLOSE.              */

OUTPUT TO M:\per-hansen.csv.
EXPORT DELIMITER ","
    "Item"
    "Description"
    "Order Num"
    "Order Line"
    "Qty"
    "Unit Price"
    "Net Price"
    "Due Date"
    "Customer"
    .
FOR EACH symix.coitem NO-LOCK WHERE
    coitem.due-date GE 01/01/15 AND
    coitem.due-date LE 12/31/15 AND
    coitem.qty-invoiced GT 0 AND
    CAN-FIND(FIRST found-fg WHERE 
             found-fg.ITEM = coitem.ITEM),
    FIRST symix.co NO-LOCK OF symix.coitem,
    FIRST symix.ITEM NO-LOCK WHERE
    ITEM.ITEM = coitem.ITEM:

    EXPORT DELIMITER ","
        coitem.ITEM
        ITEM.DESCRIPTION
        coitem.co-num
        coitem.co-line
        coitem.qty-invoiced
        coitem.price-conv
        (coitem.price-conv * coitem.qty-invoiced)
        coitem.due-date
        co.cust-num
        .
END.



