
def var has-jobmatl as log.
def var label-item as char.

def temp-table found
    field item      like symix.item.item
    .

def temp-table lbls
    field item      like symix.item.item
    .

input from M:\bax-parts.txt.
repeat :
    create found.
    import delimiter "|" found.
end.
input close.


output to M:\labels-bom-loop.csv.
for each found no-lock where
    found.item <> "":

    assign has-jobmatl = false.

    find first symix.item no-lock where
        item.item = found.item no-error.

    if avail item then do:
        for each symix.jobmatl no-lock where
            jobmatl.job = item.job and
            jobmatl.item begins "LB":
            assign has-jobmatl = true.
            export delimiter ","
                "Has Label Data"
                found.item
                jobmatl.item
                .
            find first lbls no-lock where
                lbls.item = jobmatl.item no-error.
            if not avail lbls then do:
                create lbls.
                assign lbls.item = jobmatl.item.
            end.
        end.
        if has-jobmatl = false then do:
            export delimiter ","
                "No Label Data"
                found.item
                .
        end.
    end.
    else do:
        export delimiter ","
            "No Item Record"
            found.item
            .
    end.     
end.
output close.

output to M:\label-table-export.csv.
for each lbls no-lock where
    lbls.item <> "":

    assign label-item = substring(lbls.item, 3) + ".BTW".

    find first symix.be-labelfields no-lock where
        be-labelfields.lbl-format = label-item no-error.
    if avail be-labelfields then do:
        export delimiter ","
            "Label Found"
            lbls.item
            be-labelfields.lbl-format
            be-labelfields.boxlbl-log
            be-labelfields.automation-log
            .
    end.
    else do:
        export delimiter ","
            "No Label"
            lbls.item
            label-item
            .
    end.
end.
output close.
