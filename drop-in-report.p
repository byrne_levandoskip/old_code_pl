
    
OUTPUT TO M:\drop-in-report.csv.
EXPORT DELIMITER ","
    "Order"
    "Customer"
    "Order Date"
    "Taken By"
    "Misc Chgs"
    "Total Price"
    "Freight"
    "Status"
    "Total Cost"
    "EDI Order"
    "Ln#"
    "CGS Total"
    "CI"
    "Due Date"
    "Item"
    "Ordered"
    "U/M"
    "Unit Price"
    "CGS Material Total"
    "CGS Labor Total"
    "CGS Fix Ovhd Total"
    "CGS Var Ovhd Total"
    "CGS Outside Total"
    .
    
FOR EACH byrne.co NO-LOCK WHERE
    co.order-date >= 01/01/2016 AND 
    co.order-date <= 12/31/2018,
    EACH byrne.coitem NO-LOCK OF byrne.co WHERE
    coitem.due-date LE co.order-date:
    

    EXPORT DELIMITER ","
        co.co-num
        co.cust-num
        co.order-date
        co.taken-by
        co.misc-charges
        co.price
        co.freight
        co.stat
        co.cost
        co.edi-order
        coitem.co-line
        coitem.cgs-total
        coitem.cust-item
        coitem.due-date
        coitem.item
        coitem.qty-ordered
        coitem.u-m
        coitem.price
        coitem.cgs-total-matl
        coitem.cgs-total-lbr
        coitem.cgs-total-fovhd
        coitem.cgs-total-vovhd
        coitem.cgs-total-out
        .
END.
