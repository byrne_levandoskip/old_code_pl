
def var due-last    as date.
def var due-cur     as date.
def var act-last    like symix.coitem-log.activity-seq.
def var act-cur     like symix.coitem-log.activity-seq.
def var found       as log.

OUTPUT TO M:\order-due-7-15.txt.
EXPORT DELIMITER "|"
    "Customer"
    "Cust PO"
    "Taken By"
    "Order Date"
    "Order"
    "Line"
    "Due Date"
    "Item"
    "Qty Ordered"
    "Found?"
    "Activity Seq"
    "Prior due date"
    "Who made last change"
    "When last change date"
    "When last change time"
    .
for each symix.coitem no-lock where
    coitem.due-date = 07/15/19 and
    coitem.stat = "O",
    first symix.co no-lock of symix.coitem:

    assign
        due-last = ? 
        due-cur = ?
        act-last = 0
        act-cur = 0
        found = false
        .

    for each symix.coitem-log no-lock of symix.coitem
        break by coitem-log.activity-seq desc:

        if coitem-log.due-date <> ? then
            assign
            due-last = due-cur
            due-cur = coitem-log.due-date
            act-last = act-cur
            act-cur = coitem-log.activity-seq
            .

        if due-last <> ? and
            due-last <> due-cur then do:
            found = true.
            leave.
        end.
    end.

    if found then do: 
        find first symix.coitem-log no-lock of symix.coitem where
            coitem-log.activity-seq = act-last no-error.
        find first symix.ux-coitem-log no-lock of symix.coitem-log no-error.
    end.

    export delimiter "|"
        co.cust-num
        co.cust-po
        co.taken-by
        co.order-date
        coitem.co-num
        coitem.co-line
        coitem.due-date
        coitem.item
        coitem.qty-ordered
        found
        act-cur
        due-cur
        if avail ux-coitem-log then ux-coitem-log.uf-user-code else ""
        if avail ux-coitem-log then coitem-log.activity-date else ?
        if avail ux-coitem-log then string(coitem-log.activity-time, "HH:MM:SS") else ""
        .
END.


