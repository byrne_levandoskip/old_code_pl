

def temp-table found-prod
	field product-code		like symix.item.product-code
	field description		like symix.prodcode.description
	field ord-count			as int
	field qty-count			like symix.coitem.qty-shipped
	.
	
def temp-table found-cust
	field terms-code		like symix.terms.terms-code
	field description		like symix.terms.description
	field ord-count			as int
	.
	
for each symix.co no-lock where
	co.order-date ge 01/01/19 and
	co.order-date le 01/31/19,
    each symix.coitem no-lock of symix.co where
	coitem.qty-shipped gt 0,
    first symix.customer no-lock where
	customer.cust-num = co.cust-num and
	customer.cust-seq = 0 and
	customer.cust-type = "INBD",
	first symix.item no-lock where
	item.item = coitem.item:
	
	find first found-prod exclusive-lock where
		found-prod.product-code = item.product-code no-error.
		
	if not avail found-prod then do:
        find first symix.prodcode no-lock where
            prodcode.product-code = item.product-code no-error.
        create found-prod.
        assign
            found-prod.product-code = prodcode.product-code
            found-prod.description = prodcode.description
            .
	end.
    
    assign
        found-prod.ord-count = found-prod.ord-count + 1
        found-prod.qty-count = found-prod.qty-count + coitem.qty-shipped
        .
	
	find first found-cust exclusive-lock where
		found-cust.terms-code = co.terms-code no-error.
		
	if not avail found-cust then do:
        find first symix.terms no-lock where
            terms.terms-code = co.terms-code no-error.
        create found-cust.
        assign
            found-cust.terms-code = terms.terms-code
            found-cust.description = terms.description
            .
	end.

    assign
        found-cust.ord-count = found-cust.ord-count + 1
        .
end.

output to M:\inbound-2019-prodcode.csv.
export delimiter ","
    "Product Code"
    "Description"
    "Count"
    "Qty Shipped"
    .
for each found-prod no-lock:
    export delimiter ","
        found-prod.
end.
output close.

output to M:\inbound-2019-terms.csv.
export delimiter ","
    "Terms"
    "Description"
    "Count"
    .
for each found-cust no-lock:
    export delimiter ","
        found-cust.
end.
output close.
