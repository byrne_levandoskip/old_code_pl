


DEF TEMP-TABLE move-files 
    FIELD lb        AS CHAR FORMAT "x(30)"
    FIELD old-loc   AS CHAR FORMAT "x(100)"
    FIELD new-loc   AS CHAR FORMAT "x(100)"
    .



INPUT FROM M:\move-me.csv.
REPEAT:
    CREATE move-files.
    IMPORT DELIMITER "," move-files.
    ASSIGN
        old-loc = "M:\all-labels\" + TRIM(SUBSTRING(move-files.lb, 3)) + ".btw"
        new-loc = "M:\auto-labels\" + TRIM(SUBSTRING(move-files.lb, 3)) + ".btw"
        .
END.


FOR EACH move-files:
/*     DISPLAY move-files WITH 2 COL WIDTH 200. */
    OS-COPY VALUE(old-loc) VALUE(new-loc).
END.


