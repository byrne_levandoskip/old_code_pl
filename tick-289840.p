

DEF TEMP-TABLE found
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD buyer     LIKE symix.ITEM.buyer
    FIELD plan-code LIKE symix.ITEM.plan-code
    .


INPUT FROM M:\import-me.csv.
REPEAT :
    CREATE found.
    IMPORT DELIMITER "," found.
END.
INPUT CLOSE.


OUTPUT TO M:\track-output-289840.csv.
FOR EACH found NO-LOCK WHERE
    found.ITEM <> "":

    FIND FIRST symix.ITEM EXCLUSIVE-LOCK WHERE
        ITEM.ITEM = found.ITEM NO-ERROR.

    IF AVAIL ITEM THEN DO:

        EXPORT DELIMITER ","
            found.ITEM
            ITEM.buyer
            found.buyer
            ITEM.plan-code
            found.plan-code
            .

/*         ASSIGN                                */
/*             ITEM.buyer = found.buyer          */
/*             ITEM.plan-code = found.plan-code. */

    END.
    ELSE DO:
        EXPORT DELIMITER ","
            found.ITEM
            "Not found"
            .
    END.
END.
OUTPUT CLOSE.


