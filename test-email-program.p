





RUN send-email
    ("levandoskip@byrne.com",
     "M:\button.html",
     "THIS IS A TEST 6",
     "Some Email Body Text",
     "type=text/html:charset=us-ascii:filetype=ascii",
     "text",
     "",
     "levandoskip@byrne.com"
    ).

PROCEDURE send-email:

    
    DEF VAR mailserv AS CHAR INIT "smtp_server.byrne-electrical.com".

    DEF VAR e-loops  AS INTE     NO-UNDO.

    DEF VAR mailtox  AS CHAR.

    DEF VAR mailmime AS CHAR INIT "".

    DEF VAR xstatus  AS LOG.
    DEF VAR xmessage AS CHAR.
    DEF VAR myError  AS INT.
    DEF VAR mySite   AS CHAR.
    DEF VAR i        AS INT.
    DEF VAR myDesc   AS CHAR FORMAT "x(40)".
    DEF VAR v-successful AS logi                NO-UNDO.
    DEF INPUT PARAMETER mailto   AS CHAR.
    DEF INPUT PARAMETER mailloc  AS CHAR.
    DEF INPUT PARAMETER mailsubj AS CHAR. 
    DEF INPUT PARAMETER mailbody AS CHAR.
    DEF INPUT PARAMETER mailatt  AS CHAR.
    DEF INPUT PARAMETER mailtype AS CHAR.
    DEF INPUT PARAMETER mailcc   AS CHAR.
    DEF INPUT PARAMETER mailfrom AS CHAR FORMAT "x(70)".

    DO i = 1 TO NUM-ENTRIES(mailto , "," ).
        mailtox = ENTRY(i , mailto).
        xstatus = NO.

        e-loops = 0.

        STAT-LOOP:
        DO WHILE xstatus = NO.
            e-loops = e-loops + 1.
            RUN utilities/smtpmailpub.p (mailserv, mailtox, mailfrom, mailcc, mailatt,
                                     mailloc, mailsubj, mailbody, mailmime,mailtype, 0,NO,"","","",
        OUTPUT xstatus,
        OUTPUT xmessage) NO-ERROR.
            IF e-loops = 20 THEN DO:
                LEAVE STAT-LOOP.
            END.   
        END.
    END.
END PROCEDURE.


