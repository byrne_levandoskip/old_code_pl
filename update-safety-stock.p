


def var hExcel              as com-handle no-undo.
def var hWorkBook           as com-handle no-undo.
def var hWorkSheet          as com-handle no-undo.
def var hWindow             as com-handle no-undo.
def var v-row               as int init 2.
def var v-main              as char.
def var v-be01              as char.
def var v-be02              as char.
def var v-be03              as char.
def var v-be04              as char.
def var v-other             as char.

def temp-table found
    field item              like symix.item.item
    field req-ss            like symix.itemwhse.qty-reorder
/*     field req-ss            as char */
    field row-id            as int
    .

create "Excel.Application" hExcel.
assign
   hWorkBook      = hExcel:WorkBooks:open("M:\SS-change.xlsx")
   hWorkSheet     = hexcel:Sheets:item(1) 
   hWindow        = hExcel:ActiveWindow
   hexcel:visible = false.  


do while(trim(hWorkSheet:Cells(v-row,1):value) <> ? AND v-row < 3000):

    create found.
    assign
        found.item = trim(hWorkSheet:Cells(v-row,1):value)
        found.item = if index(found.item, ".") > 0 and not found.item begins "SA" then substring(found.item, 1, index(found.item, ".") - 1) else found.item 
        found.req-ss = int(trim(hWorkSheet:Cells(v-row,2):value))
/*         found.req-ss = trim(hWorkSheet:Cells(v-row,2):value)                    */
/*         found.req-ss = substring(found.req-ss, 1, index(found.req-ss, ".") - 1) */
        v-row = v-row + 1
        .
end.

hWorkBook:close("M:\SS-change.xlsx") no-error.
hExcel:quit() no-error.
release object hExcel no-error.
release object hWorkBook no-error.
release object hWorkSheet no-error.
release object hWindow no-error.


output to M:\update-safety-stock_03-22-2020.txt.
export delimiter "|"
    "Status"
    "Item"
    "New SS"
    "Main SS"
    "BE01 SS"
    "BE02 SS"
    "BE03 SS"
    "BE04 SS"
    "Other SS"
    .
for each found no-lock:
    assign
        v-main = ""
        v-be01 = ""
        v-be02 = ""
        v-be03 = ""
        v-be04 = ""
        v-other = ""
        .

    find first symix.item no-lock where
        item.item = found.item no-error.

    if not avail item then do:
        export delimiter "|"
            "Item Not Found"
            found.item
            found.req-ss
            v-main
            v-be01
            v-be02
            v-be03
            v-be04
            v-other
            .
        next.
    end.

    find first symix.itemwhse no-lock where
        itemwhse.item = found.item and
        itemwhse.whse = "MAIN" no-error.
    if avail itemwhse then
        assign v-main = string(itemwhse.qty-reorder).
    else
        assign v-main = "X".

    find first symix.itemwhse no-lock where
        itemwhse.item = found.item and
        itemwhse.whse = "BE01" no-error.
    if avail itemwhse then
        assign v-be01 = string(itemwhse.qty-reorder).
    else
        assign v-be01 = "X".

    find first symix.itemwhse no-lock where
        itemwhse.item = found.item and
        itemwhse.whse = "BE02" no-error.
    if avail itemwhse then
        assign v-be02 = string(itemwhse.qty-reorder).
    else
        assign v-be02 = "X".

    find first symix.itemwhse no-lock where
        itemwhse.item = found.item and
        itemwhse.whse = "BE03" no-error.
    if avail itemwhse then
        assign v-be03 = string(itemwhse.qty-reorder).
    else
        assign v-be03 = "X".

    find first symix.itemwhse no-lock where
        itemwhse.item = found.item and
        itemwhse.whse = "BE04" no-error.
    if avail itemwhse then
        assign v-be04 = string(itemwhse.qty-reorder).
    else
        assign v-be04 = "X".

    for each symix.itemwhse no-lock where
        itemwhse.item = found.item and
        itemwhse.whse <> "MAIN" and
        itemwhse.whse <> "BE01" and
        itemwhse.whse <> "BE02" and
        itemwhse.whse <> "BE03" and
        itemwhse.whse <> "BE04":
        assign v-other = string(int(v-other) + itemwhse.qty-reorder).
    end.


    export delimiter "|"
        "No Change ATM"
        found.item
        found.req-ss
        v-main
        v-be01
        v-be02
        v-be03
        v-be04
        v-other
        .
end.
output close.



