

DEF VAR c AS INT.

DEF TEMP-TABLE sa-found
    FIELD ITEM      LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE auto-parts
    FIELD fg-item   LIKE symix.ITEM.ITEM
    FIELD sa-item   LIKE symix.ITEM.ITEM
    .

DEF TEMP-TABLE part-label
    FIELD fg-item   LIKE symix.ITEM.ITEM
    FIELD lb-item   LIKE symix.ITEM.ITEM
    .

OUTPUT TO M:\auto-sa-parts.csv.
FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM BEGINS "08065" OR
    ITEM.ITEM BEGINS "08498" OR
    ITEM.ITEM BEGINS "08316" OR
    ITEM.ITEM BEGINS "08160" OR
    ITEM.ITEM BEGINS "08170" OR
    ITEM.ITEM BEGINS "98498" OR
    ITEM.ITEM BEGINS "98065":

    CREATE sa-found.
    ASSIGN sa-found.ITEM = ITEM.ITEM.
    EXPORT DELIMITER "," sa-found.
END.
OUTPUT CLOSE.

OUTPUT TO M:\auto-fg-parts.csv.
FOR EACH sa-found NO-LOCK,
    EACH symix.jobmatl NO-LOCK WHERE
    jobmatl.ITEM = sa-found.ITEM,
    FIRST symix.job NO-LOCK OF symix.jobmatl WHERE
    job.TYPE = "s":

    CREATE auto-parts.
    ASSIGN
        auto-parts.fg-item = job.ITEM
        auto-parts.sa-item = sa-found.ITEM.
    EXPORT DELIMITER ","
        auto-parts.fg-item
        auto-parts.sa-item
        .
END.
OUTPUT CLOSE.


OUTPUT TO M:\auto-part-labels.csv.
FOR EACH auto-parts NO-LOCK WHERE
    auto-parts.fg-item <> "",
    FIRST symix.job NO-LOCK WHERE
    job.ITEM = auto-parts.fg-item AND
    job.TYPE = "S",
    EACH symix.jobmatl NO-LOCK OF symix.job WHERE
    jobmatl.ITEM BEGINS "LB":

    CREATE part-label.
    ASSIGN
        part-label.fg-item = auto-parts.fg-item
        part-label.lb-item = jobmatl.ITEM.
    EXPORT DELIMITER ","
        part-label.fg-item
        part-label.lb-item
        .
END.
OUTPUT CLOSE.
