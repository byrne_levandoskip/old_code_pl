


DEFINE TEMP-TABLE found
    FIELD ITEM  LIKE symix.ITEM.ITEM.


INPUT FROM M:\delete-me.csv.
REPEAT:
    CREATE found.
    IMPORT DELIMITER "," found.
END.


OUTPUT TO M:\item-table-deletes.csv.
FOR EACH symix.ITEM EXCLUSIVE-LOCK WHERE
    ITEM.ITEM <> "" AND
    CAN-FIND(FIRST found WHERE
             found.ITEM = ITEM.ITEM):

    EXPORT DELIMITER ","
        ITEM.
    DELETE ITEM.
END.
OUTPUT CLOSE.

OUTPUT TO M:\itemloc-table-deletes.csv.
FOR EACH symix.itemloc EXCLUSIVE-LOCK WHERE
    itemloc.ITEM <> "" AND
    CAN-FIND(FIRST found WHERE
             found.ITEM = itemloc.ITEM):

    EXPORT DELIMITER ","
        itemloc.
    DELETE itemloc.
END.
OUTPUT CLOSE.

OUTPUT TO M:\itemwhse-table-deletes.csv.
FOR EACH symix.itemwhse EXCLUSIVE-LOCK WHERE
    itemwhse.ITEM <> "" AND
    CAN-FIND(FIRST found WHERE
             found.ITEM = itemwhse.ITEM):

    EXPORT DELIMITER ","
        itemwhse.
    DELETE itemwhse.
END.
OUTPUT CLOSE.

OUTPUT TO M:\item-glbl-table-deletes.csv.
FOR EACH symglbl.item-glbl EXCLUSIVE-LOCK WHERE
    item-glbl.ITEM <> "" AND
    CAN-FIND(FIRST found WHERE
             found.ITEM = item-glbl.ITEM):

    EXPORT DELIMITER ","
        item-glbl.
    DELETE item-glbl.
END.
OUTPUT CLOSE.
