

DEF VAR hExcel         AS COM-HANDLE NO-UNDO.
DEF VAR hWorkBook      AS COM-HANDLE NO-UNDO.
DEF VAR hWorkSheet     AS COM-HANDLE NO-UNDO.
DEF VAR hWindow        AS COM-HANDLE NO-UNDO.
DEF VAR v-row          AS INT INIT 1.
DEF VAR v-item         LIKE symix.ITEM.ITEM.
DEF VAR v-plus-code    AS CHAR.
DEF VAR v-count        AS INT.


CREATE "Excel.Application" hExcel.
ASSIGN
   hWorkBook      = hExcel:WorkBooks:OPEN("M:\Possible Proprietary Containing SA04399.xlsx")
/*    hWorkBook      = hExcel:WorkBooks:OPEN("M:\Proprietary Containing SA04399.xlsx") */
   hWorkSheet     = hexcel:Sheets:ITEM(1) 
   hWindow        = hExcel:ActiveWindow
   hexcel:VISIBLE = FALSE.  /* skip first row - headers */

DO v-row = 2 TO 3254:
/* DO v-row = 2 TO 323: */

    ASSIGN
        v-item  = TRIM(STRING(hWorkSheet:Cells(v-row,1):VALUE))
        v-plus-code = SUBSTRING(v-item, INDEX(v-item, "+") + 1)
        .

    FIND FIRST symix.customer NO-LOCK WHERE
        customer.cust-num = v-plus-code NO-ERROR.

    ASSIGN
        hWorkSheet:Cells(v-row,2):VALUE = v-plus-code
        hWorkSheet:Cells(v-row,3):VALUE = IF AVAIL customer THEN customer.slsman ELSE ""
        .

END.
OUTPUT CLOSE.

hWorkBook:CLOSE("M:\Possible Proprietary Containing SA04399.xlsx") NO-ERROR.
/* hWorkBook:CLOSE("M:\Proprietary Containing SA04399.xlsx") NO-ERROR. */
hExcel:QUIT() NO-ERROR.
RELEASE OBJECT hExcel NO-ERROR.
RELEASE OBJECT hWorkBook NO-ERROR.
RELEASE OBJECT hWorkSheet NO-ERROR.
RELEASE OBJECT hWindow NO-ERROR.



