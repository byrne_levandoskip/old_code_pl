

OUTPUT TO M:\table-check.csv.
FOR EACH symix.customer NO-LOCK WHERE
    customer.cust-num <> "":

    FIND FIRST symix.ux-customer NO-LOCK OF symix.customer NO-ERROR.
    IF NOT AVAIL ux-customer THEN DO:
        EXPORT DELIMITER ","
            "ux-customer"
            customer.cust-num
            customer.cust-seq.
        CREATE symix.ux-customer.
        ASSIGN
            ux-customer.cust-num = customer.cust-num
            ux-customer.cust-seq = customer.cust-seq.
    END.


    FIND FIRST symglbl.ux-custaddr NO-LOCK OF symglbl.custaddr NO-ERROR.
    IF NOT AVAIL ux-custaddr THEN DO:

        EXPORT DELIMITER ","
            "ux-custaddr"
            customer.cust-num
            customer.cust-seq.
        CREATE symglbl.ux-custaddr.
        ASSIGN
            ux-custaddr.cust-num = customer.cust-num
            ux-custaddr.cust-seq = customer.cust-seq.
    END.
END.
