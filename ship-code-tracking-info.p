
   
DEF TEMP-TABLE found
    FIELD ship-code     LIKE symix.co.ship-code
    FIELD DESCRIPTION   LIKE symix.shipcode.DESCRIPTION
    FIELD c-pos         AS INT
    FIELD c-neg         AS INT.

OUTPUT TO M:\coitem-ups-export.csv.
FOR EACH symix.co NO-LOCK WHERE
    co.order-date GE 01/01/17,
    FIRST symix.coitem NO-LOCK OF symix.co WHERE
    coitem.qty-shipped GT 0:


    FIND FIRST found EXCLUSIVE-LOCK WHERE
        found.ship-code = co.ship-code NO-ERROR.
    
    IF NOT AVAIL found THEN DO:
        FIND FIRST symix.shipcode NO-LOCK WHERE
            shipcode.ship-code = co.ship-code NO-ERROR.
        CREATE found.
        ASSIGN 
            found.ship-code = co.ship-code
            found.DESCRIPTION = shipcode.DESCRIPTION
            .
    END.

    
    FIND FIRST symix.be-coitem-ups WHERE
        be-coitem-ups.co-num BEGINS TRIM(co.co-num) NO-ERROR.

    IF AVAIL be-coitem-ups THEN DO:
        EXPORT DELIMITER ","
            be-coitem-ups.
        ASSIGN found.c-pos = found.c-pos + 1.
    END.
    ELSE DO:
        ASSIGN found.c-neg = found.c-neg + 1.
    END.
END.
OUTPUT CLOSE.


OUTPUT TO M:\shipvias-with-tracking.csv.
EXPORT DELIMITER ","
    "Ship Code"
    "Description"
    "# has Tracking"
    "# w/o Tracking".

FOR EACH found:
    EXPORT DELIMITER ","
        found.
END.
OUTPUT CLOSE.
