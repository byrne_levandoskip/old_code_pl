


DEF TEMP-TABLE found
    FIELD v-year        AS INT
    FIELD v-month       AS INT
    FIELD t-count       AS INT
    FIELD t-plan        AS INT
    FIELD t-ord         AS INT
    FIELD t-stop        AS INT
    FIELD t-comp        AS INT
    .


FOR EACH symix.co NO-LOCK:

    FIND FIRST found EXCLUSIVE-LOCK WHERE
        found.v-year = YEAR(co.order-date) AND 
        found.v-month = MONTH(co.order-date) NO-ERROR.

    IF NOT AVAIL found THEN DO:
        CREATE found.
        ASSIGN
            found.v-year = YEAR(co.order-date)
            found.v-month = MONTH(co.order-date)
            .
    END.

    ASSIGN
        found.t-count = t-count + 1
        found.t-plan = IF co.stat = "P" THEN t-plan + 1 ELSE t-plan
        found.t-ord = IF co.stat = "O" THEN t-ord + 1 ELSE t-ord
        found.t-stop = IF co.stat = "S" THEN t-stop + 1 ELSE t-stop
        found.t-comp = IF co.stat = "C" THEN t-comp + 1 ELSE t-comp
        .
END.


OUTPUT TO M:\status-results.csv.
EXPORT DELIMITER ","
    "Year"
	"Month"
    "Total Ords"
    "Planned"
    "Ordered"
    "Stopped"
    "Complete"
    .
FOR EACH found:
    EXPORT DELIMITER "," found.
END.
OUTPUT CLOSE.
