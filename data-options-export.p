
DEF BUFFER b-item FOR symix.ITEM.
DEF BUFFER b-ux-item FOR symix.ux-item.

OUTPUT TO M:\data-options-export_v2.txt.
EXPORT DELIMITER "|"
    "Date Kit"
    "Data Description"
    "Component"
    "Component BOM Qty"
    "Component BOM UM"
    "Component Description"
    "Component Product Code"
    "Component Long Description"
    .

FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.ITEM BEGINS "Data Option",
    FIRST symix.job NO-LOCK WHERE
    job.job = ITEM.job AND
    job.suffix = 000,
    EACH symix.jobmatl NO-LOCK OF symix.job,
    FIRST b-item NO-LOCK WHERE
    b-item.ITEM = jobmatl.ITEM,
    FIRST b-ux-item NO-LOCK OF b-item:

    EXPORT DELIMITER "|"
        ITEM.ITEM
        ITEM.DESCRIPTION
        b-item.ITEM
        jobmatl.matl-qty
        jobmatl.u-m
        b-item.DESCRIPTION
        REPLACE(REPLACE(b-ux-item.uf-kit-description,CHR(13),''),CHR(10),'')
        .
END.

