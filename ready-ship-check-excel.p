


DEF VAR hExcel         AS COM-HANDLE NO-UNDO.
DEF VAR hWorkBook      AS COM-HANDLE NO-UNDO.
DEF VAR hWorkSheet     AS COM-HANDLE NO-UNDO.
DEF VAR hWindow        AS COM-HANDLE NO-UNDO.
DEF VAR v-row          AS INT INIT 2.
DEF VAR v-po-line      LIKE symix.coitem.co-line.
DEF VAR v-po           like symix.co.cust-po.


CREATE "Excel.Application" hExcel.
ASSIGN
   hWorkBook      = hExcel:WorkBooks:OPEN("M:\natoff-open-pos.xls")
   hWorkSheet     = hexcel:Sheets:ITEM(1) 
   hWindow        = hExcel:ActiveWindow
   hexcel:VISIBLE = FALSE.  /* skip first row - headers */

output to M:\ready-ship-check.txt.
DO v-row = 2 TO 172:

    ASSIGN 
        v-po = STRING(hWorkSheet:Cells(v-row,1):VALUE)
        v-po-line = hWorkSheet:Cells(v-row,2):VALUE
        .

    if index(v-po, ".") > 0 then
        assign v-po = substring(v-po, 1, index(v-po, ".") - 1).

    export delimiter "|" v-po v-po-line. 

    FIND FIRST symix.co NO-LOCK WHERE
        co.cust-po = v-po AND
        co.cust-num = "Natoff" NO-ERROR.

    FIND FIRST symix.ux-coitem NO-LOCK OF symix.co WHERE
        ux-coitem.co-line = v-po-line NO-ERROR. 

    IF AVAIL ux-coitem THEN DO:
        assign
            hWorkSheet:Cells(v-row,5):VALUE = ux-coitem.uf-date-label
            hWorkSheet:Cells(v-row,6):VALUE = STRING(ux-coitem.uf-time-label, "HH:MM:SS")
            hWorkSheet:Cells(v-row,7):VALUE = ux-coitem.uf-readyship-salespo
            hWorkSheet:Cells(v-row,8):VALUE = ux-coitem.uf-readyship-addr1
            hWorkSheet:Cells(v-row,9):VALUE = ux-coitem.uf-readyship-addr2
            hWorkSheet:Cells(v-row,10):VALUE = ux-coitem.uf-readyship-addr3
            hWorkSheet:Cells(v-row,11):VALUE = ux-coitem.uf-readyship-addr4
            .
    END.
    ELSE DO:
        EXPORT DELIMITER "|" "Not Found: " v-po.
    END.
END.
output close.


hWorkBook:CLOSE("M:\natoff-open-pos.xls") NO-ERROR.
hExcel:QUIT() NO-ERROR.
RELEASE OBJECT hExcel NO-ERROR.
RELEASE OBJECT hWorkBook NO-ERROR.
RELEASE OBJECT hWorkSheet NO-ERROR.
RELEASE OBJECT hWindow NO-ERROR.








