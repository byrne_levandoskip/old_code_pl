


DEF VAR hExcel         AS COM-HANDLE NO-UNDO.
DEF VAR hWorkBook      AS COM-HANDLE NO-UNDO.
DEF VAR hWorkSheet     AS COM-HANDLE NO-UNDO.
DEF VAR hWindow        AS COM-HANDLE NO-UNDO.
DEF VAR v-row          AS INT INIT 2.
DEF VAR v-parcel       like symix.item.item.

def temp-table parcel-item
    field parcel        like symix.item.item
    field item          like symix.item.item
    field src           as char
    .

def temp-table item-pack-factor
    field item          like symix.item.item
    field cust-num      like symix.customer.cust-num
    field min-qty       like symix.be-pack-factor.min-qty
    field max-qty       like symix.be-pack-factor.max-qty
    .


CREATE "Excel.Application" hExcel.
ASSIGN
   hWorkBook      = hExcel:WorkBooks:OPEN("M:\F Height final for IT and Byrne labels.xlsx")
   hWorkSheet     = hexcel:Sheets:ITEM(1) 
   hWindow        = hExcel:ActiveWindow
   hexcel:VISIBLE = FALSE.  /* skip first row - headers */

output to M:\pack-factor-update-import-log.txt.
DO v-row = 7 TO 47:

    ASSIGN v-parcel = STRING(hWorkSheet:Cells(v-row,1):VALUE).

    if index(v-parcel, ".") > 0 then
        assign v-parcel = substring(v-parcel, 1, index(v-parcel, ".") - 1).

    export delimiter "|" v-parcel.

    for each symix.itemcust no-lock where
        itemcust.cust-item = v-parcel and
        itemcust.cust-num = "STEEL":

        find first parcel-item no-lock where
            parcel-item.item = itemcust.item no-error.

        if not avail parcel-item then do:
            create parcel-item.
            assign
                parcel-item.item = itemcust.item
                parcel-item.parcel = v-parcel
                parcel-item.src = "itemcust".
            export delimiter "|" parcel-item.
        end.
    end.    

    for each symix.item no-lock where
        item.item begins v-parcel and
        item.item matches "*+STEEL*":

        find first parcel-item no-lock where
            parcel-item.item = item.item no-error.

        if not avail parcel-item then do:
            create parcel-item.
            assign
                parcel-item.item = item.item
                parcel-item.parcel = v-parcel
                parcel-item.src = "item".
            export delimiter "|" parcel-item.
        end.
    end. 
END.
output close.

output to M:\current-pack-factor.csv.
export delimiter ","
    "Item"
    "Min Qty"
    "Max Qty"
    "Style"
    "Item Weight"
    "Carton Weight"
    "Carton Vendor Ref"
    "Carton Length"
    "Carton Width"
    "Carton Height"
    "Force Carton"
    "Pack Volume"
    "Customer"
    .
output close.

output to M:\new-f-height-pack-factor.csv.
export delimiter ","
    "Item"
    "Min Qty"
    "Max Qty"
    "Style"
    "Item Weight"
    "Carton Weight"
    "Carton Vendor Ref"
    "Carton Length"
    "Carton Width"
    "Carton Height"
    "Force Carton"
    "Pack Volume"
    "Customer"
    .
output close.

for each parcel-item no-lock where
    parcel-item.parcel <> "":

    output to M:\current-pack-factor.csv append.
    for each symix.be-pack-factor exclusive-lock where
        be-pack-factor.item = parcel-item.item and
        be-pack-factor.cust-num = "STEEL":
        export delimiter "," be-pack-factor.
/*         delete be-pack-factor. */
    end.
    output close.


/*     output to M:\new-f-height-pack-factor.csv append. */
/*                                                       */
/*     create symix.be-pack-factor.                      */
/*     assign                                            */
/*         be-pack-factor.item = parcel-item.item        */
/*         be-pack-factor.cust-num = "STEEL"             */
/*         be-pack-factor.min-qty = 1                    */
/*         be-pack-factor.min-qty = 10                   */
/*         .                                             */
/*                                                       */
/*     export delimiter ","                              */
/*         be-pack-factor.                               */
/*                                                       */
/*     create symix.be-pack-factor.                      */
/*     assign                                            */
/*         be-pack-factor.item = parcel-item.item        */
/*         be-pack-factor.cust-num = "STEEL"             */
/*         be-pack-factor.min-qty = 11                   */
/*         be-pack-factor.min-qty = 20                   */
/*         .                                             */
/*                                                       */
/*     export delimiter ","                              */
/*         be-pack-factor.                               */
/*                                                       */
/*     create symix.be-pack-factor.                      */
/*     assign                                            */
/*         be-pack-factor.item = parcel-item.item        */
/*         be-pack-factor.cust-num = "STEEL"             */
/*         be-pack-factor.min-qty = 21                   */
/*         be-pack-factor.min-qty = 21                   */
/*         .                                             */
/*                                                       */
/*     export delimiter ","                              */
/*         be-pack-factor.                               */
/*                                                       */
/*     output close.                                     */

end.


hWorkBook:CLOSE("M:\F Height final for IT and Byrne labels.xlsx") NO-ERROR.
hExcel:QUIT() NO-ERROR.
RELEASE OBJECT hExcel NO-ERROR.
RELEASE OBJECT hWorkBook NO-ERROR.
RELEASE OBJECT hWorkSheet NO-ERROR.
RELEASE OBJECT hWindow NO-ERROR.






