
/* Author: Paul Levandoski */
/* IT Ticket Request #21907 */

DEF VAR t-price    AS DEC INITIAL 0.0.
DEF VAR t-due-date AS DATE.

OUTPUT TO VALUE("M:\test\open_cia.csv").
EXPORT DELIMITER "," 
    "Customer" 
    "Order" 
    "Order Date" 
    "Due Date" 
    "Terms"
    "Order Price".

FOR EACH byrne.co NO-LOCK WHERE
    co.terms-cod = "cia" OR co.terms-cod = "ncc"
        BREAK BY co.cust-num:

    FOR EACH byrne.coitem OF byrne.co NO-LOCK WHERE
        coitem.stat = "o" OR coitem.stat = "p" OR coitem.stat = "s":

        t-price = t-price + (coitem.qty-ordered * coitem.price).
        t-due-date = coitem.due-date.
    END.

    IF t-price <> 0.0 THEN
        EXPORT DELIMITER ","
            co.cust-num
            co.co-num
            co.order-date
            t-due-date
            co.terms
            t-price
            .

    t-price = 0.0.

END. /* each co */


