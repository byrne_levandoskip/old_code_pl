


DEF TEMP-TABLE found-item 
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD lb1       AS LOG INITIAL FALSE
    FIELD lb2       AS LOG INITIAL FALSE
    .



FOR EACH symix.job NO-LOCK WHERE
    job.TYPE = "S",
    EACH symix.jobmatl NO-LOCK OF symix.job WHERE
    jobmatl.ITEM = "LB3400494" OR
    jobmatl.ITEM = "LB3400505":


    FIND FIRST found-item EXCLUSIVE-LOCK WHERE
        found-item.ITEM = job.ITEM NO-ERROR.

    IF NOT AVAIL found-item THEN DO:

        CREATE found-item.
        ASSIGN found-item.ITEM = job.ITEM.

        IF jobmatl.ITEM = "LB3400494" THEN
            ASSIGN found-item.lb1 = TRUE.
        IF jobmatl.ITEM = "LB3400505" THEN
            ASSIGN found-item.lb2 = TRUE.

    END.
    ELSE IF AVAIL found-item AND 
        jobmatl.ITEM = "LB3400494" AND 
        found-item.lb1 = FALSE THEN DO:
        ASSIGN found-item.lb1 = TRUE.
    END.
    ELSE IF AVAIL found-item AND 
        jobmatl.ITEM = "LB3400505" AND 
        found-item.lb2 = FALSE THEN DO:
        ASSIGN found-item.lb2 = TRUE.
    END.                     
END.


OUTPUT TO M:\find-orders-by-labels.csv.
EXPORT DELIMITER ","
    "Order Num"
    "Order Line"
    "Item"
    "Due Date"
    "Contains LB 494"
    "Contains LB 505"
    .

FOR EACH symix.coitem NO-LOCK WHERE
    coitem.due-date GE 07/01/16,
    FIRST found-item NO-LOCK WHERE
    found-item.ITEM = coitem.ITEM:

    EXPORT DELIMITER ","
        coitem.co-num
        coitem.co-line
        coitem.ITEM
        coitem.due-date
        found-item.lb1
        found-item.lb2
        .
END.
OUTPUT CLOSE.
