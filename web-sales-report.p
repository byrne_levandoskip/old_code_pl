



DEFINE TEMP-TABLE found NO-UNDO
    FIELD comp    LIKE symcust.custaddr.NAME    
    FIELD nam     LIKE symix.customer.contact[1]
    FIELD email   LIKE symix.ux-customer.uf-acct-email
    FIELD ord     LIKE symix.co.co-num
    FIELD dat-o   LIKE symix.co.order-date
    .


RUN find-stuff.
RUN output-stuff.


PROCEDURE find-stuff:

    FOR EACH symix.co NO-LOCK WHERE
        co.taken-by = "EDI-WEB" AND 
        co.order-date GE 07/01/12 AND
        co.order-date LE 07/31/13,
        FIRST symix.customer NO-LOCK WHERE
        customer.cust-num = co.cust-num,
        FIRST symix.ux-customer NO-LOCK OF symix.customer,
        FIRST symcust.custaddr NO-LOCK OF symix.customer:
    
        CREATE found.
        ASSIGN
            found.comp  = custaddr.NAME   
            found.nam   = customer.contact[1]
            found.email = ux-customer.uf-acct-email
            found.ord   = co.co-num
            found.dat-o = co.order-date
            .
    
    END.
END.


PROCEDURE output-stuff:

    DEF VAR fname  AS CHAR FORMAT "x(20)".
    DEF VAR lname  AS CHAR FORMAT "x(20)".

    OUTPUT TO M:\web-sales-report.csv.
    EXPORT DELIMITER ","
        "Company"
        "First Name"
        "Last Name"
        "Email"
        "Order"
        "Order Date"
        .
    FOR EACH found NO-LOCK:

        IF INDEX(TRIM(found.nam), " ") <> 0 THEN DO:
            ASSIGN
                fname = SUBSTRING(TRIM(found.nam), 1, INDEX(TRIM(found.nam), " ")).
                lname = SUBSTRING(TRIM(found.nam), INDEX(TRIM(found.nam), " ")).
                .
        END.
        ELSE DO:
            ASSIGN
                fname = found.nam
                lname = ""
                .
        END.


        EXPORT DELIMITER ","
            found.comp
            fname
            lname
            found.email
            found.ord
            found.dat-o
            .
    END.
END.



