


DEF TEMP-TABLE found 
    FIELD SA-part   LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE found-part
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD SA-part   LIKE symix.ITEM.ITEM.


DEF TEMP-TABLE found-use
    FIELD cust-num      LIKE symix.customer.cust-num
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD sa-part       LIKE symix.ITEM.ITEM
    FIELD qty-ship-tot  AS INT
    FIELD num-ord-tot   AS INT
    FIELD qty-ship-2014 AS INT
    FIELD num-ord-2014  AS INT
    FIELD qty-ship-2015 AS INT
    FIELD num-ord-2015  AS INT
    FIELD qty-ship-2016 AS INT
    FIELD num-ord-2016  AS INT
    .

INPUT FROM M:\list-of-parts.txt.
REPEAT:
    CREATE found.
    IMPORT DELIMITER ","
        found.
    found.sa-part = TRIM(found.sa-part).
END.
INPUT CLOSE.



FOR EACH found NO-LOCK:
    
    FOR EACH symix.jobmatl NO-LOCK WHERE
        jobmatl.ITEM = found.SA-part,
        FIRST symix.job NO-LOCK OF symix.jobmatl WHERE
        job.TYPE = "s":

        FIND FIRST found-part NO-LOCK WHERE
            found-part.ITEM = job.ITEM NO-ERROR.

        IF NOT AVAIL found-part THEN DO:
            CREATE found-part.
            ASSIGN
                found-part.ITEM = job.ITEM
                found-part.SA-part = found.SA-part
                .
        END.
    END.
END.


FOR EACH found-part NO-LOCK,
    EACH symix.coitem NO-LOCK WHERE
    coitem.ITEM = found-part.ITEM AND
    coitem.qty-shipped GT 0 AND
    coitem.due-date GE 01/01/14 AND
    coitem.due-date LE 12/31/16,
    FIRST symix.co NO-LOCK OF symix.coitem:

    FIND FIRST found-use EXCLUSIVE-LOCK WHERE
        found-use.ITEM = found-part.ITEM AND
        found-use.cust-num = co.cust-num NO-ERROR.

    IF NOT AVAIL found-use THEN DO:
        CREATE found-use.
        ASSIGN
            found-use.ITEM = found-part.ITEM
            found-use.sa-part = found-part.sa-part
            found-use.cust-num = co.cust-num
            .
    END.

    ASSIGN
        found-use.qty-ship-tot = found-use.qty-ship-tot + coitem.qty-shipped
        found-use.num-ord-tot = found-use.num-ord-tot + 1
        .

    IF YEAR(coitem.due-date) = 2014 THEN DO:
        ASSIGN
            found-use.qty-ship-2014 = found-use.qty-ship-2014 + coitem.qty-shipped
            found-use.num-ord-2014 = found-use.num-ord-2014 + 1
            .
    END.
    ELSE IF YEAR(coitem.due-date) = 2015 THEN DO:
        ASSIGN
            found-use.qty-ship-2015 = found-use.qty-ship-2015 + coitem.qty-shipped
            found-use.num-ord-2015 = found-use.num-ord-2015 + 1
            .
    END.
    ELSE IF YEAR(coitem.due-date) = 2016 THEN DO:
        ASSIGN
            found-use.qty-ship-2016 = found-use.qty-ship-2016 + coitem.qty-shipped
            found-use.num-ord-2016 = found-use.num-ord-2016 + 1
            .
    END.


END.


OUTPUT TO M:\stuff.csv.
EXPORT DELIMITER ","
    "Customer"
    "Item"
    "Sub Assm"
    "Total Qty Shipped"
    "Total Num of POs"
    "Qty Shipped 2014"
    "Num of POs 2014"
    "Qty Shipped 2015"
    "Num of POs 2015"
    "Qty Shipped 2016"
    "Num of POs 2016"
    .
FOR EACH found-use NO-LOCK WHERE
    found-use.num-ord-tot <> 0:
    EXPORT DELIMITER ","
        found-use
        .
END.
OUTPUT CLOSE.
