
OUTPUT TO M:\test\old-do-hdr.csv.
EXPORT DELIMITER ","
    "do-hdr.invoicee-name"
    "do-hdr.invoicee-addr[01]"
    "do-hdr.invoicee-addr[02]"
    "do-hdr.invoicee-addr[03]"
    "do-hdr.invoicee-addr[04]"
    "do-hdr.invoicee-city"
    "do-hdr.invoicee-contact"
    "do-hdr.invoicee-country"
    "do-hdr.invoicee-county"
    "do-hdr.invoicee-fax"
    "do-hdr.invoicee-name"
    "do-hdr.invoicee-phone"
    "do-hdr.invoicee-state"
    "do-hdr.invoicee-zip"
    .
    
FOR EACH symix.do-hdr NO-LOCK WHERE
    do-hdr.consignee-name BEGINS "PAOLI" AND  
    do-hdr.invoicee-name BEGINS "UNISHIPPERS":

    EXPORT DELIMITER "," 
        do-hdr.invoicee-name
        do-hdr.invoicee-addr[01]
        do-hdr.invoicee-addr[02]
        do-hdr.invoicee-addr[03]
        do-hdr.invoicee-addr[04]
        do-hdr.invoicee-city
        do-hdr.invoicee-contact
        do-hdr.invoicee-country
        do-hdr.invoicee-county
        do-hdr.invoicee-fax
        do-hdr.invoicee-name
        do-hdr.invoicee-phone
        do-hdr.invoicee-state
        do-hdr.invoicee-zip 
        .

    EXPORT DELIMITER "," 
        do-hdr.consignee-name
        do-hdr.consignee-addr[01]
        do-hdr.consignee-addr[02]
        do-hdr.consignee-addr[03]
        do-hdr.consignee-addr[04]
        do-hdr.consignee-city
        do-hdr.consignee-contact
        do-hdr.consignee-country
        do-hdr.consignee-county
        do-hdr.consignee-fax
        do-hdr.consignee-name
        do-hdr.consignee-phone
        do-hdr.consignee-state
        do-hdr.consignee-zip 
        .
END.




FOR EACH symix.do-hdr EXCLUSIVE-LOCK WHERE
    do-hdr.consignee-name BEGINS "PAOLI" AND  
    do-hdr.invoicee-name BEGINS "UNISHIPPERS":
    

    ASSIGN
        do-hdr.invoicee-name     = do-hdr.consignee-name
        do-hdr.invoicee-addr[01] = do-hdr.consignee-addr[01]
        do-hdr.invoicee-addr[02] = do-hdr.consignee-addr[02]
        do-hdr.invoicee-addr[03] = do-hdr.consignee-addr[03]
        do-hdr.invoicee-addr[04] = do-hdr.consignee-addr[04]
        do-hdr.invoicee-city     = do-hdr.consignee-city
        do-hdr.invoicee-contact  = do-hdr.consignee-contact
        do-hdr.invoicee-country  = do-hdr.consignee-country
        do-hdr.invoicee-county   = do-hdr.consignee-county
        do-hdr.invoicee-fax      = do-hdr.consignee-fax
        do-hdr.invoicee-name     = do-hdr.consignee-name
        do-hdr.invoicee-phone    = do-hdr.consignee-phone
        do-hdr.invoicee-state    = do-hdr.consignee-state
        do-hdr.invoicee-zip      = do-hdr.consignee-zip 
        .

    DISPLAY 
        do-hdr.invoicee-name
        do-hdr.invoicee-addr[01]
        do-hdr.invoicee-addr[02]
        do-hdr.invoicee-addr[03]
        do-hdr.invoicee-addr[04]
        do-hdr.invoicee-city
        do-hdr.invoicee-contact
        do-hdr.invoicee-country
        do-hdr.invoicee-county
        do-hdr.invoicee-fax
        do-hdr.invoicee-name
        do-hdr.invoicee-phone
        do-hdr.invoicee-state
        do-hdr.invoicee-zip 
        WITH 2 COL.
END.
