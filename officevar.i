/*
    tNexus Software, Inc.
    199 N. Main Street -- Suite B5
    Plymouth, MI 48170
    1-800-500-7479 -- Voice
    1-734-453-3101 -- Office
    
    1-734-812-9532 -- Mobile
    
    Nexus@nexussoftware.net
    
    Carl Peterson
    CPeterson@nexussoftware.net.
    
    officevar.i 
    
*/
/* variables for Excel File */
DEFINE VAR ct                           AS INTE LABEL "Records" NO-UNDO.
DEFINE STREAM t.
OUTPUT STREAM t TO TERMINAL.
DEF VAR symix_user           AS CHAR FORMAT "x(30)"                      NO-UNDO.
    symix_user = TRIM(USERID("symix")).
DEF VAR c-task_id           AS CHAR FORMAT "x(40)"                      NO-UNDO.
DEFINE VARIABLE printername             AS CHAR FORMAT "x(30)"          NO-UNDO.
printername = SESSION:PRINTER-NAME.
DEFINE VARIABLE sheet-name              AS CHAR FORMAT "x(25)"          NO-UNDO.
DEFINE VARIABLE chExcelApplication      AS COM-HANDLE.
DEFINE VARIABLE chWorkbook              AS COM-HANDLE.
DEFINE VARIABLE chWorksheet             AS COM-HANDLE.
DEFINE VARIABLE chChart                 AS COM-HANDLE.
DEFINE VARIABLE chWorksheetRange        AS COM-HANDLE.

DEFINE VARIABLE chPageSetup             AS COM-HANDLE NO-UNDO.
DEFINE VARIABLE chPicture               AS COM-HANDLE NO-UNDO.
DEFINE VARIABLE chObject                AS COM-HANDLE NO-UNDO.

DEFINE VARIABLE chWordApplication       AS COM-HANDLE NO-UNDO.
DEFINE VARIABLE chWordTemplate          AS COM-HANDLE NO-UNDO.
DEFINE VARIABLE chWordDocument          AS COM-HANDLE NO-UNDO.


DEFINE VARIABLE t-target                AS CHAR FORMAT "x(50)"      NO-UNDO.
DEFINE VARIABLE t-target-pdf            AS CHAR FORMAT "x(50)"      NO-UNDO.
DEFINE VARIABLE t-source                AS CHAR FORMAT "x(50)"      NO-UNDO.

DEFINE VARIABLE t-target2               AS CHAR FORMAT "x(50)"      NO-UNDO.
DEFINE VARIABLE t-target-pdf2           AS CHAR FORMAT "x(50)"      NO-UNDO.
DEFINE VARIABLE t-source2               AS CHAR FORMAT "x(50)"      NO-UNDO.

DEFINE VARIABLE t-picture               AS CHAR FORMAT "x(50)"      NO-UNDO.
DEFINE VARIABLE t-object                AS CHAR FORMAT "x(50)"      NO-UNDO.
DEFINE VARIABLE t-footer                AS CHAR FORMAT "x(50)"      NO-UNDO.
DEFINE VARIABLE iRow                    AS INTE                     NO-UNDO.
DEFINE VARIABLE cRow                    AS CHAR                     NO-UNDO.
DEFINE VARIABLE cRange                  AS CHAR                     NO-UNDO.
DEFINE VARIABLE icolumn                 AS INTE                     NO-UNDO.
DEFINE VARIABLE ccolumn                 AS CHAR                     NO-UNDO.




