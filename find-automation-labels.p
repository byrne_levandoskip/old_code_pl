



DEF TEMP-TABLE found
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD lbl-format    LIKE symix.be-labelfields.lbl-format
    FIELD is-box        AS LOGICAL
    FIELD is-auto       AS LOGICAL
    .


FOR EACH symix.be-automation NO-LOCK,
    EACH symix.job NO-LOCK WHERE
    job.TYPE = "S" AND
    job.ITEM = be-automation.ITEM,
    EACH symix.jobmatl NO-LOCK OF symix.job WHERE
    jobmatl.ITEM BEGINS "LB",
    FIRST symix.be-labelfields NO-LOCK WHERE
    be-labelfields.lbl-format = SUBSTRING(jobmatl.ITEM, 3) + ".btw":

    FIND FIRST found NO-LOCK WHERE
        found.ITEM = jobmatl.ITEM NO-ERROR.

    IF NOT AVAIL found THEN DO:
        CREATE found.
        ASSIGN
            found.ITEM = jobmatl.ITEM
            found.lbl-format = be-labelfields.lbl-format
            found.is-box = be-labelfields.boxlbl-log
            found.is-auto = be-labelfields.automation-log
            .
    END.     
END.


OUTPUT TO M:\lable-fields.csv.
EXPORT DELIMITER ","
    "Item"
    "Label"
    "Box?"
    "Automation?"
    .
FOR EACH found:
    EXPORT DELIMITER ","
        found.
END.
OUTPUT CLOSE.


/* FOR EACH symix.be-labelfields NO-LOCK: */
/*     DISPLAY be-labelfields WITH 2 COL. */
/* END.                                   */