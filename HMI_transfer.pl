## Nightly FTP transfer to HMI
## David Z. Kil
## Modified extensively by Mike Padget
## And then again by Paul Levandoski
## BYR-1065 10/02/12 pl Change location of scripts

use File::Copy;
use Net::SFTP;
use Net::SMTP;

#Log keeping
my $logentry="";
my $logfile="/tmp/HMIAsnLog.txt";

#Mail service
my $mailhost="bet003.byrne-electrical.com";

#sftp  
my $sftpuploaddir =	"/company194/Company194ext/in";
my $sftphost="leopard@server.example.com";
my $file="856.txt";
my $xferdir="/tmp";
my $dirhandle;


## Open Log file
open(LOG, ">>$logfile") or die "Can't open log file";

&timestamp;

## Log that the transfer has started
print LOG "***Transfer initiated*** \n";


## ********************************
## Initiate SFTP transfer
## ********************************

my $sftp = Net::SFTP->new($sftphost) or writelog("Error connecting to $ftphost \n", $logfile);
#No need to log in with pass keys
#$ftp->login($ftpuser, $ftppass) or writelog("Couldn't connect to $ftphost \n", $logfile); 

#Don't think I have to open a certain dir or not
#$sftp->do_opendir($sftpuploaddir) or writelog("Couldn't do_opendir to $sftpuploaddir \n", $logfile);

print LOG "Connected to $sftphost \n", $logfile;
foreach my $file (@ARGV) {
	$sftp->put("$xferdir/$file", "$file") or writelog("Error transfering $file \n");
	print LOG "$file has been appended \n";
}

#$sftp->do_close($dirhandle);
$sftp->quit;


## ********************************
## File Cleanup
## ********************************

## Delete file
foreach my $file (@ARGV)
{
unlink("$xferdir/$file") or writelog("$file could not be deleted", $logfile);
}


## ********************************
## Sucess email
## ********************************

my $smtp = Net::SMTP->new("$mailhost",
				Hello => 'byrne-electrical.com',
				Timeout => 60);
	$smtp->mail("supportcenter\@byrne-electrical.com\n");
	$smtp->recipient("supportcenter\@byrne-electrical.com\n");
	$smtp->data;
	$smtp->datasend("From:  Syteline\@byrne-electrical.com\n");
	$smtp->datasend("To: supportcenter\@byrne-electrical.com\n");
	$smtp->datasend("Subject:  SFTP transfer to $host Successful\n");
	$smtp->datasend("\n");
	$smtp->dataend;
	$smtp->quit;

print LOG "Success email sent \n";

close (LOG);
exit 0;
## ********************************
## Function to create log entries
## ********************************
sub writelog {

	&timestamp;


	my $smtp = Net::SMTP->new("$mailhost",
				Hello => 'byrne-electrical.com',
				Timeout => 60);
	$smtp->mail("supportcenter\@byrne-electrical.com\n");
	$smtp->recipient("supportcenter\@byrne-electrical.com\n");
	$smtp->data;
	$smtp->datasend("From:  Syteline\@byrne-electrical.com\n");
	$smtp->datasend("To: supportcenter\@byrne-electrical.com\n");
	$smtp->datasend("Subject:  FTP transfer Error\n");
	$smtp->datasend("\n");
	$smtp->datasend("FTP Transfer had errors.  Below is the error message.\n");
	$smtp->datasend("$_[0]\n");
	$smtp->dataend;
	$smtp->quit;

	exit -1;
}

## ********************************
## Get Timestamp
##*********************************
sub timestamp {

	## Get the all the values for current time
	(my $Second, my $Minute, my $Hour, my $Day, my $Month, my $Year, my $WeekDay, my $DayOfYear, my $IsDST) =
        	localtime(time); $Year += 1900; $Month += 1;

	my $logentry = sprintf("%04d:%02d:%02d %02d:%02d:%02d $_[0]", $Year,$Month,$Day,$Hour,$Minute,$Second);
	print LOG "$logentry \n"; 
}
