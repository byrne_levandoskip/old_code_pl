
DEF TEMP-TABLE lead-time NO-UNDO
    FIELD a-date        AS DATE /* analyzed date */
    FIELD t-act-ord     AS INT /* total active orders */
    FIELD t-for-ord     AS INT /* total forecast orders */
    FIELD e-act-ord     AS INT /* entered active orders */
    FIELD e-for-ord     AS INT /* entered forecast orders */
    .

DEF VAR sDate       AS DATE. /* Beginning day to be analyzed */
DEF VAR fDate       AS DATE. /* Final day to be analyzed */
DEF VAR cDate       AS DATE. /* Current date being analyzed */
DEF VAR actRange    AS INT INITIAL 21. /* number of days considered active in production */
DEF VAR skpCustLst  AS CHAR FORMAT "x(128)". /* list of customers, co.cust-num, to not be included */
DEF VAR vCount      AS INT INITIAL 0. /* count number of active days */
DEF VAR vLeadTime   AS DEC INITIAL 0.0. /* average lead time */


ASSIGN
    sDate = TODAY - 7
    fDate = TODAY
    skpCustLst = "STEEL"
    cDate = sDate.


REPEAT WHILE (cDate LE fDate):
    CREATE lead-time.
    ASSIGN
        lead-time.a-date = cDate.


    /* find total order information */
    FOR EACH byrne.co NO-LOCK WHERE
        co.order-date LE cDate AND
        co.co-num <> "" AND
        LOOKUP(co.cust-num, skpCustLst) = 0,
        EACH byrne.coitem NO-LOCK OF byrne.co WHERE
        coitem.stat = "O" OR 
        (coitem.stat = "F" AND
         coitem.due-date GE cDate),
        FIRST byrne.coitem-log NO-LOCK OF byrne.coitem:

        IF co.order-date = cDate THEN DO:
            IF coitem.due-date LE co.order-date + actRange OR
                        coitem-log.due-date LE co.order-date + actRange THEN DO:
                
                ASSIGN
                    lead-time.t-act-ord = lead-time.t-act-ord + 1
                    lead-time.e-act-ord = lead-time.e-act-ord + 1
                    .
            END.
            ELSE DO:
                ASSIGN
                    lead-time.t-for-ord = lead-time.t-for-ord + 1
                    lead-time.e-for-ord = lead-time.e-for-ord + 1
                    .
            END.
        END.
        ELSE DO:
            IF coitem.due-date LE co.order-date + actRange OR
                        coitem-log.due-date LE co.order-date + actRange THEN DO:

                ASSIGN
                    lead-time.t-act-ord = lead-time.t-act-ord + 1
                    .
            END.
            ELSE DO:
                ASSIGN
                    lead-time.t-for-ord = lead-time.t-for-ord + 1
                    .
            END.
        END.

    END.    
    ASSIGN
        cDate = cDate + 1.
END.

OUTPUT TO /tmp/lead-time-theory-data_no-steel.csv.
EXPORT DELIMITER ","
    "As Of Date"
    "Total Active Orders"
    "Total Forecast Orders"
    "Entered Active Orders"
    "Entered Forecast Orders"
    "Total Active / Entered Active"
    .


FOR EACH lead-time NO-LOCK WHERE
    lead-time.a-date <> ? AND
    lead-time.e-act-ord <> 0:

    ASSIGN
        vCount = vCount + 1
	    vLeadTime = vLeadTime + (lead-time.t-act-ord / lead-time.e-act-ord)
	    .

    EXPORT DELIMITER ","
        lead-time.a-date
	    lead-time.t-act-ord
	    lead-time.t-for-ord
	    lead-time.e-act-ord
	    lead-time.e-for-ord
	    (lead-time.t-act-ord / lead-time.e-act-ord)
	    .
END.
OUTPUT CLOSE.

ASSIGN
    vLeadTime = vLeadTime / vCount.
OUTPUT TO /tmp/lead-time-txt_no-steel.
DISPLAY vLeadTime LABEL "Lead Time Average".
OUTPUT CLOSE.


UNIX SILENT "(cat /tmp/lead-time-txt_no-steel ; \
    uuencode /tmp/lead-time-theory-data_no-steel.csv /tmp/lead-time-theory-data_no-steel.csv) | \
    mailx -s 'Lead Time Theory' willcoxj@byrne-electrical.com levandoskip@byrne-electrical.com".
UNIX SILENT "rm /tmp/lead-time-txt".
UNIX SILENT "rm /tmp/lead-time-theory-data.csv".




