
def var f-file as char format "x(100)".

def temp-table this-to-that
    field old-pricecode     like symix.be-item-pricecode.pricecode
    field new-pricecode     like symix.be-item-pricecode.pricecode
    .

def temp-table fam-filter
    field family-code       like symix.famcode.family-code
    .

assign
    f-file = "M:\per-josh-pricing_" + replace(string(today), "/", "") + "_" + string(time) + ".txt".

input from M:\this-to-that_pricecode.txt.
repeat:
    create this-to-that.
    import delimiter "|" this-to-that.
end.
input close.


input from M:\family-code-filter.txt.
repeat:
    create fam-filter.
    import delimiter "|" fam-filter.
    if not can-find(first symix.famcode where 
                    famcode.family-code = fam-filter.family-code) then do:
        message "Deleting: " fam-filter.family-code.
        delete fam-filter.
    end.
end.
input close.



output to value(f-file).
export delimiter "|"
/*     "Last of Flag" */
    "FG Item" 
    "Description"
    "Product Code"
    "Family Code"
    "Old Price Code"
    "New Price Code"
    "Effective Date"
    "Unit Price 1"
    "Unit Price 2"
    "Unit Price 3"
    "Unit Price 4"
    "Unit Price 5"
    "Unit Price 6"
    "Break Qty 1"
    "Break Qty 3"
    "Break Qty 4"
    "Break Qty 5"
    "Break Qty 6"
    "Break Price 1"
    "Break Price 2"
    "Break Price 3"
    "Break Price 4"
    "Break Price 5"
    .
for each symix.item no-lock where
    not item.product-code = "FG-PD" and
    can-find(first fam-filter where
             fam-filter.family-code = item.family-code and
             fam-filter.family-code <> ""),
    each symix.be-item-pricecode no-lock where
    be-item-pricecode.item = item.item and 
    can-find(first this-to-that where
             this-to-that.old-pricecode = be-item-pricecode.pricecode)
    break by be-item-pricecode.item
          by be-item-pricecode.pricecode:

    if last-of(be-item-pricecode.pricecode) then do:

        find first this-to-that no-lock where
            this-to-that.old-pricecode = be-item-pricecode.pricecode no-error.

        export delimiter "|"
            item.item
            item.description
            item.product-code
            item.family-code
            be-item-pricecode.pricecode
            this-to-that.new-pricecode
            be-item-pricecode.effect-date
            be-item-pricecode.unit-price1
            be-item-pricecode.unit-price2
            be-item-pricecode.unit-price3
            be-item-pricecode.unit-price4
            be-item-pricecode.unit-price5
            be-item-pricecode.unit-price6
            be-item-pricecode.brk-qty[1]
            be-item-pricecode.brk-qty[2]
            be-item-pricecode.brk-qty[3]
            be-item-pricecode.brk-qty[4]
            be-item-pricecode.brk-qty[5]
            be-item-pricecode.brk-price[1]
            be-item-pricecode.brk-price[2]
            be-item-pricecode.brk-price[3]
            be-item-pricecode.brk-price[4]
            be-item-pricecode.brk-price[5]
            .
    end.

/*     export delimiter "|"                                         */
/*         if last-of(be-item-pricecode.pricecode) then "X" else "" */
/*         item.item                                                */
/*         item.description                                         */
/*         item.product-code                                        */
/*         item.family-code                                         */
/*         be-item-pricecode.pricecode                              */
/*         "n/a"                                                    */
/*         be-item-pricecode.effect-date                            */
/*         be-item-pricecode.unit-price1                            */
/*         be-item-pricecode.unit-price2                            */
/*         be-item-pricecode.unit-price3                            */
/*         be-item-pricecode.unit-price4                            */
/*         be-item-pricecode.unit-price5                            */
/*         be-item-pricecode.unit-price6                            */
/*         be-item-pricecode.brk-qty[1]                             */
/*         be-item-pricecode.brk-qty[2]                             */
/*         be-item-pricecode.brk-qty[3]                             */
/*         be-item-pricecode.brk-qty[4]                             */
/*         be-item-pricecode.brk-qty[5]                             */
/*         be-item-pricecode.brk-price[1]                           */
/*         be-item-pricecode.brk-price[2]                           */
/*         be-item-pricecode.brk-price[3]                           */
/*         be-item-pricecode.brk-price[4]                           */
/*         be-item-pricecode.brk-price[5]                           */
/*         .                                                        */


end.
output close.


