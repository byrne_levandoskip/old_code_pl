


DEF TEMP-TABLE always-include
    FIELD ITEM      LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE body-parts
    FIELD ITEM      LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE power-parts
    FIELD ITEM      LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE cord-parts
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD heyco     LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE data-parts-1
    FIELD ITEM      LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE data-parts-2
    FIELD ITEM      LIKE symix.ITEM.ITEM.

DEF TEMP-TABLE data-kit
    FIELD ITEM      LIKE symix.ITEM.ITEM
    FIELD adp-item  LIKE symix.ITEM.ITEM
    .

DEF TEMP-TABLE lid-parts
    FIELD ITEM      LIKE symix.ITEM.ITEM.

/* DEF TEMP-TABLE heyco-parts                */
/*     FIELD ITEM      LIKE symix.ITEM.ITEM. */

DEF TEMP-TABLE color-ref
    FIELD shrt-code AS CHAR FORMAT "xx"
    FIELD lng-code  AS CHAR FORMAT "x(20)".

DEF TEMP-TABLE plot-files-body
    FIELD plot      AS CHAR FORMAT "x(30)"  EXTENT 5.

DEF TEMP-TABLE plot-files-cord
    FIELD plot      AS CHAR FORMAT "x(30)"  EXTENT 5.


DEF SHARED TEMP-TABLE create-part
    FIELD ITEM          LIKE symix.ITEM.ITEM
    FIELD DESCRIPTION   LIKE symix.ITEM.DESCRIPTION
    FIELD all-parts     AS CHAR FORMAT "x(30)" EXTENT 20
    FIELD plotfile-1    LIKE symix.ux-item.uf-plotfile-1
    FIELD plotfile-2    LIKE symix.ux-item.uf-plotfile-2
    FIELD plotfile-3    LIKE symix.ux-item.uf-plotfile-3
    FIELD plotfile-4    LIKE symix.ux-item.uf-plotfile-4
    FIELD plotfile-5    LIKE symix.ux-item.uf-plotfile-5
    FIELD plotfile-6    LIKE symix.ux-item.uf-plotfile-6
    FIELD plotfile-7    LIKE symix.ux-item.uf-plotfile-7
    FIELD plotfile-8    LIKE symix.ux-item.uf-plotfile-8
    FIELD plotfile-9    LIKE symix.ux-item.uf-plotfile-9
    FIELD plotfile-10   LIKE symix.ux-item.uf-plotfile-10
    FIELD plotfile-11   LIKE symix.ux-item.uf-plotfile-11
    FIELD plotfile-12   LIKE symix.ux-item.uf-plotfile-12
    FIELD plotfile-13   LIKE symix.ux-item.uf-plotfile-13
    FIELD plotfile-14   LIKE symix.ux-item.uf-plotfile-14
    .

DEF VAR seq AS CHAR INITIAL "000000000000001" FORMAT "x(15)".
DEF VAR c AS INT INITIAL 1.

/* populate always include */
CREATE always-include.
ASSIGN
    always-include.ITEM = "IS03505".
CREATE always-include.
ASSIGN
    always-include.ITEM = "IS03508".
CREATE always-include.
ASSIGN
    always-include.ITEM = "IS03667".
CREATE always-include.
ASSIGN
    always-include.ITEM = "LB3400353".
CREATE always-include.
ASSIGN
    always-include.ITEM = "LB3105119".

/* /* populate HEYCO parts */     */
/* CREATE heyco-parts.            */
/* ASSIGN                         */
/*     heyco-parts.ITEM = "1237". */
/* CREATE heyco-parts.            */
/* ASSIGN                         */
/*     heyco-parts.ITEM = "1238". */

/* Populate Data Options 1 */
CREATE data-parts-1.
ASSIGN
    data-parts-1.ITEM = " ".

/* Populate Data Options 2 */
CREATE data-parts-2.
ASSIGN
    data-parts-2.ITEM = " ".



{M:\old-code\pop-temp-item-table.i 
    &CLEAR_TT=YES
    &TEMP_DB=cord-parts 
    &BEGINS_WITH=CD04747}
    
/* Remove Doug Mockett Cords */
FOR EACH cord-parts EXCLUSIVE-LOCK:
    
    IF cord-parts.ITEM MATCHES "*-DM*" THEN DO:
        DELETE cord-parts.
        NEXT.
    END.

    IF cord-parts.ITEM MATCHES "*-WH-*" THEN
        cord-parts.heyco = "1237".
    ELSE
        cord-parts.heyco = "1238".
END.

{M:\old-code\pop-temp-item-table.i 
    &CLEAR_TT=YES
    &TEMP_DB=lid-parts 
    &BEGINS_WITH=MD03384}
{M:\old-code\pop-temp-item-table.i 
    &CLEAR_TT=YES
    &TEMP_DB=power-parts 
    &BEGINS_WITH=POWER-4}
{M:\old-code\pop-temp-item-table.i 
    &CLEAR_TT=YES
    &TEMP_DB=body-parts 
    &BEGINS_WITH=NACRE-BODY-4}



RUN generate-parts
    (INPUT "POWERDATACENTER,NACRE,4A").


{M:\old-code\pop-temp-item-table.i 
    &CLEAR_TT=YES
    &TEMP_DB=power-parts 
    &BEGINS_WITH=POWER-3}
{M:\old-code\pop-temp-item-table.i 
    &CLEAR_TT=YES
    &TEMP_DB=body-parts 
    &BEGINS_WITH=NACRE-BODY-3-1}
    
RUN pop-data-kits-1.


RUN generate-parts
    (INPUT "POWERDATACENTER,NACRE,4A").




RUN output-results.



/*******************************************************************************/

PROCEDURE generate-parts:

    DEF INPUT PARAMETER p-desc  LIKE symix.ITEM.DESCRIPTION.

    FOR EACH body-parts WHERE
        body-parts.ITEM <> "",
        EACH power-parts WHERE
        power-parts.ITEM <> "",
        EACH data-kit,
        EACH cord-parts WHERE
        cord-parts.ITEM <> "",
        EACH lid-parts WHERE
        lid-parts.ITEM <> "":
    
        c = 1.
    
        CREATE create-part.
        ASSIGN
            create-part.ITEM = "BE03385-" + seq
            create-part.DESCRIPTION = p-desc
            create-part.all-parts[c] = body-parts.ITEM
            c = c + 1
            create-part.all-parts[c] = power-parts.ITEM
            c = c + 1
            create-part.all-parts[c] = STRING(data-parts-1.ITEM + " " + data-parts-2.ITEM)
            c = c + 1
            create-part.all-parts[c] = cord-parts.ITEM
            c = c + 1
            create-part.all-parts[c] = lid-parts.ITEM
            c = c + 1
/*             create-part.all-parts[c] = heyco-parts.ITEM */
            create-part.all-parts[c] = cord-parts.heyco
            c = c + 1
            seq = STRING(INT(seq) + 1, "999999999999999")
            .
    
        FOR EACH always-include WHERE
            always-include.ITEM <> "" NO-LOCK:
            ASSIGN
                create-part.all-parts[c] = always-include.ITEM
                c = c + 1.
        END.
    END.
END PROCEDURE.



PROCEDURE output-results:
    OUTPUT TO M:\test-generate-2.csv.
    EXPORT DELIMITER ","
        "Part #"
        "Description"
        "Body"
        "Power"
        "Data"
        "Cord"
        "Lid"
        "Heyco"
        .
    
    FOR EACH create-part NO-LOCK WHERE
        create-part.ITEM <> "":
        EXPORT DELIMITER ","
            create-part.
    END.
    OUTPUT CLOSE.
END PROCEDURE.

PROCEDURE pop-data-kits-1:
    EMPTY TEMP-TABLE data-parts-1.

    CREATE data-parts-1.
    ASSIGN
        data-parts-1.ITEM = "HDMI".
    
    CREATE data-parts-1.
    ASSIGN
        data-parts-1.ITEM = "VGA".
    
    CREATE data-parts-1.
    ASSIGN
        data-parts-1.ITEM = "CAT6".
    
    CREATE data-parts-1.
    ASSIGN
        data-parts-1.ITEM = "USB-A".
    
    CREATE data-parts-1.
    ASSIGN
        data-parts-1.ITEM = "USB-C".
    
    CREATE data-parts-1.
    ASSIGN
        data-parts-1.ITEM = "3.5mm".
    
    CREATE data-parts-1.
    ASSIGN
        data-parts-1.ITEM = "Open Data".
END.








