


DEFINE TEMP-TABLE found
    FIELD ord   LIKE symix.coitem.co-num
    FIELD ln    LIKE symix.coitem.co-line.



INPUT FROM m:\steel-due-tomorrow.csv NO-ECHO.
REPEAT:
    CREATE found.
    IMPORT DELIMITER "," found.
END.
INPUT CLOSE.

OUTPUT TO M:\date-change-mass.csv.
FOR EACH found NO-LOCK WHERE
    found.ord <> "",
    FIRST symix.coitem EXCLUSIVE-LOCK WHERE
    coitem.co-num = found.ord AND
    coitem.co-line = found.ln,
    EACH symix.job-sch EXCLUSIVE-LOCK WHERE
    job-sch.job = coitem.ref-num:
    ASSIGN
        coitem.due-date = TODAY
        job-sch.start-date = TODAY
        job-sch.end-date = TODAY
        .
    EXPORT DELIMITER ","
        coitem.co-num
        coitem.co-line
        coitem.due-date
        job-sch.job
        job-sch.start-date.
END.
OUTPUT CLOSE.



/*************************************************************************/

/* DEF VAR c AS INT.                                                    */
/*                                                                      */
/* OUTPUT TO M:\phy-loc-check-2.csv.                                    */
/* FOR EACH symix.phyinv NO-LOCK                                        */
/*     BREAK BY phyinv.ITEM:                                            */
/*     IF FIRST-OF(phyinv.ITEM) THEN                                    */
/*         c = 0.                                                       */
/*     c = c + 1.                                                       */
/*     IF LAST-OF(phyinv.ITEM) AND c = 1 AND phyinv.loc <> "STOCK" THEN */
/*         EXPORT DELIMITER ","                                         */
/*         phyinv.                                                      */
/* END.                                                                 */
/* OUTPUT CLOSE.                                                        */


FOR EACH symix.itemloc NO-LOCK WHERE
    itemloc.ITEM = "6005n1":
    DISPLAY itemloc WITH 2 COL.
END.

OUTPUT TO M:\missing-phyinv.csv.
FOR EACH symix.be-invtag-t NO-LOCK:

    FIND FIRST symix.phyinv NO-LOCK WHERE
        phyinv.ITEM = be-invtag-t.ITEM AND
        phyinv.loc = be-invtag-t.loc NO-ERROR.

    IF NOT AVAIL phyinv THEN DO:
        EXPORT DELIMITER ","
            be-invtag-t.
    END.
END.
OUTPUT CLOSE.


/**************************************************************/


DEF VAR on-hand AS INT.

OUTPUT TO M:\qtys-on-hand.csv.
FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.product-code = "rm-lb" OR
    ITEM.product-code = "rm-cn",
    EACH symix.itemloc OF symix.ITEM NO-LOCK
    BREAK BY itemloc.ITEM:

    IF FIRST-OF(itemloc.ITEM) THEN
        on-hand = 0.

    on-hand = on-hand + itemloc.qty-on-hand.

    IF LAST-OF(itemloc.ITEM) THEN
        EXPORT DELIMITER ","
        ITEM.ITEM
        ITEM.product-code
        on-hand.

END.
OUTPUT CLOSE.

                                                                    
/******************************************************************/


/* FOR EACH symix.pckitem EXCLUSIVE-LOCK WHERE */
/*     pckitem.co-num = " 338795" AND          */
/*     pckitem.co-line = 27:                   */
/*     UPDATE pckitem WITH 2 COL.              */
/* END.                                        */


/* FIND LAST symix.pckitem EXCLUSIVE-LOCK WHERE */
/*     pckitem.co-num      = " 338795"          */
/*     AND pckitem.co-line     = 27             */
/*     NO-ERROR.                                */
/*                                              */
/* UPDATE pckitem WITH 2 COL.                   */



/* FIND LAST symix.co-ship NO-LOCK WHERE */
/*     co-ship.co-num = " 338795" AND    */
/*     co-ship.co-line = 27 NO-ERROR.    */
/* DISPLAY co-ship WITH 2 COL.           */

/* FOR EACH symix.co-ship NO-LOCK WHERE */
/*     co-ship.co-num = " 338795":      */
/*     DISPLAY co-ship WITH 2 COL.      */
/* END.                                 */




/*****************************************************************/

FOR EACH symix.po-bln NO-LOCK WHERE
    po-bln.po-line GE 100
    BREAK BY po-bln.po-num:
    IF FIRST-OF(po-bln.po-num) THEN DO:
        DISPLAY po-bln WITH 2 COL.
    END.
END.




/*************************************************************/

DEFINE BUFFER b-job FOR byrne.job.
DEF VAR vFound as log NO-UNDO INIT FALSE.
OUTPUT TO M:\job_check.csv.

EXPORT DELIMITER ","
    "Job Number"
    "Order Number"
    "Order Line"
    "Item"
    "Qty on Hand"
    "Completed"
    "Released"
    "Job Date"
    "Job Status"
    "Order Status"
    "Difference".

/* Find all completed order lines that have a reference job */
FOR EACH byrne.coitem NO-LOCK WHERE (coitem.stat = "C" OR coitem.stat = "F") 
    AND coitem.ref-num <> "" 
    AND coitem.ship-date LE 11/30/13,
    FIRST byrne.job NO-LOCK WHERE job.job = coitem.ref-num:

    /* narrow the results down to "Job" jobs */
    IF  job.stat <> "F" AND
        job.TYPE = "J" AND
        job.ord-type <> "I" THEN DO:

        IF (job.stat = "C" AND 
            (job.qty-released <> job.qty-complete AND 
             job.qty-complete <> 0 )) OR 
            (job.stat <> "C")
            THEN DO:


            FIND FIRST byrne.itemwhse WHERE
                itemwhse.ITEM = coitem.ITEM NO-LOCK NO-ERROR.

            EXPORT DELIMITER ","
                job.job
                job.ord-num
                job.ord-line
		        coitem.item
	            itemwhse.qty-on-hand
                job.qty-complete
                job.qty-released
		        job.job-date
                job.stat
                coitem.stat
		        job.qty-complete - job.qty-released.

            vFound = TRUE.

            FIND b-job EXCLUSIVE-LOCK WHERE 
                b-job.job = job.job AND 
                b-job.suffix = job.suffix NO-ERROR.
            ASSIGN b-job.stat = "C".

        END.
    END.
END.


OUTPUT CLOSE.




/***************************************************************/



OUTPUT TO M:\conlot-568326.csv.

EXPORT DELIMITER ","
    "Order Num"
    "Order Ln"
    "Qty Ordered"
    .

FOR EACH symix.ux-coitem NO-LOCK WHERE
    ux-coitem.uf-con-lot = 568326,
    EACH symix.coitem NO-LOCK OF ux-coitem:
    EXPORT DELIMITER ","
        ux-coitem.co-num 
        ux-coitem.co-line 
        coitem.qty-ordered.
END.

OUTPUT CLOSE.




/********************************************************************/

DEF TEMP-TABLE found NO-UNDO
    FIELD itm   LIKE symix.ITEM.ITEM
    FIELD s     AS INT.

DEF VAR spac    AS CHAR FORMAT "x(40)" EXTENT 40.
DEF VAR s       AS CHAR FORMAT "x(40)".
DEF VAR t       AS INT.


s = " ".
REPEAT t = 1 TO 40:
    spac[t] = s.
    s = s + " ".
/*     DISPLAY t STRING("|" + s + "|") FORMAT "x(50)" WITH WIDTH 300. */
END.

FOR EACH symix.ITEM NO-LOCK:
    IF ITEM.DESCRIPTION = "" THEN DO:
        CREATE found.
        ASSIGN 
            found.itm = ITEM.ITEM
            found.s   = 0.
    END.
    REPEAT t = 1 TO 40:
        IF ITEM.DESCRIPTION = spac[t] THEN DO:
            CREATE found.
            ASSIGN 
                found.itm = ITEM.ITEM
                found.s   = t.
        END.
    END.
END.

OUTPUT TO M:\blank-desc.csv.
FOR EACH found NO-LOCK:
    EXPORT DELIMITER ","
        found.
END.




DEF TEMP-TABLE found NO-UNDO
    FIELD itm   LIKE symix.ITEM.ITEM
    FIELD des   LIKE symix.ITEM.DESCRIPTION.


FOR EACH symix.ITEM NO-LOCK:
    IF LENGTH(ITEM.DESCRIPTION) LE 10 THEN DO:
        CREATE found.
        ASSIGN 
            found.itm = ITEM.ITEM
            found.des = ITEM.DESCRIPTION.
    END.
END.

OUTPUT TO M:\short-desc.csv.
FOR EACH found NO-LOCK:
    EXPORT DELIMITER ","
        found.
END.



/**************************************************/

DEFINE TEMP-TABLE found NO-UNDO
    FIELD ord     LIKE symix.co.co-num
    .

RUN find-stuff.
RUN change-stuff.

PROCEDURE find-stuff:
    FOR EACH symix.be_shipment_details NO-LOCK WHERE
        be_shipment_details.shipviadesc = "":

        FIND FIRST found NO-LOCK WHERE
            found.ord = TRIM(be_shipment_details.orderno) NO-ERROR.

        IF NOT AVAIL found THEN DO:    
            CREATE found.
            ASSIGN
                found.ord = " " + be_shipment_details.orderno.
        END.
        RELEASE found.
    END.
END PROCEDURE.


PROCEDURE change-stuff:
    FOR EACH found NO-LOCK,
        EACH symix.be_shipment_details NO-LOCK WHERE
        TRIM(be_shipment_details.orderno) = found.ord:

        FIND FIRST symix.co NO-LOCK WHERE
        co.co-num = found.ord NO-ERROR.

        FIND FIRST symix.shipcode NO-LOCK WHERE
        shipcode.ship-code = co.ship-code NO-ERROR.


        ASSIGN
            be_shipment_details.shipviadesc = shipcode.DESCRIPTION
            .
    END.
END PROCEDURE.


/* OUTPUT TO M:\shipment-details.csv.                 */
/*                                                    */
/* FOR EACH symix.be_shipment_details NO-LOCK WHERE   */
/*     be_shipment_details.shipviadesc = "",          */
/*     FIRST symix.co NO-LOCK WHERE                   */
/*     TRIM(co.co-num) = be_shipment_details.orderno, */
/*     FIRST symix.shipcode NO-LOCK WHERE             */
/*     shipcode.ship-code = co.ship-code:             */
/*                                                    */
/*     EXPORT DELIMITER ","                           */
/*         co.co-num                                  */
/*         co.order-date                              */
/*         be_shipment_details.shipviadesc            */
/*         shipcode.DESCRIPTION                       */
/*         .                                          */
/* END.                                               */
/*                                                    */
/* OUTPUT CLOSE.                                      */


/*************************************************************************/


DEF VAR c AS CHAR FORMAT "x(10)".

c = "12345-6789".

MESSAGE c VIEW-AS ALERT-BOX.

c = REPLACE(c,"-","").

MESSAGE c VIEW-AS ALERT-BOX.

soldtozipcode

/***********************************************************************/

OUTPUT TO M:\e-table-orders.csv.
EXPORT DELIMITER ","
    "Item"
    "Order"
    "Line"
    "Order-date"
    "Due-date"
    "Qty-Ordered"
    "Qty-Shipped"
    "Order Status"
    .

FOR EACH symix.coitem NO-LOCK WHERE
    (coitem.ITEM BEGINS "227151" OR
     coitem.ITEM BEGINS "227152") AND
    coitem.due-date GE 02/01/13,
    FIRST symix.co NO-LOCK OF symix.coitem:

    EXPORT DELIMITER ","
        coitem.ITEM
        coitem.co-num
        coitem.co-line
        co.order-date
        coitem.due-date
        coitem.qty-ordered
        coitem.qty-shipped
        coitem.stat
        .
END.

/***********************************************************************/

FOR EACH symix.ux-coitem EXCLUSIVE-LOCK WHERE
    ux-coitem.co-num = " 337390" AND
    ux-coitem.co-line = 1:

    ASSIGN
        ux-coitem.uf-con-lot        = 584563
        ux-coitem.uf-date-lotsht    = 09/01/13
        ux-coitem.uf-time-lotsht    = TIME.

    DISPLAY ux-coitem WITH 2 COL.
END.


/***********************************************************************/

FOR EACH symix.co NO-LOCK WHERE
    co.cust-num = "ALLMUS" AND
    co.order-date = TODAY,
    EACH symix.ux-coitem NO-LOCK OF symix.co
    BREAK BY ux-coitem.uf-con-lot DESC:

    DISPLAY ux-coitem WITH 2 COL.
END.

/*********************************************************************/


FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.cust-po = "4505040076" AND
    be-loadscan.po-line = 2:

    DISPLAY be-loadscan WITH 2 COL.
END.

FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.barcode = "1PNC4387%" OR
    be-loadscan.barcode = "1PNC43880" OR
    be-loadscan.barcode = "1PNC43891" OR
    be-loadscan.barcode = "1PNC4390." OR
    be-loadscan.barcode = "1PNC4391":

    DISPLAY be-loadscan WITH 2 COL.
END.

/**********************************************************************/




DEF VAR coitem-num AS INT INITIAL 0.
DEF VAR pckitem-num AS INT INITIAL 0.
DEF VAR be-coitem-ups-num AS INT INITIAL 0.

FOR EACH symix.coitem NO-LOCK WHERE
    coitem.co-num = " 330570",
    FIRST symix.co OF symix.coitem NO-LOCK:

    coitem-num = coitem-num + 1.

    FOR EACH symix.pck-hdr OF symix.co NO-LOCK:

        FOR EACH symix.pckitem OF symix.pck-hdr NO-LOCK:

          pckitem-num = pckitem-num + 1.

          FOR EACH symix.be-coitem-ups NO-LOCK WHERE
            be-coitem-ups.co-num BEGINS TRIM(pckitem.co-num):

              be-coitem-ups-num = be-coitem-ups-num + 1.


          END.
        END.
    END.
END.


MESSAGE 
    "number of coitems: " coitem-num SKIP
    "number of pckitem: " pckitem-num SKIP
    "number of be-coitem-ups: " be-coitem-ups-num SKIP
    VIEW-AS ALERT-BOX.


OUTPUT TO M:\temps.csv.
FOR EACH byrne.be-coitem-ups NO-LOCK WHERE
    be-coitem-ups.co-num BEGINS "33"
    BREAK BY be-coitem-ups.co-num DESC:

    DISPLAY be-coitem-ups.co-num WITH 2 COL.
END.
OUTPUT CLOSE.

/* FOR EACH symix.pck-hdr NO-LOCK     */
/*     BREAK BY pck-hdr.co-num DESC:  */
/*     DISPLAY pck-hdr WITH 2 COL.    */
/* END.                               */


/************************************************************************/

OUTPUT TO M:\yesterdays-acks.csv.
EXPORT DELIMITER ","
    "Cust Num"
    "Order"
    "Ack Date"
    "Ack Type"
    .

FOR EACH symix.ux-co NO-LOCK WHERE
    ux-co.uf-verify-date = 08/27/13,
    EACH symix.co NO-LOCK OF symix.ux-co:

    EXPORT DELIMITER ","
        co.cust-num
        co.co-num
        ux-co.uf-verify-date
        ux-co.uf-verify-format
        .

END.



/************************************************************************/


FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.scanned = YES AND
    be-loadscan.extracted = NO AND
    be-loadscan.trailer = "":

    DISPLAY be-loadscan WITH 2 COL.
END.

FOR EACH symix.be-loadscan EXCLUSIVE-LOCK WHERE
    be-loadscan.scanned = YES AND
    be-loadscan.extracted = NO AND
    be-loadscan.trailer = "":


    ASSIGN
        be-loadscan.trailer = "".
END.






OUTPUT TO M:\hold-report3.csv.

FOR EACH symix.coitem NO-LOCK WHERE 
    coitem.due-date LE TODAY + 10
    AND coitem.qty-shipped LT coitem.qty-ordered
    AND coitem.stat EQ "O",
    FIRST symix.co NO-LOCK WHERE 
    co.co-num EQ coitem.co-num
    AND co.terms-code NE "SAM",
    FIRST symcust.custaddr NO-LOCK WHERE custaddr.cust-num EQ co.cust-num,
    FIRST symix.customer NO-LOCK WHERE customer.cust-num EQ co.cust-num:
    
        IF NOT co.credit-hold AND NOT custaddr.credit-hold THEN
            NEXT.

    EXPORT DELIMITER ","
        customer.cust-num
        custaddr.NAME
        customer.terms-code
        custaddr.credit-limit
        custaddr.posted-bal
        custaddr.order-bal
        (IF co.order-date EQ TODAY THEN coitem.qty-ordered * coitem.price ELSE 0)
        coitem.co-num
        coitem.co-line
        ((coitem.qty-ordered - coitem.qty-shipped) * coitem.price)
        coitem.due-date
        custaddr.credit-hold
        co.credit-hold.

END.

OUTPUT CLOSE.




/********************************************************************/


OUTPUT TO M:\found-loadscan.csv.
EXPORT DELIMITER ","
    "Cust PO"
    "PO Line"
    "Order"
    .

FOR EACH symix.coitem NO-LOCK WHERE
    coitem.due-date = TODAY,
    EACH symix.co NO-LOCK OF symix.coitem WHERE
    co.cust-num = "STEEL":

    FIND FIRST symix.be-loadscan NO-LOCK WHERE
        be-loadscan.cust-po = co.cust-po AND
        be-loadscan.po-line = coitem.co-line NO-ERROR.


    IF AVAIL be-loadscan THEN DO:

        EXPORT DELIMITER ","
            co.cust-po
            coitem.co-line
            coitem.co-num
            .

    END.

END.

OUTPUT CLOSE.





DEFINE TEMP-TABLE record NO-UNDO
    FIELD ord     LIKE symix.coitem.co-num
    FIELD ln      LIKE symix.coitem.co-line
    FIELD cst     LIKE symix.co.cust-num
    FIELD itm     LIKE symix.ITEM.ITEM
    FIELD qty     LIKE symix.coitem.qty-shipped
    FIELD po      LIKE symix.co.cust-po
    .

RUN get-orders.
RUN print-orders.

PROCEDURE get-orders:

    FOR EACH symix.coitem NO-LOCK WHERE
        coitem.due-date GE 07/01/12 AND
        coitem.due-date LT 06/30/13,
        EACH symix.co NO-LOCK OF symix.coitem WHERE
        co.cust-po MATCHES "*SAMPLE*":
    

        CREATE record.
        ASSIGN
            record.ord = coitem.co-num
            record.ln  = coitem.co-line
            record.cst = co.cust-num
            record.itm = coitem.ITEM
            record.qty = coitem.qty-shipped
            record.po  = co.cust-po
            .
    END.

END PROCEDURE.

PROCEDURE print-orders:

    OUTPUT TO M:\sample-orders.csv APPEND.
    EXPORT DELIMITER ","
        "Order Num"
        "Order Line"
        "Cust Num"
        "Item"
        "QTY"
        "Cust PO"
        .
    FOR EACH record NO-LOCK WHERE
        record.ord <> "":

        EXPORT DELIMITER ","
            record.ord
            record.ln
            record.cst
            record.itm
            record.qty
            record.po
            .
    END.
    OUTPUT CLOSE.

END PROCEDURE.


06/01/12
07/01/13
/***************************************/


DEFINE TEMP-TABLE record NO-UNDO
    FIELD itm     LIKE symix.ITEM.ITEM
    FIELD qty     LIKE symix.coitem.qty-shipped
    FIELD cst     LIKE symix.co.cust-num
    .

RUN get-orders.
RUN print-orders.

PROCEDURE get-orders:

    FOR EACH symix.coitem NO-LOCK WHERE
        coitem.due-date GE 07/01/12 AND
        coitem.due-date LE 06/30/13 AND
        (coitem.ITEM MATCHES "*Z01*" OR 
         coitem.ITEM MATCHES "*Z02*"),
        EACH symix.co NO-LOCK OF symix.coitem:
    
        FIND FIRST record NO-LOCK WHERE
            record.itm = coitem.ITEM AND
            record.cst = co.cust-num NO-ERROR.

        IF NOT AVAIL record THEN DO:
            CREATE record.
            ASSIGN
                record.itm = coitem.ITEM
                record.qty = coitem.qty-shipped
                record.cst = co.cust-num
                .
        END.
        ELSE DO:
            ASSIGN
                record.qty = record.qty + coitem.qty-shipped
                .
        END.
    END.

END PROCEDURE.

PROCEDURE print-orders:

    OUTPUT TO M:\z-finshes.csv APPEND.
    EXPORT DELIMITER ","
        "Item"
        "Qty"
        "Cust Num"
        .
    FOR EACH record NO-LOCK WHERE
        record.itm <> "":

        EXPORT DELIMITER ","
            record.itm
            record.qty
            record.cst
            .
    END.
    OUTPUT CLOSE.

END PROCEDURE.


07/01/12
06/30/13
/***************************************/
98009

DEF BUFFER bbItem FOR symix.ITEM.

FOR EACH symix.co-ship NO-LOCK WHERE 
    co-ship.ship-date GE 08/01/13 AND
    co-ship.ship-date LE 08/01/13, 
    FIRST symix.coitem NO-LOCK OF co-ship,
    FIRST symix.co OF coitem NO-LOCK,
    FIRST symix.ITEM OF coitem NO-LOCK WHERE
    NOT ITEM.product-code MATCHES "*pd*",
    FIRST symix.job NO-LOCK WHERE 
    job.job = coitem.ref-num:



    FOR EACH symix.jobmatl OF job NO-LOCK,
        FIRST bbItem WHERE bbItem.ITEM = jobmatl.ITEM AND                                 
        bbItem.unit-weight <> 0 AND
        (bbItem.product-code = "rm-cd" OR
        bbitem.product-code = "rm-te" OR
        bbItem.product-code = "rm-wr") :           


        OUTPUT TO M:\gone-fishing.csv APPEND.
        EXPORT DELIMITER ","
            coitem.ITEM
            job.job
            bbItem.ITEM
            jobmatl.matl-qty-conv
            bbItem.unit-weight
            .
        OUTPUT CLOSE.
    END.
END.



FOR EACH symix.jobmatl OF job NO-LOCK,
    FIRST bbItem WHERE bbItem.ITEM = jobmatl.ITEM AND                                 
    bbItem.unit-weight <> 0 AND
    (bbItem.product-code = "rm-cd" OR
    bbitem.product-code = "rm-te" OR
    bbItem.product-code = "rm-wr") :           

    ttCustItem.copper           = ttCustItem.copper + (jobmatl.matl-qty-conv * bbItem.unit-weight).
END.





FOR EACH symix.be-loadscan EXCLUSIVE-LOCK WHERE
    be-loadscan.cust-po = "4505040290" AND
    be-loadscan.extracted = YES:

    be-loadscan.extracted = NO.
    
END. 

DEF VAR t AS INT INITIAL 0.

FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.cust-po = "4505040290" AND
    be-loadscan.extracted = YES:

    t = t + 1.
END. 

DISPLAY t WITH 2 COL.


FOR EACH symix.be-loadscan EXCLUSIVE-LOCK WHERE
    be-loadscan.cust-po = "4505056863":

    ASSIGN
        be-loadscan.extracted = NO.

    DISPLAY be-loadscan WITH 2 COL.
END.

/* FOR EACH symix.be-loadscan NO-LOCK WHERE */
/*     be-loadscan.barcode = "1PL1K212H":   */
/*                                          */
/*     DISPLAY be-loadscan WITH 2 COL.      */
/* END.                                     */


FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.cust-po = "4505040290":

    DISPLAY be-loadscan WITH 2 COL.
END.




/*************************************************/


FOR EACH symix.edi-co NO-LOCK WHERE
    edi-co.cust-po = "38-30082-255",
    EACH symix.edi-coitem NO-LOCK OF symix.edi-co:

    DISPLAY edi-coitem WITH 2 COL WIDTH 300.
END.



OUTPUT TO M:\price-cr-status.csv.
EXPORT DELIMITER ","
    "Order"
    "Line"
    "Due Date"
    "Price"
    .

FOR EACH symix.ux-coitem NO-LOCK WHERE
    ux-coitem.uf-see-eng-stat = "price/cr" OR
    ux-coitem.uf-see-eng-stat = "cr",
    EACH symix.coitem NO-LOCK OF symix.ux-coitem WHERE
    coitem.stat = "o":

    EXPORT DELIMITER ","
        coitem.co-num
        coitem.co-line
        coitem.due-date
        coitem.price.
END.

OUTPUT CLOSE.


/* FOR EACH symix.be-see-eng NO-LOCK WHERE */
/*     be-see-eng.co-num = " 332171":      */
/*                                         */
/*     DISPLAY be-see-eng WITH 2 COL.      */
/* END.                                    */





/* FOR EACH symix.be-see-eng NO-LOCK WHERE     */
/*     be-see-eng.last-stage <> "Finished" AND */
/*     be-see-eng.last-stage <> "CAD":         */
/*                                             */
/*     DISPLAY be-see-eng WITH 2 COL.          */
/* END.                                        */


FOR EACH symix.be-see-eng NO-LOCK WHERE
    be-see-eng.co-num = " 332171":

    DISPLAY be-see-eng WITH 2 COL.
END.





FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.trailer = "Direct"
    BREAK BY be-loadscan.trans-date DESC:

    DISPLAY be-loadscan WITH 2 COL.         
END.



FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.cust-po = "4505040207":

    DISPLAY be-loadscan WITH 2 COL.
END.







/* OUTPUT TO M:\outz.txt. */
FOR EACH symix.edi-co NO-LOCK WHERE
    edi-co.posted = NO,
    EACH symix.edi-coitem EXCLUSIVE-LOCK OF symix.edi-co:

    edi-coitem.due-date = 08/02/13.

/*     DISPLAY edi-coitem WITH 2 COL WIDTH 300. */
END.




OUTPUT TO M:\mold-tool-2035.csv.
EXPORT DELIMITER ","
    "Item"
    "Description"
    "Mold1"
    "Mold2"
    "Mold3"
    "Mold4"
    .

FOR EACH symix.ux-item NO-LOCK WHERE
    ux-item.uf- = "2035":

END.

FOR EACH symix.ux-item NO-LOCK WHERE
    ux-item.uf- = "2035":

END.

FOR EACH symix.ux-item NO-LOCK WHERE
    ux-item.uf- = "2035":

END.

FOR EACH symix.ux-item NO-LOCK WHERE
    ux-item.uf- = "2035":

END.

OUTPUT CLOSE.



FOR EACH symix.ux-coitem EXCLUSIVE-LOCK WHERE
    ux-coitem.co-num = " 328631" AND
    ux-coitem.co-line = 1:

    UPDATE ux-coitem WITH 2 COL.
END.






FOR EACH symix.edi-coitem NO-LOCK WHERE
    edi-coitem.co-num = "E315392" AND
    edi-coitem.co-line = 5,
    FIRST symix.ux-edi-coitem NO-LOCK OF symix.edi-coitem:

    DISPLAY edi-coitem WITH 2 COL WIDTH 300.
END.



/* FOR EACH symix.be-loadscan NO-LOCK WHERE       */
/*     be-loadscan.cust-po = "4504918525": /* AND */
/*     be-loadscan.po-line = 4: */                */
/*                                                */
/*     DISPLAY be-loadscan WITH 2 COL.            */
/* END.                                           */


/* FOR EACH symix.be-loadscan NO-LOCK WHERE */
/*     be-loadscan.barcode = "1pkvg621t":   */
/*                                          */
/*     DISPLAY be-loadscan WITH 2 COL.      */
/* END.                                     */


OUTPUT TO M:\project\skid-counts\skid27.csv.
FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.trans-date = 06/27/13 AND
    be-loadscan.skid = 27:

    EXPORT DELIMITER ","
        be-loadscan.cust-po
        be-loadscan.po-line
        be-loadscan.barcode
        be-loadscan.qty
        be-loadscan.trans-time.

END.




/* RUN search-ux-item.                                        */
/*                                                            */
/*                                                            */
/* PROCEDURE search-ux-item:                                  */
/*                                                            */
/*     FOR EACH symix.ux-item NO-LOCK:                        */
/*         RUN search-fam.                                    */
/*     END.                                                   */
/* END PROCEDURE.                                             */
/*                                                            */
/*                                                            */
/* PROCEDURE search-fam:                                      */
/*     DEF VAR t AS INT.                                      */
/*                                                            */
/*     REPEAT t = 1 TO 30:                                    */
/*         OUTPUT TO M:\items.csv APPEND.                     */
/*         IF symix.ux-item.uf-family[t] MATCHES "*Arc*" THEN */
/*             EXPORT DELIMITER ","                           */
/*                 ux-item.ITEM                               */
/*                 ux-item.uf-family[t].                      */
/*         OUTPUT CLOSE.                                      */
/*     END.                                                   */
/* END PROCEDURE.                                             */




FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.cust-po = "4504985212":

    DISPLAY be-loadscan WITH 2 COL.
END.
/**********************************************************/

RUN check-cons.

PROCEDURE check-cons.

    OUTPUT TO M:\con-lots-3.csv APPEND.
    
    FOR EACH symix.ux-coitem NO-LOCK WHERE
        ux-coitem.uf-con-lot > 564025 AND
        ux-coitem.uf-date-lotsht = 06/21/13:
         
        
        EXPORT DELIMITER ","
            ux-coitem.uf-con-lot
            ux-coitem.uf-date-lotsht
            .
    END.
END PROCEDURE.



RUN check-cons.

PROCEDURE check-cons.

    OUTPUT TO M:\con-lots-2.csv APPEND.
    
    FOR EACH symix.ux-coitem NO-LOCK WHERE
        ux-coitem.uf-con-lot > 564025,
        FIRST symix.coitem NO-LOCK OF symix.ux-coitem WHERE
        coitem.due-date = 07/01/13:
         
        
        EXPORT DELIMITER ","
            ux-coitem.uf-con-lot
            coitem.co-num
            coitem.co-line
            coitem.due-date
            coitem.ITEM
            .
    END.
END PROCEDURE.



RUN check-cons.

PROCEDURE check-cons.

    OUTPUT TO M:\con-lots.csv APPEND.
    
    FOR EACH symix.ux-coitem NO-LOCK WHERE
        ux-coitem.uf-con-lot > 564025:

        FIND FIRST symix.coitem NO-LOCK
            OF symix.ux-coitem NO-ERROR.

        IF coitem.due-date = 07/01/13 THEN
            EXPORT DELIMITER ","
                ux-coitem.uf-con-lot
                coitem.co-num
                coitem.co-line
                coitem.due-date
                coitem.ITEM
                .
    END.
END PROCEDURE.

/**********************************************************/

FOR EACH symix.be-loadscan EXCLUSIVE-LOCK WHERE
/*     be-loadscan.cust-po = "4504982654": */

    ASSIGN
        be-loadscan.trailer = "Direct"
        be-loadscan.ship-type = "FedEx"
        be-loadscan.trans-date = TODAY
        be-loadscan.trans-time = TIME
        be-loadscan.scanned = YES
/*         be-loadscan.track-num = "539163821571" */
        .

    DISPLAY be-loadscan WITH 2 COL.
END.


/************************************************************/


FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.trans-date = 06/12/13 AND
    be-loadscan.skid = 2:

    DISPLAY be-loadscan WITH 2 COL.
END.


FOR EACH symix.be-loadscan EXCLUSIVE-LOCK WHERE
    be-loadscan.cust-po = "4504977847":

    ASSIGN
        be-loadscan.ship-type = "FedEx"
        be-loadscan.scanned = YES
        be-loadscan.trailer = "Direct"
        be-loadscan.trans-date = TODAY
        be-loadscan.trans-time = TIME
        be-loadscan.track-num = "539163821446"
        .


    DISPLAY be-loadscan WITH 2 COL.
END.


/****************************************************************/

DEF VAR t-new-amount LIKE symcust.custaddr.amt-over-inv-amt.

FOR EACH symcust.custaddr EXCLUSIVE-LOCK:

    t-new-amount = custaddr.credit-limit * 1.5.

    IF t-new-amount >= 9999999999.00 THEN DO:
        
        ASSIGN
            custaddr.amt-over-inv-amt = 9999999999.00
            custaddr.days-over-inv-due-date = 30.

    END.
    ELSE DO:

        ASSIGN
            custaddr.amt-over-inv-amt = t-new-amount
            custaddr.days-over-inv-due-date = 30.

    END.
END.


/****************************************************************/

DEF VAR myX AS INT INITIAL 0.

FOR EACH symix.co NO-LOCK WHERE
    co.freight-t <> 0 AND
    co.freight = 0,
    EACH symix.coitem NO-LOCK OF symix.coitem
    BREAK BY coitem.co-num DESC:

    IF FIRST-OF(coitem.co-num) THEN DO:
        myX = 0.
        myX = myX + co.freight-t.
        myX = myX + co.m-charges-t.
    END.

    IF LAST-OF(coitem.co-num) AND
        myX <> co.price THEN
        DISPLAY co WITH 2 COL.

END.

/****************************************************************/

OUTPUT TO M:\test\create-jobs\no-jobs.txt.

FOR EACH symix.coitem NO-LOCK WHERE
    coitem.stat     EQ "o"       AND
    coitem.ref-num  EQ ""        AND
    coitem.due-date LE TODAY + 7 AND
    NOT(coitem.ITEM BEGINS "see_eng"):

    DISPLAY coitem.co-num coitem.co-line coitem.ref-num WITH 2 COL.
END.

OUTPUT CLOSE.


/***************************************************************/

FOR EACH symix.co NO-LOCK WHERE
    co.cust-num = "MAISP",
    EACH symix.notes NO-LOCK WHERE 
    co.KEY = notes.KEY AND
    notes.txt BEGINS "tag" AND
    NOT (notes.txt BEGINS "tag:"):

    DISPLAY co.co-num notes.txt WITH 2 COL.
END.

/***************************************************************/

DEF VAR vFound AS LOG INIT FALSE.
DEF VAR s-date          AS DATE                         NO-UNDO.
DEF VAR vOrdTot         AS INT                          NO-UNDO.
DEF VAR vOrdLineTot     AS INT                          NO-UNDO.

ASSIGN s-date = TODAY - 1.

output to /tmp/daily_orders_tot.txt.


FOR EACH byrne.co NO-LOCK WHERE co.order-date = s-date and
        not co.taken-by matches "EDI*":
    vOrdTot = vOrdTot + 1.

    for EACH byrne.coitem NO-LOCK WHERE coitem.co-num = co.co-num:
    vOrdLineTot = vOrdLineTot + 1.

    END.
END.

Display
        "Order Date"
        s-date
        vOrdTot
        vOrdLineTot.


OUTPUT CLOSE.


/**********************************************************/
        
FOR EACH symix.be-labelq NO-LOCK:

    OUTPUT TO VALUE("M:\test\labels\" + be-labelq.lbl-format + be-labelq.lot + ".txt").

    DISPLAY be-labelq WITH 2 COL.

    OUTPUT CLOSE.
END.


/*********************************************************/


FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.trans-date = 05/03/13:

    DISPLAY be-loadscan WITH 2 COL.
END.


/*******************************************************/

OUTPUT TO M:\test\2012-itemcreation-purchased.csv.
EXPORT DELIMITER ","
    "Item Name"
    "Creation Date"
    "Prod Code"
    .

FOR EACH symix.ITEM NO-LOCK WHERE
    ITEM.p-m-t-code = "p",
    EACH symix.ux-item NO-LOCK OF symix.ITEM WHERE
    ux-item.uf-date-ent GE 01/01/12 AND
    ux-item.uf-date-ent LE 12/31/12:

    EXPORT DELIMITER ","
        ITEM.ITEM
        ux-item.uf-date-ent
        ITEM.product-code
        .
END.

OUTPUT CLOSE.


/*********************************************************/


OUTPUT TO M:\test\alt-maybe.txt.

FOR EACH symix.co NO-LOCK WHERE
    co.cust-num = "STEEL" AND
    co.order-date = TODAY,
    EACH symix.job NO-LOCK WHERE
    job.ord-num = co.co-num,
    EACH symix.jobmatl NO-LOCK OF symix.job WHERE
    jobmatl.ITEM MATCHES "*-alt*":


    DISPLAY co.co-num job.job jobmatl.ITEM WITH 2 COL.
END.

OUTPUT CLOSE.


/********************************************************/



FOR EACH symix.ux-item NO-LOCK WHERE
    ux-item.uf-item-option1 = "" AND
    ux-item.ITEM MATCHES "*+STEEL*",
    EACH symix.ITEM NO-LOCK OF symix.ux-item WHERE
    ITEM.product-code BEGINS "FG":

    DISPLAY ux-item.ITEM WITH 2 COL.
END.


/*******************************************************/


FOR EACH symix.co NO-LOCK WHERE 
    co.cust-num = "TREND",
    EACH symix.ux-coitem NO-LOCK OF symix.co
    BREAK BY ux-coitem.uf-con-lot DESC:

    DISPLAY ux-coitem.uf-con-lot WITH 2 COL.
END. 


FOR EACH symix.ux-coitem NO-LOCK WHERE
    ux-coitem.uf-con-lot = 546989:

    DISPLAY ux-coitem.co-num ux-coitem.co-line WITH 2 COL.
END.   
   
   
FOR EACH symix.ux-coitem NO-LOCK WHERE
    ux-coitem.uf-con-lot = 546457,
    EACH symix.coitem NO-LOCK OF symix.ux-coitem:

    DISPLAY coitem.ITEM WITH 2 COL.
END. 

FOR EACH symix.ux-coitem NO-LOCK,
    EACH symix.coitem NO-LOCK OF symix.ux-coitem WHERE
    coitem.ITEM = "401250+TREND"
    BREAK BY ux-coitem.uf-con-lot DESC:

    DISPLAY ux-coitem.uf-con-lot WITH 2 COL.
END. 

        
FOR EACH symix.ux-coitem NO-LOCK where
    ux-coitem.uf-con-lot = 546469,
    EACH symix.be-package NO-LOCK WHERE
    be-package.co-num   = ux-coitem.co-num AND
    be-package.co-line  = ux-coitem.co-line
    BREAK BY be-package.master:


    DISPLAY be-package WITH 2 COL.
END.

            
/*******************************************************/                
                    
FOR EACH symix.jobtran EXCLUSIVE WHERE
    jobtran.job = "JG15563":

    ASSIGN
        jobtran.emp-num = "  32000".

/*     UPDATE jobtran WITH 2 COL. */
END.


/* FOR EACH symix.jobtran EXCLUSIVE-LOCK: */
/*                                        */
/*     UPDATE jobtran WITH 2 COL.         */
/* END.                                   */


/* FOR EACH symix.indcode NO-LOCK: */
/*                                 */
/*     DISPLAY indcode WITH 2 COL. */
/* END.                            */


/******************************************************/




FOR EACH symix.be-loadscan NO-LOCK WHERE
    be-loadscan.cust-po = "4504895424" AND
    be-loadscan.scanned = FALSE:


    DISPLAY be-loadscan WITH 2 COL.
END.






DEF VAR vFound as log NO-UNDO INIT FALSE.
OUTPUT TO /tmp/job_check.csv.

EXPORT DELIMITER ","
    "Job Number"
    "Order Number"
    "Order Line"
    "Item"
    "Qty on Hand"
    "Completed"
    "Released"
    "Job Date"
    "Job Status"
    "Order Status"
    "Difference".

FOR EACH byrne.coitem NO-LOCK WHERE (coitem.stat = "C" OR coitem.stat = "F")
    AND coitem.ref-num <> ""
    AND coitem.ship-date ge TODAY - 360,
    FIRST byrne.job NO-LOCK WHERE job.job = coitem.ref-num:

    /* narrow the results down to "Job" jobs */
    IF  job.stat <> "F" AND
        job.TYPE = "J" AND
        job.ord-type <> "I" THEN DO:
            IF (job.stat = "C"
                    AND (job.qty-released <> job.qty-complete
                        AND job.qty-complete <> 0 ))
               OR (job.stat <> "C")
            THEN DO:

                find first byrne.itemwhse where
                itemwhse.item = coitem.item no-lock no-error.

                EXPORT DELIMITER ","
                    job.job
                    job.ord-num
                    job.ord-line
                    coitem.item
                    itemwhse.qty-on-hand
                    job.qty-complete
                    job.qty-released
                    job.job-date
                    job.stat
                    coitem.stat
                    job.qty-complete - job.qty-released.
                vFound = TRUE.
            END.
    END.
END.
OUTPUT CLOSE.






/* FOR EACH symix.coitem-log NO-LOCK WHERE */
/*     coitem-log.co-num = " 315651":      */
/*                                         */
/*                                         */
/*     DISPLAY coitem-log WITH 2 COL.      */
/* END.                                    */


    
/* DEF VAR out-s AS CHAR FORMAT "x(32)".             */
/*                                                   */
/* FOR EACH symix.edi-co NO-LOCK WHERE               */
/*     edi-co.cust-po = "PHK857447",                 */
/*     EACH symix.ux-edi-co NO-LOCK OF symix.edi-co: */
/*                                                   */
/*                                                   */
/*     out-s = "M:\test\haworth-change\" +           */
/*         STRING(edi-co.co-num) + "-" /* +          */
/*         STRING(edi-coitem.co-line) */ + ".txt".   */
/*                                                   */
/*     OUTPUT TO VALUE(out-s).                       */
/*     DISPLAY ux-edi-co WITH 2 COL WIDTH 300.       */
/*     OUTPUT CLOSE.                                 */
/*                                                   */
/* END.                                              */





DEF VAR l AS LOG INITIAL FALSE.


OUTPUT TO M:\test\comp-lines.csv.
EXPORT DELIMITER ","
    "co.cust-num"
    "coitem.co-num"
    "coitem.co-line"
    "coitem.stat"
    "coitem.due-date"
    .

FOR EACH symix.co NO-LOCK WHERE
    co.cust-num = "STEEL",
    EACH symix.coitem NO-LOCK OF symix.co WHERE
    coitem.due-date > 04/18/13 AND
    coitem.stat = "c":

    l = FALSE.
    FOR EACH symix.notes NO-LOCK WHERE
        notes.KEY = coitem.KEY:

        IF notes.txt BEGINS "Line Cancelled" THEN
            l = TRUE.
    END.


    IF l = FALSE THEN
        EXPORT DELIMITER ","
            co.cust-num
            coitem.co-num
            coitem.co-line
            coitem.stat
            coitem.due-date
            .

END.
OUTPUT CLOSE.




/* FOR EACH symix.ux-co EXCLUSIVE-LOCK WHERE                        */
/*     ux-co.uf-verify-date = TODAY:                                */
/*                                                                  */
/*     ASSIGN                                                       */
/*         ux-co.uf-verify-date = ?.                                */
/*                                                                  */
/*     DISPLAY ux-co WITH 2 COL.                                    */
/* END.                                                             */
/*                                                                  */
/* DEF VAR counter AS INT INITIAL 0.                                */
/*                                                                  */
/* FOR EACH symix.ux-co NO-LOCK WHERE                               */
/*     ux-co.uf-verify-date = TODAY,                                */
/*     EACH symix.co NO-LOCK OF symix.ux-co:                        */
/*                                                                  */
/*     DISPLAY co WITH 2 COL.                                       */
/* END.                                                             */
/*                                                                  */
/*                                                                  */
/* FOR EACH symix.ITEM NO-LOCK WHERE                                */
/*     ITEM.product-code = "RM-MC",                                 */
/*     EACH symix.coitem NO-LOCK WHERE                              */
/*     coitem.ITEM = ITEM.ITEM                                      */
/*     BREAK BY coitem.co-num DESC:                                 */
/*                                                                  */
/*     DISPLAY coitem.co-num coitem.co-line coitem.ITEM WITH 2 COL. */
/* END.                                                             */


/* OUTPUT TO M:\test\fg-items-lead-time.csv. */
/*                                           */
/* EXPORT DELIMITER ","                      */
/*     "Item"                                */
/*     "Lead Time"                           */
/*     "Product Code"                        */
/*     "Last Order"                          */
/*     "Last Due Date"                       */
/*     .                                     */
/*                                           */
/*                                           */
/* FOR EACH symix.ITEM NO-LOCK WHERE         */
/*     ITEM.lead-time <> 0.0 AND             */
/*     ITEM.product-code BEGINS "FG".        */
/*                                           */
/*     FIND LAST symix.coitem NO-LOCK WHERE  */
/*         coitem.ITEM = ITEM.ITEM NO-ERROR. */
/*                                           */
/*                                           */
/*     IF AVAIL coitem THEN                  */
/*         EXPORT DELIMITER ","              */
/*             ITEM.ITEM                     */
/*             ITEM.lead-time                */
/*             ITEM.product-code             */
/*             coitem.co-num                 */
/*             coitem.due-date               */
/*             .                             */
/*     ELSE                                  */
/*         EXPORT DELIMITER ","              */
/*             ITEM.ITEM                     */
/*             ITEM.lead-time                */
/*             ITEM.product-code             */
/*             "No coitem found"             */
/*             "No coitem found"             */
/*             .                             */
/* END.                                      */




/* CREATE symix.be-labelfields.                     */
/*                                                  */
/* ASSIGN                                           */
/*     be-labelfields.lbl-format = "lot_label.btw". */

/* FOR EACH symix.be-labelfields EXCLUSIVE-LOCK WHERE      */
/*     be-labelfields.lbl-format = "lot_label.btw" AND     */
/*     be-labelfields.PRINTER = "":                        */
/*                                                         */
/*     ASSIGN be-labelfields.PRINTER = "BE-2844Z-PLANT1B". */
/*                                                         */
/*     UPDATE be-labelfields WITH 2 COL.                   */
/* END.                                                    */


/* FOR EACH symix.co NO-LOCK,                               */
/*     EACH symix.coitem OF symix.co NO-LOCK WHERE          */
/*     coitem.due-date = 11/01/12                           */
/*          BREAK BY co.co-num:                             */
/*                                                          */
/*                                                          */
/*                                                          */
/*     IF FIRST-OF(co.co-num) THEN                          */
/*         DISPLAY coitem.co-num coitem.co-line WITH 2 COL. */
/*                                                          */
/* END.                                                     */




DEFINE TEMP-TABLE loaded NO-UNDO
    FIELD job     LIKE symix.job.job
    FIELD ord     LIKE symix.coitem.co-num
    FIELD ln      LIKE symix.coitem.co-line
    FIELD itm     LIKE symix.ITEM.ITEM.

/* get all dates */
INPUT FROM M:\test\load-me.csv.
REPEAT:
    CREATE loaded.
    IMPORT DELIMITER "," loaded NO-ERROR.
END.
INPUT CLOSE. PAUSE(0).

FOR EACH loaded EXCLUSIVE-LOCK WHERE
    loaded.job <> "":

    loaded.ord = " " + loaded.ord.
END.

FOR EACH loaded NO-LOCK WHERE
    loaded.job <> "":

    FIND FIRST symix.jobroute WHERE
        jobroute.job = loaded.job NO-ERROR.

    DISPLAY jobroute.wc WITH 2 COL.

/*     FIND FIRST symix.coitem NO-LOCK WHERE    */
/*         coitem.co-num = loaded.ord AND       */
/*         coitem.co-line = loaded.ln NO-ERROR. */
/*                                              */
/*     DISPLAY coitem.due-date WITH 2 COL.      */
END.

/* symix.jobroute.wc */



/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
OUTPUT TO M:\test\steel-pos.csv.
EXPORT DELIMITER ","
    "Cust PO"
    "DC"
    "Co Num"
    "Co Line"
    "Due Date"
    "Status"
    .
FOR EACH symix.be-loadscan NO-LOCK
    BREAK BY be-loadscan.cust-po:

    IF FIRST-OF(be-loadscan.cust-po) THEN DO:
        
        FIND FIRST symix.co NO-LOCK WHERE
            co.cust-po = be-loadscan.cust-po NO-ERROR.

        IF AVAIL co THEN DO:
            FOR EACH symix.coitem NO-LOCK OF symix.co:
                EXPORT DELIMITER ","
                    be-loadscan.cust-po
                    be-loadscan.dc
                    coitem.co-num
                    coitem.co-line
                    coitem.due-date
                    coitem.stat
                    .
            END.
        END.
        ELSE DO:
            EXPORT DELIMITER ","
                be-loadscan.cust-po
                be-loadscan.dc
                "Not Available"
                "Not Available"
                "Not Available"
                "Not Available"
                .
        END.
    END.
END.
/* - - - - - - - - - - - - - - - - - - - - - - - - - - */
