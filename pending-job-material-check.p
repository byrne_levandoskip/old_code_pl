

output to M:\pending-job-material-trans.txt.
export delimiter "|"
	"Job"
	"Suffix"
	"Created Date"
	"Item"
	"Warehouse"
	"Location"
	"Qty"
	"Qty Required"
	.		

for each symix.jobt-mat no-lock where
	jobt-mat.job = "KA28321" or
	jobt-mat.job = "KA89891" or
	jobt-mat.job = "KB41593" or
	jobt-mat.job = "KB55673" or
	jobt-mat.job = "KC24487" or
	jobt-mat.job = "KC31324":
	
	export delimiter "|"
		jobt-mat.job
		jobt-mat.suffix
		jobt-mat.trans-date
		jobt-mat.item
		jobt-mat.whse
		jobt-mat.loc
		jobt-mat.qty-needed
		jobt-mat.qty-required
		.	
end.
output close.