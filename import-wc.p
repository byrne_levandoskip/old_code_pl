





DEFINE TEMP-TABLE found
    FIELD fg    LIKE symix.ITEM.ITEM
    FIELD wc    LIKE symix.jobroute.wc.

DEFINE TEMP-TABLE out
    FIELD fg    LIKE symix.ITEM.ITEM
    FIELD job   LIKE symix.job.job
    FIELD n-wc  LIKE symix.jobroute.wc
    FIELD o-wc  LIKE symix.jobroute.wc.


RUN import-stuff.
RUN update-ops.
RUN output-stuff.

PROCEDURE import-stuff:
    INPUT FROM m:\import-wc-update.csv NO-ECHO.

    REPEAT:
        CREATE found.
        IMPORT DELIMITER "," found.
    END.
END PROCEDURE.

PROCEDURE update-ops:
    FOR EACH found NO-LOCK WHERE
        found.fg <> "":

        FOR EACH symix.job NO-LOCK WHERE
            ((job.TYPE = "J" AND job.stat = "r") OR
            job.TYPE = "S") AND
            job.ITEM = found.fg,
            FIRST symix.jobroute NO-LOCK WHERE
            jobroute.job = job.job:

            IF jobroute.wc = "A_AC" THEN DO:

                CREATE out.
    
                ASSIGN
                    out.fg    = found.fg
                    out.job   = job.job
                    out.n-wc  = found.wc
                    out.o-wc  = jobroute.wc.
            END.
        END.
    END.
END PROCEDURE.

PROCEDURE output-stuff:
    OUTPUT TO M:\wc-check.csv.
    FOR EACH out NO-LOCK WHERE
        out.fg <> "":
            
            EXPORT DELIMITER ","
                out.
    END.
    OUTPUT CLOSE.
END PROCEDURE.



