
OUTPUT TO M:\enwork-x-ref-check.csv.
EXPORT DELIMITER ","
    "Taken By"
    "Cust PO"
    "Order Num"
    "Order Line"
    "Live Item"
    "Live Cust Item"
    "EDI Item"
    "EDI Cust Item"
    .

FOR EACH symix.co NO-LOCK WHERE
    co.cust-num = "NATION",
    EACH symix.coitem NO-LOCK OF symix.co WHERE
    coitem.stat = "O":

    FIND LAST symix.edi-co NO-LOCK WHERE
        edi-co.cust-po = co.cust-po NO-ERROR.

    FIND LAST symix.edi-coitem NO-LOCK OF symix.edi-co WHERE
        edi-coitem.co-line = coitem.co-line NO-ERROR.

    EXPORT DELIMITER ","
        co.taken-by
        co.cust-po
        coitem.co-num
        coitem.co-line
        coitem.ITEM
        coitem.cust-item
        IF AVAIL edi-coitem THEN edi-coitem.ITEM ELSE ""
        IF AVAIL edi-coitem THEN edi-coitem.cust-item ELSE ""
        .
END.
