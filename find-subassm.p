






DEFINE BUFFER xItem FOR symix.ITEM.

DEFINE TEMP-TABLE found
    FIELD fg    LIKE symix.ITEM.ITEM
    FIELD sub   LIKE symix.ITEM.ITEM
    FIELD dat   LIKE symix.coitem.due-date.


RUN find-subassm.
RUN output-stuff.

PROCEDURE find-subassm:
    FOR EACH symix.ITEM NO-LOCK WHERE
        ITEM.product-code BEGINS "FG",
        EACH symix.job NO-LOCK WHERE
        job.ITEM = ITEM.ITEM AND
        job.TYPE = "s",
        EACH symix.jobroute NO-LOCK WHERE
        jobroute.job = job.job AND
        jobroute.wc = "A_AC",
        EACH symix.jobmatl NO-LOCK WHERE
        jobmatl.job = job.job:
        FIND FIRST xItem NO-LOCK WHERE
            xItem.ITEM = jobmatl.ITEM AND
            xItem.product-code = "SA" NO-ERROR.
        IF AVAIL xItem THEN DO:
            FIND LAST symix.coitem NO-LOCK WHERE
                coitem.ITEM = ITEM.ITEM NO-ERROR.
            CREATE found.
            ASSIGN
                found.fg    = ITEM.ITEM
                found.sub   = xItem.ITEM
                found.dat   = IF AVAIL coitem THEN coitem.due-date
                                ELSE ?.
            RELEASE xItem.
            RELEASE coitem.
        END.
    END.
END PROCEDURE.

PROCEDURE output-stuff:
    OUTPUT TO M:\fg-parts-subassm.csv.
    FOR EACH found NO-LOCK WHERE
        found.fg <> "":
        EXPORT DELIMITER ","
            found.
    END.
    OUTPUT CLOSE.
END PROCEDURE.
