

def temp-table found
    field item          as char format "x(30)"
    field usr           as char
    field create-date   as char
    field old-pricecode as char
    field new-pricecode as char
    .

def var date-yy as char.
def var date-mm as char.
def var date-dd as char.


input from c:\temp\new-pricecode-load.txt.
repeat:
    create found.
    import delimiter "|" found.
    if new-pricecode = "NULL" then delete found.
    else 
        assign
        date-yy = substring(create-date, 3, 2)
        date-mm = substring(create-date, 6, 2)
        date-dd = substring(create-date, 9, 2)
        create-date = date-mm + "/" + date-dd + "/" + date-yy
        .
end.
input close.


output to c:\temp\new-pricecode-no-null-load.txt.
for each found no-lock:
    export delimiter "|" found.
end.
output close.
