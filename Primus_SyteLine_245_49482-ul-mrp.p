
def var t-item like symix.item.item.

UPDATE t-item 
    WITH SIDE-LABELS ROW 2 CENTERED THREE-D V6FRAME FRAME f-1.

/* ITEM */
output to sup-item.d.
find symix.item where item.item = t-item no-lock.
export item.
output close.

/* ITEMWHSE */
output to sup-itemwhse.d.
for each symix.itemwhse where itemwhse.item = t-item no-lock:
   export itemwhse.
end.
output close. 

/* ITEMLOC */
output to sup-itemloc.d.
for each symix.itemloc where itemloc.item = t-item no-lock:
   export itemloc.
end.
output close. 

/* MRP */
output to sup-mrp.d.
for each symix.mrp where mrp.item = t-item no-lock:
   export mrp.
end.
output close. 

/* rcpts */
output to sup-rcpts.d.
for each symix.rcpts where rcpts.item = t-item no-lock:
   export rcpts.
end.
output close. 

/* exc-mesg */
output to sup-exc-mesg.d.
for each symix.exc-mesg where exc-mesg.item = t-item no-lock:
   export exc-mesg.
end.
output close. 

/* forecast */
output to sup-forecast.d.
for each symix.forecast where forecast.item = t-item no-lock:
   export forecast.
end.
output close. 

/* coitem */
output to sup-coitem.d.
for each symix.coitem where coitem.item = t-item no-lock:
   export coitem.
end.
output close. 

/* prodcode */
output to sup-prodcode.d.
find symix.item where item.item = t-item no-lock.
find symix.prodcode where prodcode.product-code = item.product-code NO-LOCK.
export prodcode.
output close. 

/* mrp-parm */
output to sup-mrp-parm.d.
find first symix.mrp-parm no-lock.
export mrp-parm.
output close.

/* mrp-exc */
output to sup-exc-mesg.d.
for each symix.mrp-exc no-lock:
   export mrp-exc.
end.
output close. 

MESSAGE "Procedure Complete".