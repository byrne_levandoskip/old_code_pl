

DEF TEMP-TABLE found
    FIELD ITEM              LIKE symix.ITEM.ITEM
    FIELD be-item           LIKE symix.ITEM.ITEM
    FIELD is-kit            AS LOGICAL
    FIELD be-stat           AS CHAR
    FIELD part-seq          AS INT
    FIELD qty-per           LIKE symix.jobmatl.matl-qty
    FIELD qty-sold          LIKE symix.be_sls_rpt.qty-invoiced
    FIELD product-code      LIKE symix.ITEM.product-code
    .

DEF BUFFER b-item FOR symix.ITEM.
DEF VAR v-be-item LIKE symix.ITEM.ITEM.
DEF VAR v-seq AS INT.
DEF VAR v-tot-sold LIKE symix.be_sls_rpt.qty-invoiced.
DEF VAR v-kit-sold LIKE symix.be_sls_rpt.qty-invoiced.
DEF VAR v-single-sold LIKE symix.be_sls_rpt.qty-invoiced.


/* FG-KT-JP */

OUTPUT TO M:\rough-jumpers-check.csv.
EXPORT DELIMITER ","
    "ITEM"
    "Product-code"
    "DESCRIPTION"
    "Inv-date"
    "Qty-invoiced"
    .
FOR EACH symix.be_sls_rpt NO-LOCK WHERE
    be_sls_rpt.inv-date GE 01/01/18 AND
    be_sls_rpt.inv-date LE 12/31/18,
    FIRST symix.ITEM NO-LOCK WHERE
    ITEM.ITEM = be_sls_rpt.ITEM AND
    ITEM.product-code MATCHES "*JP*":

    IF ITEM.product-code = "FG-KT-JP" THEN DO:
        IF CAN-FIND(FIRST found WHERE 
                    found.ITEM = ITEM.ITEM) THEN DO:
            FOR EACH found EXCLUSIVE-LOCK WHERE
                found.ITEM = ITEM.ITEM:
                ASSIGN
                    found.qty-sold = found.qty-sold + (found.qty-per * be_sls_rpt.qty-invoiced).
            END.
        END.
        ELSE DO:

            ASSIGN
                v-seq = 1.

            FOR EACH symix.jobmatl NO-LOCK WHERE
                jobmatl.job = ITEM.job AND
                CAN-FIND(FIRST b-item NO-LOCK WHERE
                         b-item.ITEM = jobmatl.ITEM AND
                         NOT b-item.ITEM BEGINS "DI-" AND
                         NOT b-item.ITEM BEGINS "BULK" AND
                         b-item.product-code = "SA"):
                
                ASSIGN
                    v-be-item = IF NOT jobmatl.ITEM BEGINS "BE" THEN "BE" + jobmatl.ITEM ELSE jobmatl.ITEM.
                FIND FIRST b-item NO-LOCK WHERE
                    b-item.ITEM = v-be-item NO-ERROR.

                CREATE found.
                ASSIGN
                    found.ITEM = ITEM.ITEM
                    found.be-item = v-be-item
                    found.is-kit = YES
                    found.be-stat = IF AVAIL b-item THEN "Found BE" ELSE "Made Up BE"
                    found.part-seq = v-seq
                    found.qty-per = jobmatl.matl-qty
                    found.product-code = IF AVAIL b-item THEN b-item.product-code ELSE ""
                    found.qty-sold = found.qty-sold + (found.qty-per * be_sls_rpt.qty-invoiced)
                    v-seq = v-seq + 1
                    .
            END.
        END.
    END.
    ELSE DO:
        FIND FIRST symix.jobmatl NO-LOCK WHERE
            jobmatl.job = ITEM.job NO-ERROR.

        IF ITEM.ITEM BEGINS "BE" THEN DO:
            FIND FIRST found EXCLUSIVE-LOCK WHERE
                found.ITEM = ITEM.ITEM NO-ERROR.
            IF NOT AVAIL found THEN DO:
                CREATE found.
                ASSIGN
                    found.ITEM = ITEM.ITEM
                    found.be-item = IF INDEX(ITEM.ITEM, "+") > 0 THEN SUBSTRING(ITEM.ITEM, 1, INDEX(ITEM.ITEM, "+") - 1) ELSE ITEM.ITEM
                    found.is-kit = NO
                    found.be-stat = IF INDEX(ITEM.ITEM, "+") > 0 THEN "BE w/ +" ELSE "Same"
                    found.part-seq = 1
                    found.qty-per = jobmatl.matl-qty
                    found.product-code = ITEM.product-code
                    .
            END.
        END.
        ELSE DO:
            FIND FIRST found EXCLUSIVE-LOCK WHERE
                found.ITEM = ITEM.ITEM NO-ERROR.
            IF NOT AVAIL found THEN DO:
                ASSIGN
                    v-be-item = IF NOT jobmatl.ITEM BEGINS "BE" THEN "BE" + jobmatl.ITEM ELSE jobmatl.ITEM.
                FIND FIRST b-item NO-LOCK WHERE
                    b-item.ITEM = v-be-item NO-ERROR.

                CREATE found.
                ASSIGN
                    found.ITEM = ITEM.ITEM
                    found.be-item = v-be-item
                    found.is-kit = NO
                    found.be-stat = IF AVAIL b-item THEN "Found BE" ELSE "Made Up BE"
                    found.part-seq = 1
                    found.qty-per = jobmatl.matl-qty
                    found.product-code = IF AVAIL b-item THEN b-item.product-code ELSE ""
                    .
            END.
        END.

        ASSIGN 
            found.qty-sold = found.qty-sold + (found.qty-per * be_sls_rpt.qty-invoiced).
    END.


    EXPORT DELIMITER ","
        ITEM.ITEM
        ITEM.product-code
        ITEM.DESCRIPTION
        be_sls_rpt.inv-date
        be_sls_rpt.qty-invoiced
        .

END.
OUTPUT CLOSE.



OUTPUT TO M:\be-jumpers-check.csv.
EXPORT DELIMITER ","
    "Item"
    "be-item"
    "is-kit"
    "be-stat"
    "part-seq"
    "qty-per"
    "qty-sold"
    "product-code"
    .
FOR EACH found NO-LOCK:
    EXPORT DELIMITER ","
        found.
END.
OUTPUT CLOSE.


OUTPUT TO M:\mainoutput-jumpers-check.csv.
EXPORT DELIMITER ","
    "BE Item"
    "Product Code"
    "Total Invoiced"
    "Kit Invoiced"
    "Single Invoiced"
    .
FOR EACH found NO-LOCK
    BREAK BY found.be-item:

    IF FIRST-OF(found.be-item) THEN
        ASSIGN
        v-tot-sold = 0 
        v-kit-sold = 0 
        v-single-sold = 0
        .

    ASSIGN
        v-tot-sold = v-tot-sold + found.qty-sold
        v-kit-sold = IF found.is-kit THEN v-kit-sold + found.qty-sold ELSE v-kit-sold 
        v-single-sold = IF found.is-kit = NO THEN v-single-sold + found.qty-sold ELSE v-single-sold
        .

    IF LAST-OF(found.be-item) THEN
        EXPORT DELIMITER ","
        found.be-item
        found.product-code
        v-tot-sold
        v-kit-sold
        v-single-sold
        .
END.
OUTPUT CLOSE.
