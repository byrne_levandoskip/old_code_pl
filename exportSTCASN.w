&ANALYZE-SUSPEND _VERSION-NUMBER AB_v9r12 GUI ALERT-BOX.
&ANALYZE-RESUME
/* Connected Databases 
          byrne            PROGRESS
*/
&Scoped-define WINDOW-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: 

  Description: from cntnrwin.w - ADM SmartWindow Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  History: New V9 Version - January 15, 1998
 BYR-1000 11/12/2013 MPryor - added rdoCarrier-2 to support
         trailer FedX for FedExLTL and FedEx, user will now need to
         select both rdoCarrier-2 and cmbTrailer before they can export
         add t-test variable, browser updates when user chooses a 
         different Carrier
         removed rdoCarrier radio buttons, btnChangeSD, txtCarrierBOL
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AB.              */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR t-test AS LOGICAL INIT TRUE NO-UNDO.
DEF VAR t-batch         AS INT.
DEF VAR t-cur-time      AS CHAR.
DEF VAR t-batch-mode    AS LOGICAL.
DEF VAR t-beg-batch     AS INT INITIAL 0.
DEF VAR t-end-batch     AS INT.

DEF VAR vItemID         AS INT.
DEF VAR vWayBillID      AS INT.
DEF VAR vShipQty        AS INT.
DEF VAR vBarcodeID      AS INT.
DEF VAR vPartnerNumber  AS CHAR. 
DEF VAR vTransportMeans AS CHAR.
DEF VAR t-arrival-date  AS CHAR FORMAT "x(10)".
DEF VAR t-outfile       AS CHAR FORMAT "x(150)".
DEF VAR t-log-msg       AS CHAR FORMAT "x(130)".
DEF VAR vTrailer        AS CHAR FORMAT "x(150)".
DEF VAR vCarrier        AS CHAR INIT 'All'.  /* BYR-1000 */

DEFINE BUFFER bbe-loadscan FOR byrne.be-loadscan.



DEFINE TEMP-TABLE asn-log NO-UNDO
    FIELD t-trailer     AS CHAR FORMAT "x(20)"
    FIELD t-co-num      LIKE byrne.co.co-num
    FIELD t-co-line     LIKE byrne.coitem.co-line
    FIELD t-date-seq    LIKE byrne.co-ship.date-seq
    FIELD t-qty         LIKE byrne.co-ship.qty-shipped
    FIELD t-scan-date   AS DATE
    FIELD t-scan-time   AS INT
    FIELD t-stat        AS CHAR FORMAT "x(1)"
    FIELD t-err-code    AS INT
    .

DEFINE TEMP-TABLE ttPO NO-UNDO
    FIELD cust-po   LIKE symix.be-loadscan.cust-po
    FIELD po-line   LIKE symix.be-loadscan.po-line
    FIELD trailer   LIKE symix.be-loadscan.trailer
    FIELD ship-type     LIKE symix.be-loadscan.ship-type  /* BYR-1000 Trailer is the same for FedEx and FedExLTL */
    FIELD ItemID    AS INT.

DEFINE TEMP-TABLE ttTrailer NO-UNDO
    FIELD trailer       LIKE symix.be-loadscan.trailer
    FIELD ship-type     LIKE symix.be-loadscan.ship-type.
    .

DEFINE STREAM str-log.
DEFINE STREAM edi-asn.
DEFINE STREAM out-rpt.

ASSIGN t-batch-mode = SESSION:BATCH-MODE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwLoadScan

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES byrne.be-loadscan

/* Definitions for BROWSE brwLoadScan                                   */
&Scoped-define FIELDS-IN-QUERY-brwLoadScan byrne.be-loadscan.trans-date ~
byrne.be-loadscan.barcode ~
substring(byrne.be-loadscan.dc,1,2) + "-" + byrne.be-loadscan.loadingpoint ~
byrne.be-loadscan.cust-po byrne.be-loadscan.po-line byrne.be-loadscan.qty ~
byrne.be-loadscan.skid byrne.be-loadscan.trailer ~
byrne.be-loadscan.track-num 
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwLoadScan ~
byrne.be-loadscan.trailer byrne.be-loadscan.track-num 
&Scoped-define ENABLED-TABLES-IN-QUERY-brwLoadScan byrne.be-loadscan
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwLoadScan byrne.be-loadscan
&Scoped-define QUERY-STRING-brwLoadScan FOR EACH byrne.be-loadscan ~
      WHERE byrne.be-loadscan.extracted = FALSE ~
 AND byrne.be-loadscan.scanned = true ~
 AND (byrne.be-loadscan.ship-type = vCarrier ~
 OR vCarrier = 'All') NO-LOCK ~
    BY byrne.be-loadscan.skid INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwLoadScan OPEN QUERY brwLoadScan FOR EACH byrne.be-loadscan ~
      WHERE byrne.be-loadscan.extracted = FALSE ~
 AND byrne.be-loadscan.scanned = true ~
 AND (byrne.be-loadscan.ship-type = vCarrier ~
 OR vCarrier = 'All') NO-LOCK ~
    BY byrne.be-loadscan.skid INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwLoadScan byrne.be-loadscan
&Scoped-define FIRST-TABLE-IN-QUERY-brwLoadScan byrne.be-loadscan


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwLoadScan}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-6 rdoCarrier-2 cmbTrailer brwLoadScan 
&Scoped-Define DISPLAYED-OBJECTS rdoCarrier-2 cmbTrailer 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btnExport 
     LABEL "Export ASN" 
     SIZE 15 BY 1.12.

DEFINE VARIABLE cmbTrailer AS CHARACTER FORMAT "X(256)":U 
     LABEL "Trailer" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE rdoCarrier-2 AS INTEGER INITIAL 5 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Steelcase Truck", 1,
"FedEx", 2,
"FedEx Freight", 3,
"Same Day", 4,
"All", 5
     SIZE 24.86 BY 3.92 NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 142.86 BY 4.27.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwLoadScan FOR 
      byrne.be-loadscan SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwLoadScan
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwLoadScan wWin _STRUCTURED
  QUERY brwLoadScan NO-LOCK DISPLAY
      byrne.be-loadscan.trans-date COLUMN-LABEL "Scan Date" FORMAT "99/99/99":U
      byrne.be-loadscan.barcode COLUMN-LABEL "Barcode" FORMAT "X(40)":U
            WIDTH 25.43
      substring(byrne.be-loadscan.dc,1,2) + "-" + byrne.be-loadscan.loadingpoint COLUMN-LABEL "DC"
      byrne.be-loadscan.cust-po FORMAT "x(22)":U WIDTH 17.72
      byrne.be-loadscan.po-line FORMAT ">>>>>>>9":U WIDTH 6.29
      byrne.be-loadscan.qty FORMAT "->,>>>,>>9":U
      byrne.be-loadscan.skid FORMAT ">,>>9":U
      byrne.be-loadscan.trailer FORMAT "X(30)":U WIDTH 14.57
      byrne.be-loadscan.track-num FORMAT "X(50)":U
  ENABLE
      byrne.be-loadscan.trailer
      byrne.be-loadscan.track-num
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 143.14 BY 21 ROW-HEIGHT-CHARS .62 EXPANDABLE.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     rdoCarrier-2 AT ROW 1.23 COL 4.72 NO-LABEL NO-TAB-STOP 
     btnExport AT ROW 1.69 COL 113.57
     cmbTrailer AT ROW 1.81 COL 46.43 COLON-ALIGNED
     brwLoadScan AT ROW 5.38 COL 3.29
     RECT-6 AT ROW 1 COL 3.86
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.08
         SIZE 155.14 BY 25.69.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Export Steelcase ASN"
         HEIGHT             = 26.54
         WIDTH              = 147.57
         MAX-HEIGHT         = 39.15
         MAX-WIDTH          = 274.29
         VIRTUAL-HEIGHT     = 39.15
         VIRTUAL-WIDTH      = 274.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
                                                                        */
/* BROWSE-TAB brwLoadScan cmbTrailer fMain */
/* SETTINGS FOR BUTTON btnExport IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwLoadScan
/* Query rebuild information for BROWSE brwLoadScan
     _TblList          = "byrne.be-loadscan"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _OrdList          = "byrne.be-loadscan.skid|yes"
     _Where[1]         = "byrne.be-loadscan.extracted = FALSE
 AND byrne.be-loadscan.scanned = true
 AND (byrne.be-loadscan.ship-type = vCarrier
 OR vCarrier = 'All')"
     _FldNameList[1]   > byrne.be-loadscan.trans-date
"trans-date" "Scan Date" ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[2]   > byrne.be-loadscan.barcode
"barcode" "Barcode" ? "character" ? ? ? ? ? ? no ? no no "25.43" yes no no "U" "" ""
     _FldNameList[3]   > "_<CALC>"
"substring(byrne.be-loadscan.dc,1,2) + ""-"" + byrne.be-loadscan.loadingpoint" "DC" ? ? ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[4]   > byrne.be-loadscan.cust-po
"cust-po" ? ? "character" ? ? ? ? ? ? no ? no no "17.72" yes no no "U" "" ""
     _FldNameList[5]   > byrne.be-loadscan.po-line
"po-line" ? ? "integer" ? ? ? ? ? ? no ? no no "6.29" yes no no "U" "" ""
     _FldNameList[6]   = byrne.be-loadscan.qty
     _FldNameList[7]   = byrne.be-loadscan.skid
     _FldNameList[8]   > byrne.be-loadscan.trailer
"trailer" ? ? "character" ? ? ? ? ? ? yes ? no no "14.57" yes no no "U" "" ""
     _FldNameList[9]   > byrne.be-loadscan.track-num
"track-num" ? ? "character" ? ? ? ? ? ? yes ? no no ? yes no no "U" "" ""
     _Query            is OPENED
*/  /* BROWSE brwLoadScan */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Export Steelcase ASN */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON ENTRY OF wWin /* Export Steelcase ASN */
DO:
  vTrailer = "".
  FOR EACH ttTrailer EXCLUSIVE:
          DELETE ttTrailer.
  END.
  FOR EACH byrne.be-loadscan NO-LOCK WHERE
        extracted = NO AND
        scanned = YES:

        FIND FIRST ttTrailer NO-LOCK WHERE
            ttTrailer.trailer = be-loadscan.trailer 
            AND ttTrailer.ship-type = be-loadscan.ship-type /* BYR-1000 */
            NO-ERROR.

        IF NOT AVAIL ttTrailer THEN DO:
            CREATE ttTrailer.
            ASSIGN
                ttTrailer.trailer   = be-loadscan.trailer
                ttTrailer.ship-type = be-loadscan.ship-type.
            IF LOOKUP(ttTrailer.trailer,vTrailer) = 0 THEN
                ASSIGN vTrailer = vTrailer + "," + ttTrailer.trailer.
            IF t-test THEN MESSAGE 'be-loadscan.ship-type' be-loadscan.ship-type 
                VIEW-AS ALERT-BOX.
         END.
    
  END.
  IF t-test THEN MESSAGE '1 populating cmbTrailer' vTrailer VIEW-AS ALERT-BOX.
  cmbTrailer:LIST-ITEMS IN FRAME fMain = vTrailer.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Export Steelcase ASN */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnExport wWin
ON CHOOSE OF btnExport IN FRAME fMain /* Export ASN */
DO:
    IF cmbTrailer:SCREEN-VALUE = ? THEN DO:
        MESSAGE "You must select a trailer first" VIEW-AS ALERT-BOX ERROR.
        DISABLE btnExport.  /* BYR-1000 */
         UNDO, RETRY.
    END.
    IF vCarrier = 'All' THEN DO:  /* BYR-1000 */
        MESSAGE "You must select a ship type first" VIEW-AS ALERT-BOX ERROR.
        DISABLE btnExport.
        UNDO, RETRY.
    END.

    FOR EACH byrne.be-loadscan NO-LOCK WHERE
        extracted = NO AND
        scanned = TRUE:

        FIND FIRST ttTrailer NO-LOCK WHERE
            ttTrailer.trailer = be-loadscan.trailer 
            AND ttTrailer.ship-type = be-loadscan.ship-type  /* BYR-1000 */
            NO-ERROR.

        IF NOT AVAIL ttTrailer THEN DO:
            CREATE ttTrailer.
            ASSIGN
                ttTrailer.trailer   = be-loadscan.trailer
                ttTrailer.ship-type = be-loadscan.ship-type.
        END.
        
        
        FIND FIRST ttPO NO-LOCK WHERE
            ttPO.cust-po = be-loadscan.cust-po AND
            ttPO.po-line = be-loadscan.po-line and
            ttPO.trailer = be-loadscan.trailer AND 
            ttPO.ship-type = be-loadscan.ship-type /* BYR-1000 */
            NO-ERROR.


        IF NOT AVAIL ttPO THEN DO:
           
            CREATE ttPO.
            ASSIGN
                ttPO.cust-po = be-loadscan.cust-po
                ttPO.po-line = be-loadscan.po-line
                ttPO.trailer = be-loadscan.trailer
                ttPO.ship-type = be-loadscan.ship-type.  /* BYR-1000 */
        END.

        

    END.  /* for each be-loadscan */
    /* BYR-1000 - try adding an eror message if they don't have any records */
    IF NOT CAN-FIND (FIRST ttPO WHERE
            ttPO.trailer = cmbTrailer:SCREEN-VALUE
            AND ttPO.ship-type = vCarrier) THEN DO:

        MESSAGE "No Orders met the criteria you entered" VIEW-AS ALERT-BOX ERROR.
        UNDO, RETRY.
    END.

    
    /* Hard code output folder per Mike P. */
    IF t-test THEN
     OUTPUT STREAM str-log TO VALUE("c:\temp\STC_ASN.Log." ) APPEND.
    ELSE
     OUTPUT STREAM str-log TO VALUE("\\byrne-electrical.com\network\public\ftp\steelcase\Outbound\STC_ASN.Log." ) APPEND.
     
    t-cur-time = STRING(time,"HH:MM:SS").  /* all invoices will have same "create time" and date/time stamp in the file name */
         
    t-arrival-date = STRING(YEAR(TODAY)) + "-" + STRING(MONTH(TODAY),"99") + "-" + STRING(DAY(TODAY),"99") + "T" + t-cur-time.  

    vItemID = 0.

    FOR EACH ttTrailer NO-LOCK WHERE 
        ttTrailer.trailer = cmbTrailer:SCREEN-VALUE
        /* BYR-1000 */
        AND ttTrailer.ship-type = vCarrier
        AND vCarrier NE 'ALL':


        FIND LAST byrne.be-loadscan NO-LOCK USE-INDEX si-waybillid NO-ERROR.

        vWayBillID = byrne.be-loadscan.waybillid + 1.

        

        IF ttTrailer.ship-type = "FedEx" THEN
            ASSIGN
                vPartnerNumber  = "FDEN"
                vTransportMeans = "CNT".

        ELSE IF ttTrailer.ship-type = "SCRP" THEN
            ASSIGN
                vPartnerNumber  = "SCRP"
                vTransportMeans = "TRK".

        ELSE IF ttTrailer.ship-type = "SameDay" THEN
            ASSIGN
                vPartnerNumber  = "SDAF"
                vTransportMeans = "TRK".

        ELSE IF ttTrailer.ship-type  = "FedExLTL" THEN
            ASSIGN
                vPartnerNumber  = "FDEN"
                vTransportMeans = "TRK".  
    
        /* Last Tran Record 50 is a custom code added for the EDI export */                                    
        FIND byrne.lasttran WHERE lasttran.lasttran-key = 51 NO-ERROR.

        IF  NOT AVAILABLE lasttran THEN DO:

            CREATE lasttran.
            ASSIGN
                    lasttran-key = 51
                    trans-file   = "Steelcase ASN #"
                    last-tran    = 1
                    .

        END.    /* NOT AVAIL lasttran */


        ASSIGN
            t-batch            = lasttran.last-tran
            lasttran.last-tran = lasttran.last-tran + 1
            .

        IF  t-beg-batch = 0 THEN
            t-beg-batch = t-batch.

        t-end-batch = t-batch.

        t-log-msg = "  Batch # = " + STRING(t-batch) + ".".

        PUT STREAM str-log t-log-msg SKIP.
    
      /* hard code output folder per Mike P. */
            IF t-test THEN 
                ASSIGN t-outfile = "c:\temp\OutboundASN_" + STRING(t-batch) + "-" + string(vWayBillID) + "_" + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99")
                       + SUBSTRING(STRING(YEAR(TODAY)),3,2 ) + SUBSTRING(t-cur-time,1,2) + SUBSTRING(t-cur-time,4,2) 
                       + SUBSTRING(t-cur-time,7,2) + ".xml".
            ELSE ASSIGN 
                t-outfile = "\\byrne-electrical.com\network\public\ftp\steelcase\Outbound" + "\ASN_" + STRING(t-batch) + "-" + string(vWayBillID) + "_" + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99")
                           + SUBSTRING(STRING(YEAR(TODAY)),3,2 ) + SUBSTRING(t-cur-time,1,2) + SUBSTRING(t-cur-time,4,2) 
                           + SUBSTRING(t-cur-time,7,2) + ".xml".
    
        OUTPUT STREAM edi-asn TO VALUE(t-outfile) APPEND.
        
        PUT STREAM edi-asn UNFORMATTED '<?xml version="1.0" encoding="UTF-8" ?>'.
        PUT STREAM edi-asn UNFORMATTED '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:p1="urn:steelcase.com:wsprocurement:xml2suppliers">'.
        PUT STREAM edi-asn UNFORMATTED '<soap:Body>'.
        PUT STREAM edi-asn UNFORMATTED '<p1:AdvancedDeliveryNotification>'.
        PUT STREAM edi-asn UNFORMATTED '<Delivery>'.
        PUT STREAM edi-asn UNFORMATTED "<Header>".
        PUT STREAM edi-asn UNFORMATTED "<ID>" STRING(t-batch) "</ID>".
        PUT STREAM edi-asn UNFORMATTED "<ArrivalDateTime>" t-arrival-date "</ArrivalDateTime>".    
/*         PUT STREAM edi-asn UNFORMATTED "<WayBillID>" STRING(vWayBillID) "</WayBillID>".  no used now */
        PUT STREAM edi-asn UNFORMATTED "<TransportMeans>".
        PUT STREAM edi-asn UNFORMATTED "<ID>" vTransportMeans "</ID>".
        PUT STREAM edi-asn UNFORMATTED "</TransportMeans>".
        IF ttTrailer.trailer <> "direct" THEN DO:
            PUT STREAM edi-asn UNFORMATTED '<TransportTracking>'.
            PUT STREAM edi-asn UNFORMATTED '<ID>' TRIM(ttTrailer.trailer) '</ID>'.
            PUT STREAM edi-asn UNFORMATTED '</TransportTracking>'.
        END.
        PUT STREAM edi-asn UNFORMATTED "<PartnerFunction>CR</PartnerFunction>".
        PUT STREAM edi-asn UNFORMATTED "<PartnerNumber>" vPartnerNumber "</PartnerNumber>".
        PUT STREAM edi-asn UNFORMATTED "</Header>".

        OUTPUT STREAM edi-asn CLOSE.
  

        vItemID = 0.
        
       
        FOR EACH ttPO EXCLUSIVE-LOCK WHERE
            ttPO.trailer = ttTrailer.trailer
            AND ttPO.ship-type = ttTrailer.ship-type:  /* BYR-1000 */

            FIND FIRST byrne.co NO-LOCK WHERE
                 byrne.co.cust-po = ttPO.cust-po NO-ERROR.
            IF NOT AVAIL byrne.co THEN NEXT.
            FIND FIRST byrne.ux-coitem NO-LOCK WHERE
                ux-coitem.co-num = co.co-num AND 
                   ux-coitem.uf-steel-po-line = string(ttPO.po-line) NO-ERROR.  
            IF NOT AVAIL byrne.ux-coitem THEN NEXT.
            FIND FIRST byrne.coitem OF ux-coitem NO-LOCK NO-ERROR.
            IF NOT AVAIL byrne.coitem THEN NEXT.
    
    
            t-log-msg = "        Processing shipment for " + ux-coitem.co-num + "-" + STRING(ux-coitem.co-line,"9999") + " Shpd " 
                      + STRING(TODAY,"99/99/99") + " seq " + STRING(TIME) + " Item:" + coitem.ITEM.
    
            PUT STREAM str-log t-log-msg SKIP.
        
            vBarcodeID = 0.
    
        
            vShipQty = 0.

/*             FOR EACH byrne.co-ship NO-LOCK WHERE                                  */
/*                 co-ship.co-num = coitem.co-num AND                                */
/*                 co-ship.co-line = coitem.co-line:                                 */
/*                                                                                   */
/*                 vShipQty = vShipQty + co-ship.qty-shipped - co-ship.qty-returned. */
/*             END.                                                                  */
        

            FOR EACH byrne.be-loadscan NO-LOCK WHERE
                byrne.be-loadscan.trailer = ttTrailer.trailer     AND
                byrne.be-loadscan.ship-type = ttTrailer.ship-type AND  /* BYR-1000 */
                byrne.be-loadscan.cust-po = ttPO.cust-po          AND
                byrne.be-loadscan.po-line = ttPO.po-line:
            
                vShipQty = vShipQty + be-loadscan.qty.

            END.
            
            vItemID = vItemID + 1.
            
            OUTPUT STREAM edi-asn TO VALUE(t-outfile) APPEND.
            
            PUT STREAM edi-asn UNFORMATTED "<Item>".
            PUT STREAM edi-asn UNFORMATTED "<ID>" vItemID "</ID>".
            PUT STREAM edi-asn UNFORMATTED "<PurchaseOrderReference>".
            PUT STREAM edi-asn UNFORMATTED "<ID>" ttPO.cust-po  "</ID>".
            PUT STREAM edi-asn UNFORMATTED "<ItemID>" ttPO.po-line  "</ItemID>".
            PUT STREAM edi-asn UNFORMATTED "</PurchaseOrderReference>".
            PUT STREAM edi-asn UNFORMATTED "<Product>".
            PUT STREAM edi-asn UNFORMATTED "<ConsigneeID>" trim(SUBSTRING(ux-coitem.uf-drawing-nbr,1,18)) "</ConsigneeID>".
            PUT STREAM edi-asn UNFORMATTED "</Product>".
            PUT STREAM edi-asn UNFORMATTED '<Quantity unitCode="PC">' vShipQty '</Quantity>'.
            PUT STREAM edi-asn UNFORMATTED "</Item>".

            OUTPUT STREAM edi-asn CLOSE.
            
            ttPO.ItemID = vItemID.
            
        END.

        FOR EACH ttPO NO-LOCK WHERE
            ttPO.trailer = ttTrailer.trailer
            AND ttPO.ship-type = ttTrailer.ship-type:  /* BYR-1000 */

            FOR EACH byrne.be-loadscan EXCLUSIVE-LOCK WHERE
                    byrne.be-loadscan.trailer = ttTrailer.trailer     AND
                    byrne.be-loadscan.ship-type = ttTrailer.ship-type AND
                    byrne.be-loadscan.cust-po = ttPO.cust-po          AND
                    byrne.be-loadscan.po-line = ttPO.po-line:
    
/*                     vBarcodeID = vBarcodeID + 1.  */
    
                    OUTPUT STREAM edi-asn TO VALUE(t-outfile) APPEND.
                    
                    PUT STREAM edi-asn UNFORMATTED "<Barcode>".
                    PUT STREAM edi-asn UNFORMATTED "<HandlingUnitIdentification>" byrne.be-loadscan.barcode "</HandlingUnitIdentification>".
                    IF byrne.be-loadscan.track-num <> "" THEN
                    PUT STREAM edi-asn UNFORMATTED "<HandlingUnitIdentification2>" byrne.be-loadscan.track-num "</HandlingUnitIdentification2>".
                    PUT STREAM edi-asn UNFORMATTED "<HandlingUnitContents>".  
                    PUT STREAM edi-asn UNFORMATTED "<DeliveryItem>".    
                    PUT STREAM edi-asn UNFORMATTED "<ItemID>" ttPO.ItemID "</ItemID>".  
                    PUT STREAM edi-asn UNFORMATTED '<Quantity unitCode="PC">' byrne.be-loadscan.qty '</Quantity>'.
                    PUT STREAM edi-asn UNFORMATTED "</DeliveryItem>".    
                    PUT STREAM edi-asn UNFORMATTED "</HandlingUnitContents>".
                    PUT STREAM edi-asn UNFORMATTED "</Barcode>".
            
                    OUTPUT STREAM edi-asn CLOSE.
                        
            
                    ASSIGN
                        be-loadscan.waybillid   = vWayBillID
                        be-loadscan.extracted   = YES
                        be-loadscan.trans-date  = TODAY
                        be-loadscan.trans-time  = TIME.
                END.
        END.



        OUTPUT STREAM edi-asn TO VALUE(t-outfile) APPEND.
        
        PUT STREAM edi-asn UNFORMATTED "</Delivery>".
        PUT STREAM edi-asn UNFORMATTED "</p1:AdvancedDeliveryNotification>".
        PUT STREAM edi-asn UNFORMATTED "</soap:Body>".
        PUT STREAM edi-asn UNFORMATTED "</soap:Envelope>".

        OUTPUT STREAM edi-asn CLOSE.
        
    END.

    OUTPUT STREAM str-log CLOSE.
    ASSIGN 
        vPartnerNumber  = ""
        vTransportMeans = ""
        vTrailer = ""
        vCarrier = 'All'   /* BYR-1000 */
        rdoCarrier-2:SCREEN-VALUE = "5".   /* BYR-1000 */

    {&OPEN-BROWSERS-IN-QUERY-fMain}

      FOR EACH ttTrailer EXCLUSIVE:
          DELETE ttTrailer.
      END.

      FOR EACH byrne.be-loadscan NO-LOCK WHERE
            extracted = NO AND
            scanned = YES:
    
            FIND FIRST ttTrailer NO-LOCK WHERE
                ttTrailer.trailer = be-loadscan.trailer 
                AND ttTrailer.ship-type = be-loadscan.ship-type  /* BYR-1000 */
                NO-ERROR.
    
            IF NOT AVAIL ttTrailer THEN DO:
                CREATE ttTrailer.
                ASSIGN
                    ttTrailer.trailer = be-loadscan.trailer
                    ttTrailer.ship-type = be-loadscan.ship-type.
                IF LOOKUP(ttTrailer.trailer,vTrailer) = 0 THEN
                        ASSIGN vTrailer = vTrailer + "," + ttTrailer.trailer.
    
             END.
        
      END.
    
      IF t-test THEN MESSAGE '3 populating cmbTrailer' vTrailer VIEW-AS ALERT-BOX.
      cmbTrailer:LIST-ITEMS = vTrailer.

      IF cmbTrailer:SCREEN-VALUE <> ? THEN
        MESSAGE "ASN Sent" VIEW-AS ALERT-BOX.

      DISABLE btnExport WITH FRAME fMain .  /* BYR-1000 */

END.  /* btnExport procedure */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbTrailer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbTrailer wWin
ON VALUE-CHANGED OF cmbTrailer IN FRAME fMain /* Trailer */
DO:  /* BYR-1000 */
  IF t-test THEN MESSAGE 'value-changed cmbTrailer' vCarrier cmbTrailer:SCREEN-VALUE VIEW-AS ALERT-BOX.
  IF vCarrier NE 'All' AND cmbTrailer:SCREEN-VALUE NE ? THEN
      ENABLE btnExport WITH FRAME fMain IN WINDOW wWin.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rdoCarrier-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rdoCarrier-2 wWin
ON VALUE-CHANGED OF rdoCarrier-2 IN FRAME fMain
DO:
  IF t-test THEN MESSAGE 'running value-changed of rdoCarrier-2' VIEW-AS ALERT-BOX.
  IF rdoCarrier-2:SCREEN-VALUE = "2" THEN 
       ASSIGN vCarrier = "FedEx". 
  ELSE IF rdoCarrier-2:SCREEN-VALUE = "3" THEN
       ASSIGN vCarrier = "FedExLTL".
  ELSE IF rdoCarrier-2:SCREEN-VALUE = "4" THEN 
       ASSIGN vCarrier = "SameDay".
  ELSE IF rdoCarrier-2:SCREEN-VALUE = "1" THEN 
       ASSIGN vCarrier = 'SCRP'.
  ELSE ASSIGN vCarrier = 'ALL'.
    
  DISABLE btnExport WITH FRAME fMain .  /* BYR-1000 */    

  vTrailer = "".

  FOR EACH ttTrailer NO-LOCK
      WHERE ttTrailer.ship-type = vCarrier
      OR vCarrier= 'ALL':
  
      IF LOOKUP(ttTrailer.trailer,vTrailer) = 0 THEN
          ASSIGN vTrailer = vTrailer + "," + ttTrailer.trailer.

    
  END.

  IF t-test THEN MESSAGE '4 populating cmbTrailer' vTrailer vCarrier VIEW-AS ALERT-BOX.
  cmbTrailer:LIST-ITEMS IN FRAME fMain = vTrailer.
  /* BYR-1000 update browser */
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwLoadScan
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY rdoCarrier-2 cmbTrailer 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE RECT-6 rdoCarrier-2 cmbTrailer brwLoadScan 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

